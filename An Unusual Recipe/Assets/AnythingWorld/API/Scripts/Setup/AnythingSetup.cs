﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Initializes the scene ready for creating objects with AnythingCreator.
/// </summary>
[ExecuteAlways]
[Serializable]
public class AnythingSetup : MonoBehaviour
{
    #region Fields
    public AnythingSettings anythingSettings;
    public BehaviourSettings behaviourSettings;
    public WaitSwirly WaitIndicator;
    private const int MAX_ATTRIBUTION_LINES = 10;
    [SerializeField]
    private Text attributionText;
    [SerializeField]
    private List<string> attributions;
    private bool isShowingLoading;
    public bool IsShowingLoading
    {
        get
        {
            return isShowingLoading;
        }
    }
    private static AnythingSetup instance;
    public static AnythingSetup Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<AnythingSetup>();
                if (instance == null)
                {
                    Debug.LogError("No anything setup in scene");
                }
            }

            instance.ApplySettings();
            return instance;
        }
    }
    #endregion

    #region Unity Callbacks
    void Awake()
    {
        instance = this;


        if (anythingSettings == null)
        {
            anythingSettings = AnythingBase.Settings;
        }

        GameObject attributionObject = GameObject.FindGameObjectWithTag("AWAttribution");
        if (attributionObject == null)
        {
            Debug.LogError("No Attribution Object Found. Please Add a Text UI object with the tag 'AWAttribution'");
        }
        else
        {
            attributionText = attributionObject.GetComponent<Text>();
        }



        ApplySettings();
    }
    private void Reset()
    {
        attributions = new List<string>();
        attributionText.text = "";
    }
    #endregion

    #region Private Methods
    private void ApplySettings()
    {
        Physics.defaultSolverIterations = anythingSettings.PhysicsSolverIterations;
        Time.maximumDeltaTime = anythingSettings.MaximumAllowedTimeStep;
        Application.targetFrameRate = anythingSettings.FrameRate;
        int defaultLayer = LayerMask.NameToLayer("Default");
        int raycastLayer = LayerMask.NameToLayer("Ignore Raycast");
        int animationLayer = LayerMask.NameToLayer("Animation");
        Physics.IgnoreLayerCollision(defaultLayer, raycastLayer, true);
        Physics.IgnoreLayerCollision(defaultLayer, animationLayer, true);
    }
    #endregion

    #region Public Methods
    public void ShowAttribution(string creatorText)
    {
        if (attributions == null)
            Reset();

        //Debug.Log("show attribution : " + creatorText);

        if (attributions.Count == MAX_ATTRIBUTION_LINES)
        {
            attributions.RemoveAt(0);
        }
        attributions.Add(creatorText);
        attributionText.text = "";
        foreach (string attr in attributions)
        {
            attributionText.text += attr;
            attributionText.text += "\n";
        }
#if UNITY_EDITOR
        EditorUtility.SetDirty(attributionText);
#endif
    }
    public void ClearAttribution()
    {
        Reset();
    }
    public void ShowLoading(bool showLoading)
    {
        //Debug.Log("Setting loading to: " + showLoading.ToString());
        if (WaitIndicator != null)
        {
            if (WaitIndicator.gameObject.activeSelf == showLoading)
                return;
            if (showLoading)
                WaitIndicator.gameObject.SetActive(showLoading);
            WaitIndicator.StartSwirly(showLoading);
            if (!showLoading)
                WaitIndicator.gameObject.SetActive(showLoading);
            isShowingLoading = showLoading;
        }
    }

    #endregion
}
