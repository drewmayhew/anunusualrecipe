﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourHandler : MonoBehaviour
{
    private AnythingCreator anythingCreator;
    private Camera mainCamera;
    private CameraFollow mainCameraFollower;
    private void OnEnable()
    {
        anythingCreator = AnythingCreator.Instance;
        mainCamera = Camera.main;
        mainCameraFollower = mainCamera.GetComponent<CameraFollow>();
    }


    public IEnumerator ChaseCoroutine(string _chasingAnimal, string _fleeingAnimal, bool _makeNew = false)
    {
        // Debug.Log("Chasing animal: " + _chasingAnimal);
        // Debug.Log("Fleeing animal: " + _fleeingAnimal);

        AWObj chasingAWObj = null;
        AWObj fleeingAWObj = null;
        FollowTarget followScript = null;

        if (_makeNew)
        {
            Debug.Log("No chasing animal found " + _chasingAnimal + ", trying to make.");

            chasingAWObj = anythingCreator.MakeObject(_chasingAnimal);
            if (chasingAWObj != null)
            {
                followScript = chasingAWObj.AddBehaviour<FollowTarget>();
                followScript.SpeedMultiplier = 2f;
            }

            fleeingAWObj = anythingCreator.MakeObject(_fleeingAnimal);
            if (fleeingAWObj != null)
            {
                fleeingAWObj.AddBehaviour<RandomMovement>().SpeedMultiplier = 2f;
                if (followScript != null)
                {
                    followScript.targetController = fleeingAWObj;
                }
                else
                {
                    Debug.Log("No followers found");
                }

            }

            yield return null;
        }
        else
        {
            //Find chaser
            AWObj[] foundItems = GameObject.FindObjectsOfType<AWObj>();
            yield return null;
            List<AWObj> foundList = new List<AWObj>(foundItems);
            foreach (AWObj creature in foundList)
            {

                if (chasingAWObj != null && fleeingAWObj != null) break;

                Debug.Log(creature._objName);
                if (creature._objName == _chasingAnimal)
                {
                    chasingAWObj = creature;
                }
                yield return null;
                if (creature._objName == _fleeingAnimal && creature != chasingAWObj)
                {
                    fleeingAWObj = creature;
                }
                yield return null;
            }


            if (fleeingAWObj == null)
            {
                fleeingAWObj = anythingCreator.MakeObject(_fleeingAnimal);
            }
            if (chasingAWObj == null)
            {
                chasingAWObj = anythingCreator.MakeObject(_chasingAnimal);
            }


            fleeingAWObj.AddBehaviour<RandomMovement>().SpeedMultiplier = 2f;


            chasingAWObj.AddBehaviour<FollowTarget>().targetController = fleeingAWObj;
        }
    }

    public void StopRiding()
    {
        mainCameraFollower.SwitchModes(CameraFollow.CameraMode.Follow);
        //mainCamera.GetComponent<CameraFollow>().SwitchModes(CameraFollow.CameraMode.Follow);
    }
    public IEnumerator RideCoroutine(string _animalToRide, bool _makeNew = false)
    {
        AWObj animalAWObj = null;

        if (!_makeNew)
        {
            AWObj[] foundItems = GameObject.FindObjectsOfType<AWObj>();
            List<AWObj> foundList = new List<AWObj>(foundItems);
            yield return null;


            foreach (AWObj creature in foundList)
            {
                if (animalAWObj != null) break;
                Debug.Log(creature._objName);

                if (creature._objName == _animalToRide)
                {
                    animalAWObj = creature;
                    break;
                }
                yield return null;
            }

            if (animalAWObj == null)
            {
                animalAWObj = anythingCreator.MakeObject(_animalToRide);
                if (animalAWObj != null)
                {
                    animalAWObj.AddBehaviour<RandomMovement>();
                }
            }



        }
        else
        {
            animalAWObj = anythingCreator.MakeObject(_animalToRide);
            if (animalAWObj != null)
            {
                mainCamera.GetComponent<CameraFollow>().RideTarget(animalAWObj);
            }
        }


        mainCamera.GetComponent<CameraFollow>().RideTarget(animalAWObj);
        yield return null;
    }

    // TODO: add generic quantity / PFX to thrown thing?
    public IEnumerator ThrowCoroutine(string _thingToThrow, string _thingToThrowAt, string quantity, bool _makeNew = false)
    {
        // Debug.Log("Throw thing: " + _thingToThrow);
        // Debug.Log("Thrown at thing: " + _thingToThrowAt);

        AWObj throwAWObj = null;
        AWObj throwAtAWObj = null;
        ThrowTarget throwScript = null;

        Vector3 throwStartPos = Camera.main.transform.position - new Vector3(0, 10f, 0f);

        if (_makeNew)
        {

            throwAWObj = anythingCreator.MakeObject(_thingToThrow, throwStartPos, false);
            if (throwAWObj != null)
            {
                throwScript = throwAWObj.gameObject.AddComponent<ThrowTarget>();
            }

            throwAtAWObj = anythingCreator.MakeObject(_thingToThrowAt);
            if (throwAtAWObj != null)
            {
                if (throwScript != null)
                {
                    throwScript.TargetTransform = throwAtAWObj.transform;
                }
                else
                {
                    Debug.Log("Nothing to throw at found");
                }
            }

            yield return null;
        }
        else
        {
            //Find thrower
            AWObj[] foundItems = GameObject.FindObjectsOfType<AWObj>();
            yield return null;
            List<AWObj> foundList = new List<AWObj>(foundItems);
            foreach (AWObj creature in foundList)
            {

                if (throwAWObj != null && throwAtAWObj != null) break;


                if (creature._objName == _thingToThrow)
                {
                    Debug.Log("found object to throw -> " + creature._objName);
                    throwAWObj = creature;
                }
                yield return null;
                if (creature._objName == _thingToThrowAt && creature != throwAWObj)
                {
                    Debug.Log("found object to be thrown at -> " + creature._objName);
                    throwAtAWObj = creature;
                }
                yield return null;
            }


            if (throwAWObj == null)
            {

                throwAWObj = anythingCreator.MakeObject(_thingToThrow, throwStartPos, false);
                while (!throwAWObj.AWObjMade)
                {
                    yield return new WaitForEndOfFrame();
                }
            }
            if (throwAtAWObj == null && !String.IsNullOrEmpty(_thingToThrowAt))
            {
                throwAtAWObj = anythingCreator.MakeObject(_thingToThrowAt);
                while (!throwAtAWObj.AWObjMade)
                {
                    yield return new WaitForEndOfFrame();
                }

            }

            if (!String.IsNullOrEmpty(_thingToThrowAt))
            {
                ThrowTarget throwTarget = throwAWObj.gameObject.AddComponent<ThrowTarget>();
                throwTarget.TargetTransform = throwAtAWObj.transform;
            }



        }
    }

    public IEnumerator CarryCoroutine(string _thingCarrying, string _thingToCarry, bool _makeNew = false)
    {
        // Debug.Log("Thing carrying: " + _thingCarrying);
        // Debug.Log("Thing being carried : " + _thingToCarry);

        AWObj carryingAWObj = null;
        AWObj carriedAWObj = null;
        //CarryTarget carryScript = null;
        FollowTarget followScript = null;

        if (_makeNew)
        {

            carryingAWObj = anythingCreator.MakeObject(_thingCarrying);
            if (carryingAWObj != null)
            {
                followScript = carryingAWObj.AddBehaviour<FollowTarget>();
            }

            carriedAWObj = anythingCreator.MakeObject(_thingToCarry);
            if (carriedAWObj != null)
            {
                carriedAWObj.AddBehaviour<RandomMovement>().SpeedMultiplier = 2f; ;
                if (followScript != null)
                {
                    followScript.targetController = carriedAWObj;
                }
                else
                {
                    Debug.Log("No followers found");
                }

            }

            yield return null;
        }
        else
        {
            AWObj[] foundItems = GameObject.FindObjectsOfType<AWObj>();
            yield return null;
            List<AWObj> foundList = new List<AWObj>(foundItems);
            foreach (AWObj creature in foundList)
            {

                if (carryingAWObj != null && carriedAWObj != null) break;

                if (creature._objName == _thingCarrying)
                {
                    carryingAWObj = creature;
                }
                yield return null;
                if (creature._objName == _thingToCarry && creature != carriedAWObj)
                {
                    carriedAWObj = creature;
                }
                yield return null;
            }


            if (carriedAWObj == null)
            {
                carriedAWObj = anythingCreator.MakeObject(_thingToCarry);
            }
            if (carryingAWObj == null)
            {
                carryingAWObj = anythingCreator.MakeObject(_thingCarrying);
            }

            followScript = carryingAWObj.AddBehaviour<FollowTarget>();
            followScript.targetController = carriedAWObj;
        }



        followScript.TargetRadius = 10f;

        while (!followScript.ReachedTarget)
        {
            followScript.moveSpeed *= 1.1f;
            yield return new WaitForEndOfFrame();
        }

        carriedAWObj.RemoveExistingBehaviours();
        CarryTarget carryTarget = carriedAWObj.gameObject.AddComponent<CarryTarget>();
        carryTarget.targetController = carryingAWObj;
        carryingAWObj.AddBehaviour<RandomMovement>();

    }

    #region Private Methods

    #endregion
}