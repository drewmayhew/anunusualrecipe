﻿using UnityEngine;
using System.Collections;
using System;
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
using UnityEngine.Windows.Speech;
using SpeechLib;
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
using System.Runtime.InteropServices;
#endif

/// <summary>
/// Handles voice activation and input.
/// </summary>
[System.Serializable]
public class AnythingSpeech : MonoBehaviour
{
    #region Plugin Methods

#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
    [DllImport("AWMacOSSpeech", CallingConvention = CallingConvention.Cdecl)]
    private static extern IntPtr StartAudioCapture();

    [DllImport("AWMacOSSpeech", CallingConvention = CallingConvention.Cdecl)]
    private static extern IntPtr StartRecognition();

    [DllImport("AWMacOSSpeech", CallingConvention = CallingConvention.Cdecl)]
    private static extern IntPtr GetSpeechTranscript();

    [DllImport("AWMacOSSpeech", CallingConvention = CallingConvention.Cdecl)]
    private static extern IntPtr StopRecognition();

    [DllImport("AWMacOSSpeech", CallingConvention = CallingConvention.Cdecl)]
    private static extern IntPtr StopAudioCapture();
#elif UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
    private DictationRecognizer m_DictationRecognizer;
#endif

    #endregion

    #region Fields

    private bool voiceInitialised = false;
    private bool listenForVoice = false;
    private bool listening = false;
    private string recognisedString;
    private CreatureKeywords creatureKeywords;
    private string lastProcessString;
    private Tuple<int, string> resultTuple;

    //Amount of time before listening times out, defaults to 2;
    private float timeOutValue = TIMEOUT_TIME;
    public float TimeOutValue
    {
        set
        {
            timeOutValue = value;
        }
        get
        {
            return timeOutValue;
        }
    }



    #region Delegates
    public delegate void AWSpeechHandler(string testResponse);
    public event AWSpeechHandler OnSpeechResponse;

    public delegate void AWSpeechDoneHandler(string testResponse);
    public event AWSpeechDoneHandler OnSpeechDone;

    #endregion

    private AWSpeechHandler responseCallback;
    private AWSpeechDoneHandler doneCallback;

    private float lastUtteranceTime;
    private const float TIMEOUT_TIME = 2f;
    private bool SHOW_DEBUG = false;
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN 
    SpVoice _winVoice;
#endif
    private static AnythingSpeech instance;
    public static AnythingSpeech Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject anythingCreatorGO = new GameObject();
                anythingCreatorGO.name = "Anything Speech";
                AnythingSpeech anythingCreator = anythingCreatorGO.AddComponent<AnythingSpeech>();
                instance = anythingCreator;
            }
            return instance;
        }
    }

    #endregion

    #region Unity Callbacks
    void Awake()
    {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
        m_DictationRecognizer = new DictationRecognizer();

        m_DictationRecognizer.DictationResult += (text, confidence) =>
        {
            if (SHOW_DEBUG) Debug.LogFormat("Dictation result: {0}", text);
            recognisedString = text;
        };

        m_DictationRecognizer.DictationHypothesis += (text) =>
        {
            if (SHOW_DEBUG) Debug.LogFormat("Dictation hypothesis: {0}", text);
            recognisedString += text;
        };

        m_DictationRecognizer.DictationComplete += (completionCause) =>
        {
            if (completionCause != DictationCompletionCause.Complete)
                if (SHOW_DEBUG) Debug.LogErrorFormat("Dictation completed unsuccessfully: {0}.", completionCause);
        };

        m_DictationRecognizer.DictationError += (error, hresult) =>
        {
            if (SHOW_DEBUG) Debug.LogErrorFormat("Dictation error: {0}; HResult = {1}.", error, hresult);
        };
#endif

        // Special voice setup here
        VoiceSetup();

        listenForVoice = false;

#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
        _winVoice = new SpVoice();
#endif
    }

    void Update()
    {
        if (voiceInitialised)
        {
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
            recognisedString = Marshal.PtrToStringAnsi(GetSpeechTranscript()).ToLower();
#endif

            if (recognisedString.ToLower() == "starting recognition")
            {
                recognisedString = "...";
            }
            else if (recognisedString != lastProcessString)
            {
                lastUtteranceTime = Time.time;
                if (OnSpeechResponse != null)
                    OnSpeechResponse(recognisedString);
                lastProcessString = recognisedString;
            }
            else if (Time.time - lastUtteranceTime > timeOutValue)
            {
                // If listening for specified time ignore TIMEOUT_TIME
                if (listening == true)
                {
                    StopListening();
                }

            }
        }


    }
    #endregion

    #region Public Methods

    /// <summary>
    /// Activates voice input and starts listening to user.
    /// </summary>
    /// <param name="responseCallback"></param>
    /// <param name="doneCallback"></param>
    public void StartListening(AWSpeechHandler responseCallback, AWSpeechDoneHandler doneCallback)
    {

        Debug.Log("A: startlistening");

        ActivateVoiceInput(true);
        this.responseCallback = responseCallback;
        this.doneCallback = doneCallback;
        OnSpeechResponse += this.responseCallback;
        OnSpeechDone += this.doneCallback;
        if (SHOW_DEBUG) Debug.Log("Listening started.");
        listening = true;
    }
    public void StartListening(AWSpeechDoneHandler doneCallback)
    {

        Debug.Log("B: startlistening");

        ActivateVoiceInput(true);
        this.responseCallback = null;
        this.doneCallback = doneCallback;
        OnSpeechResponse += this.responseCallback;
        OnSpeechDone += this.doneCallback;
        if (SHOW_DEBUG) Debug.Log("Listening started.");
        listening = true;
    }

    /// <summary>
    /// Deactivate voice input and stop listening.
    /// </summary>
    public void StopListening()
    {
        ActivateVoiceInput(false);
        OnSpeechDone?.Invoke(recognisedString);
        OnSpeechDone -= doneCallback;
        OnSpeechResponse -= responseCallback;
        if (SHOW_DEBUG) Debug.Log("Listening stopped.");
        listening = false;
    }

    /// <summary>
    /// Use speech synthesis to say input string.
    /// </summary>
    /// <param name="thingToSay">String to vocalise.</param>
    /// <param name="voiceName">Name of voice</param>
    public void Speak(string thingToSay, string voiceName = "none")
    {
        if (SHOW_DEBUG) Debug.Log("Speak string '" + thingToSay + "' requested.");
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
        if(SHOW_DEBUG) Debug.Log("Speak using MACOSSpeechSynth.");
        if (voiceName == "none")
        {
            MacOSSpeechSynth.Instance.Speak(thingToSay);
        }
        else
        {
            MacOSSpeechSynth.Instance.Speak(thingToSay,voiceName);
        }
#elif UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
        if (SHOW_DEBUG) Debug.Log("Speak using winVoice");
        if (_winVoice == null)
            _winVoice = new SpVoice();
        _winVoice.Speak(thingToSay, SpeechVoiceSpeakFlags.SVSFlagsAsync);
#endif
    }

    /// <summary>
    /// Listen for a specified amount of time and received a string to a callback.
    /// </summary>
    /// <param name="time">Time in seconds to listen.</param>
    /// <param name="doneCallBack">Callback handling the final detected string once listening completed.</param>
    public void ListenForInterval(float time, AWSpeechDoneHandler doneCallBack)
    {
        StartCoroutine(ListenForIntervalCoroutine(time, null, doneCallBack));
    }

    /// <summary>
    /// Listen for a specified amount of time and received a string to a callback.
    /// </summary>
    /// <param name="time">Time in seconds to listen.</param>
    /// <param name="responseCallBack">Callback handling any detected changes to the input string during listening.</param>
    /// <param name="doneCallBack">Callback handling the final detected string once listening completed.</param>
    public void ListenForInterval(float time, AWSpeechHandler responseCallBack, AWSpeechDoneHandler doneCallBack)
    {
        StartCoroutine(ListenForIntervalCoroutine(time, responseCallBack, doneCallBack));
    }

    /// <summary>
    /// Inumerator that starts listening, waits for a set interval and then stops listening.
    /// </summary>
    /// <param name="interval">Time in seconds to listen.</param>
    /// <param name="responseCallBack">Callback handling any detected changes to the input string during listening.</param>
    /// <param name="doneCallBack">Callback handling the final detected string once listening completed.</param>
    /// <returns></returns>
    private IEnumerator ListenForIntervalCoroutine(float interval, AWSpeechHandler responseCallBack, AWSpeechDoneHandler doneCallBack)
    {
        if (listening == false)
        {
            float oldTimeOut = TimeOutValue;
            TimeOutValue = interval;
            StartListening(responseCallBack, doneCallBack);
            yield return new WaitForSeconds(interval);
            StopListening();
            TimeOutValue = oldTimeOut;
        }
    }
    #endregion

    #region Private Methods


    /// <summary>
    /// Setup voice input.
    /// </summary>
    private void VoiceSetup()
    {
        //Debug.Log("voice setup");
        voiceInitialised = listenForVoice = false;
        creatureKeywords = new CreatureKeywords(this);
        recognisedString = "";
    }

    /// <summary>
    /// Initialise and activate voice input on noise detected/continue listening.
    /// </summary>
    /// <param name="shouldActivate">Bool indicating where voice input should be active, set from listener.</param>
    private void ActivateVoiceInput(bool shouldActivate)
    {
        if (shouldActivate)
        {
            if (!voiceInitialised)
            {
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
                Debug.Log(Marshal.PtrToStringAnsi(StartAudioCapture()));
#endif
                voiceInitialised = true;
            }
            //thinkingText = " ";
            recognisedString = "";
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
            Debug.Log(Marshal.PtrToStringAnsi(StartRecognition()));
#elif UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            m_DictationRecognizer.Start();
#endif
        }
        else
        {
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX 
            Debug.Log(Marshal.PtrToStringAnsi(StopRecognition()));
#elif UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            if (m_DictationRecognizer.Status == SpeechSystemStatus.Running)
            {
                m_DictationRecognizer.Stop();
            }

#endif
            voiceInitialised = false;
        }
        listenForVoice = shouldActivate;
    }
    #endregion
}