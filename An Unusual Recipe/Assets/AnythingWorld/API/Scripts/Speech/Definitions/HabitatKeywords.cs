﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class HabitatKeywords
{

    private List<string> _habitats;

    public List<string> Habitats
    {
        get
        {
            return _habitats;
        }
    }

    private readonly bool showDebugLog = false;
    public Tuple<int, string> ProcessUtterance(string utterance)
    {
        // Debug.Log($"Processutterance {utterance}");
        if (_habitats == null)
            PopulateHabitats();

        string[] words = utterance.Split(' ');
        string findHabitat = "none";
        int findInd = -1;

        for (int i = 0; i < words.Length; i++)
        {
            string word = words[i];
            if (IsPreferredHabitat(word))
            {
                findHabitat = word;
                findInd = utterance.IndexOf(word) + word.Length;
                break;
            }
        }


        if (showDebugLog) Debug.Log($"HabitatKeywords ProcessUtterance returning {findInd},{findHabitat}");
        return new Tuple<int, string>(findInd, findHabitat);
    }

    public bool IsPreferredHabitat(string thingName)
    {
        bool havePreferred = false;
        if (_habitats.Contains(thingName))
            havePreferred = true;
        return havePreferred;
    }

    private void PopulateHabitats()
    {
        SortedDictionary<string, HabitatDescription> habitatList = HabitatMap.GetHabitats();
        _habitats = new List<string>();
        foreach (KeyValuePair<string, HabitatDescription> kvp in habitatList)
        {
            _habitats.Add(kvp.Key);
        }
    }


}
