using System;

[Serializable]
public class AWQueryResult
{
    public string action = "none";
    public AWParameterResult parameters;
}