﻿using Dummiesman;
using System.IO;
using System.Text;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;
using System.Text.RegularExpressions;
#if UNITY_EDITOR
using Unity.EditorCoroutines.Editor;
#endif
using UnityEngine.Networking;

[ExecuteAlways]
[Serializable]
//[RequireComponent(typeof(AWBehaviourController))]
public class AWObj : MonoBehaviour
{
    #region Fields
    private const string API_PREFIX = "https://anything-world-api.appspot.com/anything?name=";
    private const string API_SUFFIX = "&key=";
    private const string APP_SUFFIX = "&app=";

    public const bool SHOW_DEBUG_BOXES = false;

    private Dictionary<string, string> _objParts;
    private string _materialName;
    private string _textureName;
    private int _objIndex;
    private Material _objMaterial;
    private Material _firstPartMaterial;
    private Texture _objTexture;
    private AWThing _requestedObject;
    private PolyThing _polyRequest;
    private Shader _universalLitShader;
    private Shader _fishWobbleShader;
    private Shader _fishBigWobbleShader;
    private Shader _fishWriggleShader;
    private Shader _fishWriggleBigShader;
    private Shader _uniformWriggleShader;
    private Shader _uniformSlitherShader;
    private Shader _uniformSlitherVerticalShader;
    private Shader _quadrupedCrawlShader;
    private Material _anythingMaterial;
    [SerializeField, HideInInspector]
    private GameObject _mappedPrefab;
    private bool _polyObject;
    [SerializeField, HideInInspector]
    public string _objName;
    [SerializeField, HideInInspector]
    private string _category;
    [SerializeField, HideInInspector]
    private string _prefabName;
    [SerializeField, HideInInspector]
    private string _categoryBehaviourName;
    [SerializeField, HideInInspector]
    private string _behaviour;
    [SerializeField, HideInInspector]
    private bool _hasBehaviour;
    [SerializeField, HideInInspector]
    public AWBehaviourController behaviourController;
    [SerializeField, HideInInspector]
    public bool _awObjMade = false;
    [HideInInspector]
    public bool _awObjNotFound = false;
    [HideInInspector]
    public bool _awKeyInvalid = false;
    [HideInInspector]
    public bool _awAppIdInvalid = false;
    private List<GameObject> _loadedObjs;
    private List<Renderer> _objRenderers;
    private List<MeshFilter> _objMeshes;
    private Vector3 _awObjectPosition = Vector3.zero;
    private List<Rigidbody> _prefabRBs;
    private Collider[] _prefabColliders;
    private Joint[] _prefabJoints;
    private AWBehaviour[] _awBehaviours;
    private GameObject _boundsShow;
    private string _attribution;
    private Rigidbody _polyRbody;
    private bool _hasCollider;
    [SerializeField, HideInInspector]
    private bool _madeAtRuntime;
    private Vector2 _objectScale;
    [SerializeField, HideInInspector]
    private float _boundsYOffset;
    [SerializeField, HideInInspector]
    public float BoundsYOffset
    {
        get
        {
            return _boundsYOffset;
        }
    }
    [SerializeField, HideInInspector]
    private float _objScale;
    [SerializeField, HideInInspector]
    public float ObjectScale
    {
        get
        {
            return _objScale;
        }
    }
    private bool addDefaultBehaviour = true;
    [SerializeField, HideInInspector]
    public GameObject behaviourObject;
    [SerializeField, HideInInspector]
    public GameObject awThing
    {
        get
        {
            if (_mappedPrefab == null)
            {
                if (_awObjMade)
                {
                    AddCollider addedCollider = GetComponentInChildren<AddCollider>();
                    if (addedCollider != null) 
                    {
                        _mappedPrefab = GetComponentInChildren<AddCollider>().gameObject;
                    }
                    else
                    {
                        _mappedPrefab = transform.gameObject;
                    }

                    return _mappedPrefab;
                }
                Debug.LogError("No _mappedPrefab found yet, object may not have finished being made");
                return null;
            }
            else
            {
                return _mappedPrefab;
            }

        }
    }

    [HideInInspector]
    public bool AWObjMade
    {
        get
        {
            return _awObjMade;
        }
        set
        {
            _awObjMade = value;
        }
    }
    [HideInInspector]
    public bool AWObjNotFound
    {
        get
        {
            return _awObjNotFound;
        }
        set
        {
            _awObjNotFound = value;
        }
    }
    [HideInInspector]
    public bool AWKeyInvalid
    {
        get
        {
            return _awKeyInvalid;
        }
        set
        {
            _awKeyInvalid = value;
        }
    }
    [HideInInspector]
    public bool AWAppIdInvalid
    {
        get
        {
            return _awAppIdInvalid;
        }
        set
        {
            _awAppIdInvalid = value;
        }
    }
    [HideInInspector]
    #endregion
    public bool HasBehaviour
    {
        get
        {
            return _hasBehaviour;
        }
    }

    #region Public Fields
    public void MakeAWObj(string objName, Vector3 objPos)
    {
        _hasBehaviour = _hasCollider = true;
        _awObjectPosition = objPos;
        if (behaviourObject == null)
        {
            BuildBehaviourContainer();
        }
        MakeAWObj(objName);
    }

    public void MakeAWObj(string objName, bool hasBehaviour)
    {
        _hasBehaviour = hasBehaviour;
        _hasCollider = true;
        MakeAWObj(objName);
    }

    public void MakeAWObj(string objName, bool hasBehaviour, bool hasCollider)
    {
        _hasBehaviour = hasBehaviour;
        _hasCollider = hasCollider;
        MakeAWObj(objName);
    }

    public void MakeAWObj(string objName)
    {
        //Debug.Log("AWObj.MakeAWObj: " + objName);
        if (_hasBehaviour)
        {
            behaviourController = gameObject.AddComponent<AWBehaviourController>();
            behaviourController.parentAWObj = this;
        }

        if (Application.isEditor && !Application.isPlaying)
        {
            _madeAtRuntime = false;
        }
        else
        {
            _madeAtRuntime = true;
        }

        _awObjMade = false;

        _objName = objName.ToLower();
        if (Application.isEditor && !Application.isPlaying)
        {
#if UNITY_EDITOR
            EditorCoroutineUtility.StartCoroutine(RequestObject(_objName), this);
#endif
        }
        else
        {
            AnythingSetup.Instance.ShowLoading(true);
            StartCoroutine(RequestObject(_objName));
        }


    }
    public void ActivateRBs(bool shouldActivate)
    {
        if (_prefabRBs == null)
            GatherRBs();

        foreach (Rigidbody rb in _prefabRBs)
        {
            if ((shouldActivate && _hasBehaviour) || !shouldActivate)
            {
                rb.isKinematic = shouldActivate ? false : true;
            }
            rb.velocity = rb.angularVelocity = Vector3.zero;
        }

        // Debug.Log($"ActivateColliders {_hasCollider} {_hasBehaviour}");

        if ((_hasCollider) || (!_hasCollider && !shouldActivate))
        {

            foreach (Collider cl in _prefabColliders)
            {
                cl.enabled = shouldActivate;
            }
        }


        // foreach(Joint jnt in _prefabJoints)
        // {
        //     jnt.gameObject.SetActive(shouldActivate);
        // }

        if (shouldActivate && !_hasBehaviour)
            return;

        foreach (AWBehaviour awB in _awBehaviours)
        {
            awB.enabled = shouldActivate;
        }


        // Debug.Log($"Activate {gameObject.name} : {shouldActivate}");
    }
    public string GetObjCategory()
    {
        return _category;
    }
    public string GetObjName()
    {
        return _prefabName;
    }
    public string GetObjCatBehaviour()
    {
        if (_hasBehaviour)
        {
            return _categoryBehaviourName;
        }
        else
        {
            return _category;
        }
    }



    #endregion

    #region Private Methods
    private void Awake()
    {
        // AnythingSettings _anythingSettings = AnythingSettings.GetSerializedSettings();

        _hasBehaviour = _hasCollider = true;

        /*
        * TODO: be careful, this flag fixes editor & runtime - maybe find a cleaner way! - GM
        */

        if (_madeAtRuntime)
        {
            //_awObjMade = false;
        }
        else
        {
            //_awObjMade = true;
        }
        Init();
        _universalLitShader = Shader.Find("Anything World/Simple Lit");
        _fishWobbleShader = Shader.Find("Shader Graphs/Fish Animation");
        _fishWriggleShader = Shader.Find("Shader Graphs/Fish Vertical Animation");
        _fishWriggleBigShader = Shader.Find("Shader Graphs/Fish Vertical Big Animation");
        _uniformWriggleShader = Shader.Find("Shader Graphs/Wriggle Animation");
        _uniformSlitherShader = Shader.Find("Shader Graphs/Slither Animation");
        _uniformSlitherVerticalShader = Shader.Find("Shader Graphs/Slither Vertical Animation");
        _quadrupedCrawlShader = Shader.Find("Shader Graphs/Crawler Animation");
        _fishBigWobbleShader = Shader.Find("Shader Graphs/Fish Big Animation");
        _anythingMaterial = Resources.Load("Materials/AnythingMaterial") as Material;
    }

    private void Init()
    {
        _objParts = new Dictionary<string, string>();
        _objRenderers = new List<Renderer>();
        _objMeshes = new List<MeshFilter>();
        _loadedObjs = new List<GameObject>();
        _objIndex = 0;

    }




    private IEnumerator RequestObject(string objName)
    {
        // Debug.Log("AWObj.RequestObject: " + objName);

        if (String.IsNullOrEmpty(AnythingSettings.Instance.AppName))
        {
            AnythingSetup.Instance.ShowLoading(false);
            Debug.LogError("Please enter an App Name in AnythingSettings!");
            _awAppIdInvalid = true;
            yield break;
        }

        /* TODO: put check back in when appropriate
        if (String.IsNullOrEmpty(AnythingSettings.Instance.AgentId))
        {
            AnythingSetup.Instance.ShowLoading(false);
            Debug.LogError("Please enter a Dialogflow agent id in AnythingSettings!");
            _awAppIdInvalid = true;
            yield break;
        }
        */

        _polyObject = false;

        // trim our request string
        char[] charsToTrim = { '*', ' ', '\'', ',', '.' };
        objName = objName.Trim(charsToTrim);

        string APICall = API_PREFIX + objName + API_SUFFIX + AnythingSettings.Instance.APIKey + APP_SUFFIX + AnythingSettings.Instance.AppName;
        if (AnythingSettings.Instance.showDebugMessages) Debug.Log(APICall);
        UnityWebRequest www = UnityWebRequest.Get(APICall);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            AnythingSetup.Instance.ShowLoading(false);
            Debug.LogError("Network Error for Anything World using API Key: " + AnythingSettings.Instance.APIKey);
            _awKeyInvalid = true;
            yield break;
        }

        string result = www.downloadHandler.text;

        if (AnythingSettings.Instance.showDebugMessages) Debug.LogWarning("Result:" + result);
        // remove array wrappings
        // TODO: amend API!
        string text = Regex.Replace(www.downloadHandler.text, @"[[\]]", "");


        // TODO: add error message here!
        if (result.ToLower().IndexOf("no model found") != -1)
        {
            AnythingSetup.Instance.ShowLoading(false);
            Debug.LogError($"Model not found for {objName} in database.");
            _awObjNotFound = true;
            yield break;
        }

        _requestedObject = JsonUtility.FromJson<AWThing>(text);
        _attribution = objName + " by " + _requestedObject.author;

        if (_requestedObject.source.ToLower().IndexOf("poly") == -1)
        {
            CategoryMap.SetCategory(_requestedObject.type, _objName);
            BehaviourMap.SetBehaviour(_requestedObject.behaviour, _objName);

            BuildAbout();
        }
        else
        {
            _polyRequest = JsonUtility.FromJson<PolyThing>(text);
            _textureName = _polyRequest.basecolor;

            _polyObject = true;
            addDefaultBehaviour = false; // NB: no default behaviours added to poly objects
            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                EditorCoroutineUtility.StartCoroutine(LoadMaterial(), this);
#endif
            }
            else
            {
                StartCoroutine(LoadMaterial());
            }
        }


    }

    /// <summary>
    /// Loads OBJ into game object and parents to this transform.
    /// </summary>
    /// <returns></returns>
    private IEnumerator LoadPolyObj()
    {
        //URL of model part
        string ObjLocation = _polyRequest.model;

        //Request part from server
        UnityWebRequest www = UnityWebRequest.Get(ObjLocation);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.LogError("Network Error for Poly: " + www.error);
            yield break;
        }
        MemoryStream textStream = new MemoryStream(Encoding.UTF8.GetBytes(www.downloadHandler.text));
        MemoryStream matStream = new MemoryStream(Encoding.UTF8.GetBytes(_rawMaterial));
        //Load game object using datastream
        GameObject loadedObj = new OBJLoader().Load(textStream, matStream);
        //Attach mesh renderer to new game object.
        MeshRenderer mRenderer = loadedObj.GetComponentInChildren(typeof(MeshRenderer)) as MeshRenderer;
        //Set materials
        Material[] polyMaterials = mRenderer.sharedMaterials;

        GameObject meshGO = mRenderer.gameObject;
        //NEW_ADD
        meshGO.GetComponent<MeshRenderer>().enabled = false;

        if (_hasCollider)
        {
            BoxCollider pCollider = meshGO.AddComponent<BoxCollider>();
            _polyRbody = meshGO.AddComponent<Rigidbody>();
            _polyRbody.isKinematic = true;
        }

        _objRenderers.Add(mRenderer.GetComponent<Renderer>());
        _objMeshes.Add(mRenderer.GetComponent<MeshFilter>());
        _loadedObjs.Add(loadedObj);

        // Debug.Log("objTexture ---> " + _objTexture);

        //Set color and texture for objects.
        if (_objTexture != null)
        {
            foreach (Material dMat in polyMaterials)
            {
                dMat.SetTexture("_BaseMap", _objTexture);
                dMat.SetColor("_BaseColor", Color.white); // TODO: forcing color to white if texture on poly objects here, why is this needed?
            }
        }

        //Set transform parent to be this game object.
        loadedObj.transform.parent = this.transform;

        //Centre transform and size to bounds.
        CenterAndSizeObjectToBounds();

        //Mark object as loaded.
        StartCoroutine(MarkObjectAsDone(0.05f));
    }

    /// <summary>
    /// Converts string to title case.
    /// </summary>
    /// <param name="stringToConvert">String to convert.</param>
    /// <returns>String with titlecase.</returns>
    private string ToTitleCase(string stringToConvert)
    {
        string firstChar = stringToConvert[0].ToString();
        return (stringToConvert.Length > 0 ? firstChar.ToUpper() + stringToConvert.Substring(1) : stringToConvert);

    }

    /// <summary>
    /// Converts string in format "1.23m" to float.
    /// </summary>
    /// <param name="dimension">String to convert.</param>
    /// <returns>float</returns>
    private float parseDimension(string dimension)
    {
        string trimmedDim = dimension.Replace("m", "");

        float floatDim = 1f;
        float pFloat;

        if (float.TryParse(trimmedDim, out pFloat))
        {
            floatDim = pFloat;
        }


        return floatDim;
    }

    /// <summary>
    /// Creates dictionary of parts from the JSON request.
    /// </summary>
    private void BuildAbout()
    {
        _category = CategoryMap.GetCategory(_objName);

        _prefabName = PrefabMap.PrefabName(_category, _behaviour);
        // get common elements
        AWThingLook awThingLook = _requestedObject.model.other;
        _materialName = awThingLook.material;
        _textureName = awThingLook.texture;
        _behaviour = _requestedObject.behaviour;
        _categoryBehaviourName = PrefabMap.PrefabName(_category, _behaviour);
        _objectScale = new Vector2(parseDimension(_requestedObject.scale.length), parseDimension(_requestedObject.scale.height));

        AWParts awThingParts = _requestedObject.model.parts;

        string JSONRep = JsonUtility.ToJson(_requestedObject.model.parts);

        switch (_category)
        {
            case "multileg_flyer":
                AWMultilegFlyerParts awMultilegFlyerParts = JsonUtility.FromJson<AWMultilegFlyerParts>(JSONRep);
                BuildObjParts(awMultilegFlyerParts);
                break;
            case "multileg_crawler":
                AWMultilegCrawlerParts awMultilegCrawlerParts = JsonUtility.FromJson<AWMultilegCrawlerParts>(JSONRep);
                BuildObjParts(awMultilegCrawlerParts);
                break;
            case "multileg_crawler_big":
                AWMultilegCrawlerParts awMultilegCrawlerBigParts = JsonUtility.FromJson<AWMultilegCrawlerParts>(JSONRep);
                BuildObjParts(awMultilegCrawlerBigParts);
                break;
            case "multileg_crawler_eight":
                AWMultilegCrawlerEightParts awMultilegCrawlerEightParts = JsonUtility.FromJson<AWMultilegCrawlerEightParts>(JSONRep);
                BuildObjParts(awMultilegCrawlerEightParts);
                break;
            case "winged_standing":
                AWWingedStandingParts awWingedStandingParts = JsonUtility.FromJson<AWWingedStandingParts>(JSONRep);
                BuildObjParts(awWingedStandingParts);
                break;
            case "winged_standing_small":
                AWWingedStandingParts awWingedStandingSmallParts = JsonUtility.FromJson<AWWingedStandingParts>(JSONRep);
                BuildObjParts(awWingedStandingSmallParts);
                break;
            case "winged_flyer":
                AWWingedFlyerParts awWingedFlyerParts = JsonUtility.FromJson<AWWingedFlyerParts>(JSONRep);
                BuildObjParts(awWingedFlyerParts);
                break;
            case "quadruped_standard":
            case "quadruped":
                AWQuadParts awQuadParts = JsonUtility.FromJson<AWQuadParts>(JSONRep);
                BuildObjParts(awQuadParts);
                break;
            case "quadruped_ungulate":
                AWQuadParts awQuadUngulateParts = JsonUtility.FromJson<AWQuadParts>(JSONRep);
                BuildObjParts(awQuadUngulateParts);
                break;
            case "quadruped_fat":
                AWQuadFatParts awQuadFatParts = JsonUtility.FromJson<AWQuadFatParts>(JSONRep);
                BuildObjParts(awQuadFatParts);
                break;
            case "quadruped_fat_small_generic":
                AWQuadFatParts awQuadFatSmallParts = JsonUtility.FromJson<AWQuadFatParts>(JSONRep);
                BuildObjParts(awQuadFatSmallParts);
                break;
            case "quadruped_fat_shortleg_generic":
                AWQuadFatParts awQuadFatShortLegParts = JsonUtility.FromJson<AWQuadFatParts>(JSONRep);
                BuildObjParts(awQuadFatShortLegParts);
                break;
            case "hopper":
                AWHopperParts awHopperParts = JsonUtility.FromJson<AWHopperParts>(JSONRep);
                BuildObjParts(awHopperParts);
                break;
            case "vehicle_four_wheel":
                AWVehicleFourWheelParts awVehicleFourParts = JsonUtility.FromJson<AWVehicleFourWheelParts>(JSONRep);
                BuildObjParts(awVehicleFourParts);
                break;
            case "vehicle_three_wheel":
                AWVehicleThreeWheelParts awVehicleThreeParts = JsonUtility.FromJson<AWVehicleThreeWheelParts>(JSONRep);
                BuildObjParts(awVehicleThreeParts);
                break;
            case "vehicle_two_wheel":
                AWVehicleTwoWheelParts awVehicleTwoParts = JsonUtility.FromJson<AWVehicleTwoWheelParts>(JSONRep);
                BuildObjParts(awVehicleTwoParts);
                break;
            case "vehicle_one_wheel":
                AWVehicleOneWheelParts awVehicleOneParts = JsonUtility.FromJson<AWVehicleOneWheelParts>(JSONRep);
                BuildObjParts(awVehicleOneParts);
                break;
            case "vehicle_load":
                AWVehicleLoadParts awVehicleLoadParts = JsonUtility.FromJson<AWVehicleLoadParts>(JSONRep);
                BuildObjParts(awVehicleLoadParts);
                break;
            case "vehicle_flyer":
                AWVehicleFlyerParts awVehicleFlyerParts = JsonUtility.FromJson<AWVehicleFlyerParts>(JSONRep);
                BuildObjParts(awVehicleFlyerParts);
                break;
            case "vehicle_propeller":
                AWVehiclePropellerParts awVehiclePropellerParts = JsonUtility.FromJson<AWVehiclePropellerParts>(JSONRep);
                BuildObjParts(awVehiclePropellerParts);
                break;
            case "biped":
                AWBipedParts awBipedParts = JsonUtility.FromJson<AWBipedParts>(JSONRep);
                BuildObjParts(awBipedParts);
                break;
            case "uniform":
            case "vehicle_other":
            case "vehicle_uniform":
                AWUniformParts awUniformParts = JsonUtility.FromJson<AWUniformParts>(JSONRep);
                BuildObjParts(awUniformParts);
                break;
            case "floater":
                AWFloaterParts awFloaterParts = JsonUtility.FromJson<AWFloaterParts>(JSONRep);
                BuildObjParts(awFloaterParts);
                break;
            case "scenery":
                AWSceneryParts aWSceneryParts = JsonUtility.FromJson<AWSceneryParts>(JSONRep);
                BuildObjParts(aWSceneryParts);
                break;
            default:
                Debug.LogError("NO CATEGORY FOUND FOR OBJECT -> " + _category + " -> ABORTING!");
                break;

        }

        if (Application.isEditor && !Application.isPlaying)
        {
#if UNITY_EDITOR
            EditorCoroutineUtility.StartCoroutine(LoadMaterial(), this);
#endif
        }
        else
        {
            StartCoroutine(LoadMaterial());
        }


    }


    /// <summary>
    /// Builds list of parts from AWParts parts class.
    /// </summary>
    /// <param name="parts">AWParts parts class.</param>
    private void BuildObjParts(AWParts parts)
    {
        Type type = parts.GetType();

        FieldInfo[] properties = type.GetFields();
        foreach (FieldInfo property in properties)
        {
            string propName = property.Name.ToString();
            string propValue = property.GetValue(parts).ToString();
            if (propValue.Length > 1)
            {
                if (!_objParts.ContainsKey(propName))
                {
                    _objParts.Add(propName, propValue);
                }
            }
        }
    }

    private string _rawMaterial;

    /// <summary>
    /// Load material from JSON request.
    /// </summary>
    /// <returns>Coroutine</returns>
    private IEnumerator LoadMaterial()
    {

        //Debug.Log("LoadMaterial");

        string matLocation = "";
        if (_polyObject)
        {
            matLocation = _polyRequest.mtl;
        }
        else
        {
            matLocation = _materialName;
        }

        UnityWebRequest www = UnityWebRequest.Get(matLocation);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.LogError("Network Error for material : " + matLocation);
            yield break;
        }

        _rawMaterial = www.downloadHandler.text;

        if (Application.isEditor && !Application.isPlaying)
        {
#if UNITY_EDITOR
            EditorCoroutineUtility.StartCoroutine(LoadTexture(), this);
#endif
        }
        else
        {
            StartCoroutine(LoadTexture());
        }

    }

    /// <summary>
    /// Loads texture from JSON request.
    /// </summary>
    /// <returns></returns>
    private IEnumerator LoadTexture()
    {
        //Debug.Log("textureLocation = " + _textureName);
        _objTexture = null;

        if (_textureName != null)
        {
            if (_textureName.Length > 2 && _textureName.ToLower().IndexOf("no texture") == -1)
            {

                UnityWebRequest www = UnityWebRequestTexture.GetTexture(_textureName);
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.LogError("Network Error");
                    yield break;
                }

                _objTexture = DownloadHandlerTexture.GetContent(www);
            }
        }

        if (_polyObject)
        {
            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                EditorCoroutineUtility.StartCoroutine(LoadPolyObj(), this);
#endif
            }
            else
            {
                StartCoroutine(LoadPolyObj());
            }
        }
        else
        {
            GetPrefabMap();
        }
    }

    /// <summary>
    /// Loads pref from category and behaviour.
    /// </summary>
    /// <remarks>
    /// Adds non-kinematic rigidbodies.
    /// Gets joints in prefab and sets to _prefabJoints.
    /// Gets colliders in prefab and sets to _prefabColliders.
    /// Gets AWBehaviours and sets to _awBehaviours.
    /// </remarks>
    private void GetPrefabMap()
    {

        //Debug.Log("GetPrefabMap");

        GameObject prefabObj = PrefabMap.ThingPrefab(_category, _behaviour);
        _mappedPrefab = Instantiate(prefabObj) as GameObject;

        // only add non-kinematic RBs into our list
        _prefabRBs = new List<Rigidbody>();
        Rigidbody[] allRBs = _mappedPrefab.GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rb in allRBs)
        {
            if (!rb.isKinematic)
            {
                _prefabRBs.Add(rb);
            }
        }

        _prefabColliders = _mappedPrefab.GetComponentsInChildren<Collider>();
        _prefabJoints = _mappedPrefab.GetComponentsInChildren<Joint>();
        _awBehaviours = _mappedPrefab.GetComponentsInChildren<AWBehaviour>();
        ActivateRBs(false);
        _mappedPrefab.transform.parent = transform;

        if (Application.isEditor && !Application.isPlaying)
        {
#if UNITY_EDITOR
            EditorCoroutineUtility.StartCoroutine(LoadParts(), this);
#endif
        }
        else
        {
            StartCoroutine(LoadParts());
        }

    }

    private Material firstPartMaterial;

    private IEnumerator LoadParts()
    {

        GameObject loadedObj;

        bool _appliedMaterial = false;

        int partCount = 0;

        foreach (KeyValuePair<string, string> kvp in _objParts)
        {
            string ObjLocation = kvp.Value;

            UnityWebRequest www = UnityWebRequest.Get(ObjLocation);
            yield return www.SendWebRequest();

            if (!www.isNetworkError && !www.isHttpError)
            {
                // Debug.Log(www.downloadHandler.text);
            }
            else
            {
                Debug.LogError("Network Error for : " + ObjLocation);
                yield break;
            }

            MemoryStream textStream = new MemoryStream(Encoding.UTF8.GetBytes(www.downloadHandler.text));
            MemoryStream matStream = new MemoryStream(Encoding.UTF8.GetBytes(_rawMaterial));

            // if we have a material texture create a new obj loader for each object
            // this hack / workaround fixes material issues for objects like bee
            if (_objTexture != null)
            {
                // if always applied will fix models like bee
                // Debug.Log("A: part with texture");
                loadedObj = new OBJLoader().Load(textStream, matStream);
            }
            else
            {
                if (_appliedMaterial)
                {
                    // Debug.Log("B: part with _appliedMaterial");
                    loadedObj = new OBJLoader().Load(textStream, matStream);

                }
                else
                {
                    // Debug.Log("A: part normal material");
                    loadedObj = new OBJLoader().Load(textStream, matStream);
                    _appliedMaterial = true;
                }
            }

            string partName = kvp.Key;

            loadedObj.transform.parent = _mappedPrefab.transform.FindDeepChild(partName);
            MeshRenderer mRenderer = loadedObj.GetComponentInChildren(typeof(MeshRenderer)) as MeshRenderer;

            if (mRenderer == null)
            {
                Debug.Log(partName + " >> problem");
                continue;
            }



            _objMaterial = mRenderer.sharedMaterial;

            if (!_objTexture)
            {
                if (partCount == 0)
                {
                    _firstPartMaterial = _objMaterial;
                }
                else
                {
                    mRenderer.sharedMaterial = _firstPartMaterial;
                }
            }

            GameObject rendererGO = mRenderer.gameObject;
            mRenderer.enabled = false;
            _objRenderers.Add(mRenderer.GetComponent<Renderer>());
            _objMeshes.Add(mRenderer.GetComponent<MeshFilter>());
            _loadedObjs.Add(loadedObj);

            switch (_behaviour)
            {
                case "swim":
                    Shader swimShader = _fishWobbleShader;
                    // TODO: another way, large swimmable category?
                    if (_objName.IndexOf("whale") != -1)
                        swimShader = _fishBigWobbleShader;
                    SwitchShaders(mRenderer, swimShader);
                    break;
                case "wriggle":
                    SwitchShaders(mRenderer, _uniformWriggleShader);
                    break;
                case "swim2":
                    SwitchShaders(mRenderer, _fishWriggleShader);
                    break;
                case "swim3":
                    SwitchShaders(mRenderer, _fishWriggleBigShader);
                    break;
                case "crawl":
                    SwitchShaders(mRenderer, _quadrupedCrawlShader);
                    break;
                case "slither":
                    SwitchShaders(mRenderer, _uniformSlitherShader);
                    break;
                case "slithervertical":
                    SwitchShaders(mRenderer, _uniformSlitherVerticalShader);
                    break;

                default:
                    if (_objTexture != null)
                    {
                        _objMaterial.SetTexture("_BaseMap", _objTexture);
                        // TODO: beware, i have forced this to white for now if a texture present, the values in materials came through as (0,0,0) black very often hence the force - will need amending later
                        _objMaterial.SetColor("_BaseColor", Color.white);

                        // Debug.Log("Forcing texture color to white!");
                    }
                    break;
            }

            _objIndex++;
        }

        CenterAndSizeObjectToBounds();
        StartCoroutine(MarkObjectAsDone(0.05f));


    }

    /// <summary>
    /// Set all renderers to be enabled, showing the object as visible.
    /// </summary>
    private void MakeRenderersVisible()
    {
        foreach (MeshRenderer rend in _objRenderers)
        {
            rend.enabled = true;
        }
    }
    private void SwitchShaders(Renderer sRenderer, Shader sShader)
    {
        Material[] _allMats = sRenderer.sharedMaterials;
        foreach (Material sMat in _allMats)
        {
            Color matColor = sMat.GetColor("_BaseColor");
            sMat.shader = sShader;
            if (_objTexture != null)
            {
                sMat.SetTexture("Texture2D_4D1649D6", _objTexture);
            }
            else
            {
                sMat.SetColor("Color_C46A69DE", matColor);
            }
        }
    }
    private Vector3 GetObjectBounds()
    {
        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
        foreach (Renderer objRenderer in _objRenderers)
        {
            // Debug.Log("Object bounds: " + objRenderer.bounds);
            bounds.Encapsulate(objRenderer.bounds);
        }
        return bounds.size;
    }
    private Vector3 GetObjectOrigin()
    {
        Bounds totalBounds = new Bounds(Vector3.zero, Vector3.zero);
        // GameObject boundsCenterDebug;
        Mesh mMesh;
        foreach (MeshFilter mFilter in _objMeshes)
        {
            mMesh = mFilter.sharedMesh;
            totalBounds.Encapsulate(mMesh.bounds);
        }

        // Debug visualisation
        /*if (SHOW_DEBUG_BOXES)
        {
            _boundsShow = GameObject.CreatePrimitive(PrimitiveType.Cube);
            _boundsShow.GetComponent<BoxCollider>().enabled = false;
            _boundsShow.transform.localScale = totalBounds.size;
            _boundsShow.transform.position = totalBounds.center;
            _boundsShow.transform.parent = transform;
            _boundsShow.GetComponent<Renderer>().material = Resources.Load("Materials/DebugMaterial") as Material;
        }
        */
        _boundsYOffset = totalBounds.extents.y;

        return totalBounds.center;

    }

    private float GetObjectRelativeScale()
    {
        float maxDimension = _objectScale.x > _objectScale.y ? _objectScale.x : _objectScale.y;
        // default to 1 in case no scale value found
        if (maxDimension == 0)
        {
            maxDimension = 1;
        }
        else
        {
            // TODO: we're resizing here, revisit! 
            float sqrDimension = (float)Math.Sqrt(maxDimension);
            float divDimension = (sqrDimension) / (sqrDimension / 1.5f);
            maxDimension /= divDimension;
        }

        // Debug.Log("maxDimension = " + maxDimension);

        return maxDimension;
    }

    private void CenterAndSizeObjectToBounds()
    {
        Vector3 objectBounds = GetObjectBounds();
        Vector3 objectOrigin = GetObjectOrigin();

        float max = objectBounds.x;
        if (max < objectBounds.y)
            max = objectBounds.y;
        if (max < objectBounds.z)
            max = objectBounds.z;

        //Debug.Log("max bounds = " + gameObject.name + " " + max);
        if (_polyObject)
        {
            _objScale = 1f;
        }
        else
        {
            // TODO: we are clamping scale range for now - revisit?
            _objScale = Mathf.Clamp(GetObjectRelativeScale(), 0.25f, 5f);
        }
        float boundsScale = (_objScale * 10f) / max;

        _boundsYOffset *= boundsScale;
        /*
        if (SHOW_DEBUG_BOXES)
        {
            Vector3 currBoundsScale = _boundsShow.transform.localScale;
            currBoundsScale *= boundsScale;
            _boundsShow.transform.localScale = currBoundsScale;
            _boundsShow.transform.position -= objectOrigin;
        }
        */
        foreach (GameObject loadedObj in _loadedObjs)
        {
            loadedObj.transform.localScale = new Vector3(boundsScale, boundsScale, boundsScale);
            loadedObj.transform.position -= objectOrigin * boundsScale;
        }

    }
    private IEnumerator MarkObjectAsDone(float doneDelay = 0.1f)
    {
        if (Application.isEditor && !Application.isPlaying)
        {
#if UNITY_EDITOR
            yield return new EditorWaitForSeconds(doneDelay);
#endif
        }
        else
        {
            yield return new WaitForSeconds(doneDelay);
        }

        if (_hasBehaviour && _polyObject)
        {
            _polyRbody.isKinematic = false;
        }
        else if (!_polyObject)
        {
            ActivateRBs(true);
        }

        _awObjMade = true;

        //Debug.Log("AW Obj Made");

        AnythingSetup.Instance.ShowAttribution(_attribution);

        if (Application.isPlaying)
        {
            AnythingSetup.Instance.ShowLoading(false);
        }


        if (addDefaultBehaviour)
        {
            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                EditorCoroutineUtility.StartCoroutineOwnerless(AddDefaultBehaviour());
#endif
            }
            else
            {
                StartCoroutine(AddDefaultBehaviour());
            }

        }

        MakeRenderersVisible();
    }
    private void GatherRBs()
    {
        _prefabRBs = new List<Rigidbody>();
        Rigidbody[] allRBs = gameObject.GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rb in allRBs)
        {
            if (!rb.isKinematic)
            {
                _prefabRBs.Add(rb);
            }
        }

        _prefabColliders = gameObject.GetComponentsInChildren<Collider>();
        _prefabJoints = gameObject.GetComponentsInChildren<Joint>();
        _awBehaviours = gameObject.GetComponentsInChildren<AWBehaviour>();

        Transform[] AWtransforms = gameObject.GetComponentsInChildren<Transform>();
        foreach (Transform t in AWtransforms)
        {
            if (t != this.transform && t.tag != "AWThing")
            {
                t.localPosition = Vector3.zero;
            }
        }
    }

    #region Behaviour Handling
    /// <summary>
    /// Adds behaviour once has finished being made.
    /// </summary>
    /// <param name="AWbehaviourName">String name of AWBehaviour script to be added.</param>
    public void AddBehaviour(System.Type _behaviourType)
    {
        Debug.Log("AWObj.AddBehaviour(str): " + gameObject.name);
        if (behaviourObject == null) { BuildBehaviourContainer(); }
        AWBehaviour behaviour = (AWBehaviour)behaviourObject.AddComponent(_behaviourType);
        behaviour.enabled = false;
        behaviour.ParentAWObj = this;
        addDefaultBehaviour = false;


        if (Application.isEditor && !Application.isPlaying)
        {
#if UNITY_EDITOR
            EditorCoroutineUtility.StartCoroutineOwnerless(ActivateBehaviourWhenMade(behaviour));
#endif
        }
        else
        {
            StartCoroutine(ActivateBehaviourWhenMade(behaviour));
        }

    }


    public T AddBehaviour<T>(bool ClearExistingBehaviours = true) where T : AWBehaviour, new()
    {
        List<AWBehaviour> existingBehaviours = new List<AWBehaviour>();
        T behaviour = null;


        if (behaviourObject == null)
        {
            BuildBehaviourContainer();
            behaviour = behaviourObject.AddComponent<T>();
            behaviour.ParentAWObj = this;
            behaviour.enabled = false;
            addDefaultBehaviour = false;

            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                EditorCoroutineUtility.StartCoroutineOwnerless(ActivateBehaviourWhenMade<T>(behaviour));
#endif
            }
            else
            {
                StartCoroutine(ActivateBehaviourWhenMade<T>(behaviour));
            }
        }
        else
        {
            if (ClearExistingBehaviours)
            {
                existingBehaviours = GetExistingBehaviours();
            }

            if (gameObject.TryGetComponent<T>(out T temp))
            {
                behaviour = temp;
                existingBehaviours.Remove(temp);
            }
            else
            {
                behaviour = behaviourObject.AddComponent<T>();
                behaviour.ParentAWObj = this;
                behaviour.enabled = false;
                addDefaultBehaviour = false;

                if (Application.isEditor && !Application.isPlaying)
                {
#if UNITY_EDITOR
                    EditorCoroutineUtility.StartCoroutineOwnerless(ActivateBehaviourWhenMade<T>(behaviour));
#endif
                }
                else
                {
                    StartCoroutine(ActivateBehaviourWhenMade<T>(behaviour));
                }
            }
            if (ClearExistingBehaviours)
            {
                DisableExistingBehaviours(existingBehaviours);
            }
        }
        return behaviour;
    }

    /// <summary>
    /// Adds default behaviour once OBJ spawned.
    /// </summary>
    private IEnumerator AddDefaultBehaviour()
    {
        //Debug.Log("AWObj.AddDefaultBehaviour: " + gameObject.name);
        if (addDefaultBehaviour)
        {
            if (behaviourObject == null)
            {
                BuildBehaviourContainer();
            }
            while (!_awObjMade)
            {
                if (Application.isEditor && !Application.isPlaying)
                {
#if UNITY_EDITOR
                    yield return new EditorWaitForSeconds(1);
#endif
                }
                else
                {
                    yield return new WaitForSeconds(1);
                }
            }



            bool isGroupMember = GroupMap.ThingHasAGroup(_behaviour, _category);
            // don't add defualt behvaiours to group members for now as these work differently - GM
            if (_hasBehaviour && !isGroupMember)
            {
                // Debug.Log($"_categoryBehaviourName = {_categoryBehaviourName}");
                // _behaviour alone cannot always determine default behaviour, so _categoryBehaviourName checks first - GM
                if (_categoryBehaviourName == "vehicle_uniform__drive" || _categoryBehaviourName == "vehicle_uniform")
                {
                    AddBehaviour<RandomMovement>();
                }
                else
                {
                    if (_behaviour == "drive") AddBehaviour<VehicleDriveMovement>();
                    else if (_category == "hopper" || _behaviour == "hop") AddBehaviour<HoppingMovement>();
                    else if (_behaviour == "fly") AddBehaviour<VehicleFlyerRandomMovement>();
                    else AddBehaviour<RandomMovement>();
                }
            }
        }
    }

    /// <summary>
    /// Adds behaviour to AWObj without removing other behaviours.
    /// </summary>
    /// <param name="behaviourName">string name of behaviour script</param>
    /// <returns></returns>
    private IEnumerator AddBehaviourIfMade(string behaviourName)
    {
        addDefaultBehaviour = false;
        while (!_awObjMade)
        {
            yield return new WaitForSeconds(1);
        }
        if (behaviourController != null)
        {
            behaviourController.AddAWBehaviour(behaviourName);
        }
        else
        {
            Debug.LogError("No controller added, cannot add behaviour " + behaviourName + " to " + gameObject.name);
        }
    }
    private IEnumerator AddBehaviourIfMade<T>(T _behaviourRef) where T : AWBehaviour
    {
        addDefaultBehaviour = false;
        while (!_awObjMade)
        {

            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                yield return new EditorWaitForSeconds(1);
#endif
            }
            else
            {
                yield return new WaitForSeconds(1);
            }
        }

        if (behaviourController != null)
        {
            _behaviourRef = (T)behaviourController.AddAWBehaviour<T>();

        }
        else
        {
            Debug.LogError("No controller added, cannot add behaviour" + typeof(T).ToString() + " to " + gameObject.name); ;
        }
        yield return _behaviourRef;
    }

    private IEnumerator ActivateBehaviourWhenMade<T>(T _behaviourRef) where T : AWBehaviour
    {
        // Debug.Log("AWObj.ActivateBehaviourWhenMade: " + gameObject.name);
        while (!_awObjMade)
        {
            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                yield return new EditorWaitForSeconds(1);
#endif
            }
            else
            {
                yield return new WaitForSeconds(1);
            }
        }
        // Debug.Log("_awObjMade: " + _awObjMade + ": " + this.gameObject.name);
        // Debug.Log("Setting object " + gameObject.name + " ready to go.");

        _behaviourRef.enabled = true;
        _behaviourRef.ReadyToGo = true;
        _behaviourRef.InitializeBehaviour();



    }


    private IEnumerator ActivateBehaviourWhenMade(AWBehaviour _behaviourRef)
    {
        while (!_awObjMade)
        {
            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                yield return new EditorWaitForSeconds(1);
#endif
            }
            else
            {
                yield return new WaitForSeconds(1);
            }
        }
        Debug.Log("_awObjMade: " + _awObjMade + ": " + this.gameObject.name);
        Debug.Log("Setting object " + gameObject.name + " ready to go.");
        _behaviourRef.ReadyToGo = true;
        _behaviourRef.enabled = true;
        _behaviourRef.InitializeBehaviour();
        _behaviourRef.ParentAWObj = this;
    }
    /// <summary>
    /// Removes other behaviours and adds input behaviour only.
    /// </summary>
    /// <param name="behaviourName">string name of behaviour script.</param>
    /// <returns></returns>
    private IEnumerator SetBehaviourIfMade(string behaviourName)
    {
        while (!_awObjMade)
        {
            yield return new WaitForSeconds(1);
        }
        behaviourController.RemoveActiveBehaviours();
        behaviourController.AddAWBehaviour(behaviourName);
    }
    #endregion
    public void RemoveExistingBehaviours()
    {
        if (behaviourObject != null)
        {
            foreach (var behaviour in behaviourObject.GetComponents<AWBehaviour>())
            {
                behaviour.RemoveAWAnimator();
                AWSafeDestroy.SafeDestroy(behaviour, false);
            }
        }
    }

    private List<AWBehaviour> GetExistingBehaviours()
    {
        List<AWBehaviour> behaviourList = new List<AWBehaviour>();
        if (behaviourObject != null)
        {
            foreach (var behaviour in behaviourObject.GetComponents<AWBehaviour>())
            {
                behaviourList.Add(behaviour);
            }
        }
        return behaviourList;
    }
    private void DisableExistingBehaviours(List<AWBehaviour> behavioursToDisable)
    {
        if (behaviourObject != null)
        {
            foreach (var behaviour in behavioursToDisable)
            {
                behaviour.enabled = false;
            }
        }
    }
    private void BuildBehaviourContainer()
    {
        behaviourObject = new GameObject();
        behaviourObject.name = "Behaviours";
        behaviourObject.transform.parent = transform;
    }
    #endregion

}