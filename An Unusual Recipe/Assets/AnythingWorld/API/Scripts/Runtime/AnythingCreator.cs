﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using System;
using UnityEngine.Networking;
#if UNITY_EDITOR
using Unity.EditorCoroutines.Editor;
#endif

[ExecuteAlways]
public class AnythingCreator : AnythingBase
{
    #region Made Things Dict
    public Dictionary<string, List<GameObject>> madeThings = null;
    public Dictionary<string, List<GameObject>> MadeThings
    {
        get
        {
            return madeThings;
        }
    }
    #endregion

    #region Scene Ledger
    [SerializeField]
    private SceneLedger sceneLedger = null;
    public SceneLedger SceneLedger
    {
        get
        {
            if (sceneLedger == null)
            {
                sceneLedger = new SceneLedger();
            }
            return sceneLedger;
        }
    }
    #endregion

    public delegate void SearchCompleteDelegate(SearchResult[] results);
    SearchCompleteDelegate searchDelegate;


    #region Object Making Variables
    [SerializeField]
    public static List<GameObject> objectsInScene;

    public Queue anythingQueue;
#if UNITY_EDITOR
    private EditorCoroutine makeObjectEditorRoutine;
#endif
    private Coroutine makeObjectRoutine;
    private string behaviour;
    private GameObject groupControllerObject;
    private Flock groupController;
    #endregion

    private bool makeObjectRoutineRunning = false;
    public bool MakeObjectRoutineRunning
    {
        get
        {
            return makeObjectRoutineRunning;
        }
    }

    #region AnythingCreator Instance
    [SerializeField]
    private static AnythingCreator instance;
    public static AnythingCreator Instance
    {
        get
        {
            if (instance == null)
            {
                CheckAWSetup();
                instance = GameObject.FindObjectOfType<AnythingCreator>();
                if (instance != null)
                {
                    instance.Init();
                    return instance;
                }
                else
                {
                    GameObject anythingCreatorGO = new GameObject();
                    anythingCreatorGO.name = "Anything Creator";
                    AnythingCreator anythingCreator = anythingCreatorGO.AddComponent<AnythingCreator>();
                    instance = anythingCreator;
                    instance.Init();
                }
            }
            return instance;
        }
    }
    #endregion

    public AnythingSettings AWSettings
    {
        get
        {
            if (AnythingSettings.Instance == null)
            {
                AnythingSettings.CreateInstance<AnythingSettings>();
            }
            return AnythingSettings.Instance;
        }

    }


    #region Grid Instance
    [SerializeField]
    private AWGrid layoutGrid = null;
    public AWGrid LayoutGrid
    {
        get
        {
            if (layoutGrid == null)
            {
                layoutGrid = new AWGrid(10, 10);
            }
            return layoutGrid;
        }
    }
    #endregion

    
    public void Init()
    {
        madeThings = new Dictionary<string, List<GameObject>>();
        anythingQueue = new Queue();
    }


    #region Adjust and Clones
    public void AdjustGameObjects(string objName, int quantity)
    {
        ThingGroup thingGroup = SearchSceneLedger(objName);

        if (thingGroup != null)
        {
            thingGroup.Adjust(quantity);
        }


    }
    public void AdjustGameObjects(ThingGroup thingGroup, int quantity)
    {
        if (thingGroup != null)
        {
            thingGroup.Adjust(quantity);
        }
    }

    #endregion


    #region MakeObject
    public List<AWObj> MakeManyObjects(string objName, int quantity, bool hasBehaviour = true)
    {
        if (quantity < 0) return null;

        List<AWObj> madeObjects = new List<AWObj>();



        for (int i = 0; i < quantity; i++)
        {
            GameObject goRef = new GameObject();
            AWObj awRef = goRef.AddComponent<AWObj>();

            AddToQueue(objName.ToLower(), true, hasBehaviour, true, Vector3.zero, Quaternion.identity, Vector3.one, null, goRef);
            madeObjects.Add(awRef);
        }

        return madeObjects;
    }


    public AWObj MakeObject(string objName, bool hasBehaviour = true)
    {

        GameObject goRef = new GameObject();
        AWObj awRef = goRef.AddComponent<AWObj>();

        AddToQueue(objName.ToLower(), true, hasBehaviour, true, Vector3.zero, Quaternion.identity, Vector3.one, null, goRef);
        return awRef;
    }
    public AWObj MakeObject(string objName, Transform transform, Transform parentTransform, bool hasBehaviour = true)
    {
        GameObject goRef = new GameObject();
        AWObj awRef = goRef.AddComponent<AWObj>();
        AddToQueue(objName.ToLower(), false, hasBehaviour, true, transform.position, transform.rotation, transform.localScale, parentTransform, goRef);
        return awRef;
    }
    public AWObj MakeObject(string objName, Vector3 objectPos, bool hasBehaviour = true)
    {
        GameObject goRef = new GameObject();
        AWObj awRef = goRef.AddComponent<AWObj>();
        AddToQueue(objName.ToLower(), false, hasBehaviour, true, objectPos, Quaternion.identity, Vector3.one, null, goRef);
        return awRef;
    }
    public AWObj MakeObject(string objName, Vector3 objectPos, Quaternion objectRot, bool hasBehaviour = true)
    {
        GameObject goRef = new GameObject();
        AWObj awRef = goRef.AddComponent<AWObj>();
        AddToQueue(objName.ToLower(), false, hasBehaviour, true, objectPos, objectRot, Vector3.one, null, goRef);
        return awRef;
    }
    public AWObj MakeObject(string objName, Vector3 objectPos, Quaternion objectRot, Vector3 objectScale, bool hasBehaviour = true)
    {
        GameObject goRef = new GameObject();
        AWObj awRef = goRef.AddComponent<AWObj>();
        AddToQueue(objName.ToLower(), false, hasBehaviour, true, objectPos, objectRot, objectScale, null, goRef);
        return awRef;
    }
    public AWObj MakeObject(string objName, Vector3 objectPos, Quaternion objectRot, Vector3 objectScale, Transform parentTrans, bool hasBehaviour = true)
    {
        GameObject goRef = new GameObject();
        AWObj awRef = goRef.AddComponent<AWObj>();
        AddToQueue(objName.ToLower(), false, hasBehaviour, true, objectPos, objectRot, objectScale, parentTrans, goRef);
        return awRef;
    }
    public AWObj MakeObject(string objName, Vector3 objectPos, Quaternion objectRot, Vector3 objectScale, Transform parentTrans, bool hasCollider, bool hasBehaviour = true)
    {
        GameObject goRef = new GameObject();
        AWObj awRef = goRef.AddComponent<AWObj>();
        AddToQueue(objName.ToLower(), false, hasBehaviour, hasCollider, objectPos, objectRot, objectScale, parentTrans, goRef);
        return awRef;
    }
    #endregion

    #region Search






    public void RequestCategorySearchResults(string searchTerm, SearchCompleteDelegate delegateFunc)
    {
        StartCoroutine(CategorySearch(searchTerm, delegateFunc));
    }
    public void RequestNameSearchResults(string searchTerm, SearchCompleteDelegate delegateFunc)
    {
        StartCoroutine(NameSearch(searchTerm, delegateFunc));
    }

    [Obsolete]
    public void RequestCompleteSearchResults(string searchTerm, SearchCompleteDelegate delegateFunc)
    {
        StartCoroutine(CompleteSearch(searchTerm, delegateFunc));
    }
    public IEnumerator NameSearch(string searchTerm, SearchCompleteDelegate delegateFunc)
    {
        if (searchTerm == "")
        {
            List<SearchResult> emptyList = new List<SearchResult>();
            searchDelegate += delegateFunc;
            searchDelegate(emptyList.ToArray());
            searchDelegate -= delegateFunc;
            yield break;
        }

        searchDelegate += delegateFunc;

        #region Get Result JSON Array
        //https://anything-world-api.appspot.com/anything?key=${API-KEY}&name=${name}
        string apiPrefix = "https://anything-world-api.appspot.com/anything?key=";
        string categPrefix = "&name=";
        string apiKey = AnythingSettings.Instance.APIKey;
        string appName = AnythingSettings.Instance.AppName;
        if (String.IsNullOrEmpty(appName))
        {
            AnythingSetup.Instance.ShowLoading(false);
            Debug.LogError("Please enter an App Name in AnythingSettings!");
            yield break;
        }
        if (String.IsNullOrEmpty(apiKey))
        {
            AnythingSetup.Instance.ShowLoading(false);
            Debug.LogError("Please enter an API Key in AnythingSettings!");
            yield break;
        }


        // Trim our request string
        char[] charsToTrim = { '*', ' ', '\'', ',', '.' };
        searchTerm = searchTerm.Trim(charsToTrim);
        // Make API call string
        string APICall = apiPrefix + apiKey + categPrefix + searchTerm;
        if (AnythingSettings.Instance.showDebugMessages) Debug.Log(APICall);
        // Make web request to API
        UnityWebRequest www = UnityWebRequest.Get(APICall);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            AnythingSetup.Instance.ShowLoading(false);
            Debug.LogError(www.downloadHandler.text);
            //TODO: Advanced error handling. 
            Debug.LogError("Network Error for Anything World using API Key: " + AnythingSettings.Instance.APIKey);
            yield break;
        }
        //Convert result to JSON
        string result = www.downloadHandler.text;
        result = FixJson(result);
        if (AnythingSettings.Instance.showDebugMessages) Debug.LogWarning("Result:" + result);
        string text = Regex.Replace(www.downloadHandler.text, @"[[\]]", "");
        //Turn JSON into AWThing data format.
        AWThing[] resultsArray = JSONHelper.FromJson<AWThing>(result);
        #endregion
        //Request thumbnails to textures
        List<SearchResult> searchResultList = new List<SearchResult>();
        for (int i = 0; i < resultsArray.Length; i++)
        {
            List<Texture2D> thumbnailTextureList = new List<Texture2D>();
            Texture2D tempTex = null;
            string thumbnailURL = null;
            if (resultsArray[i].source == "AW-API")
            {
                thumbnailURL = resultsArray[i].model.other.reference;
            }
            else
            {
                thumbnailURL = resultsArray[i].reference;
            }

            if (thumbnailURL != null)
            {
                using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(thumbnailURL))
                {
                    yield return uwr.SendWebRequest();

                    if (uwr.isNetworkError || uwr.isHttpError)
                    {
                        Debug.Log(uwr.error);
                        Debug.Log("URL" + thumbnailURL);
                    }
                    else
                    {
                        // Get downloaded asset bundle
                        tempTex = DownloadHandlerTexture.GetContent(uwr);
                    }
                }
            }
            else
            {
                //Debug.LogWarning("No thumbnail detected for " + resultsArray[i].name);
            }

            if (tempTex == null)
            {
                tempTex = Resources.Load("Textures/Editor/globe_arms") as Texture2D;
            }




            SearchResult temp = new SearchResult();
            temp.name = resultsArray[i].name;
            temp.Thumbnail = tempTex;
            searchResultList.Add(temp);
        }

        searchDelegate(searchResultList.ToArray());
        searchDelegate -= delegateFunc;
    }

    /// <summary>
    /// IEnumerator <c>CategorySearch</c> requests a list of search results and generates a list of <see cref="SearchResult"/> objects.
    /// </summary>
    /// <param name="searchTerm"></param>
    /// <param name="delegateFunc">Function to be called once search results have loaded.</param>
    /// <returns></returns>
    public IEnumerator CategorySearch(string searchTerm, SearchCompleteDelegate delegateFunc)
    {
        if (searchTerm == "")
        {
            List<SearchResult> emptyList = new List<SearchResult>();
            searchDelegate += delegateFunc;
            searchDelegate(emptyList.ToArray());
            searchDelegate -= delegateFunc;
            yield break;
        }

        searchDelegate += delegateFunc;

        #region Get Result JSON Array
        string apiPrefix = "https://anything-world-api.appspot.com/anything?key=";
        string categPrefix = "&creature=";
        string apiKey = AnythingSettings.Instance.APIKey;
        string appName = AnythingSettings.Instance.AppName;
        if (String.IsNullOrEmpty(appName))
        {
            AnythingSetup.Instance.ShowLoading(false);
            Debug.LogError("Please enter an App Name in AnythingSettings!");
            //appIDInvalid = true;
            yield break;
        }
        if (String.IsNullOrEmpty(apiKey))
        {
            AnythingSetup.Instance.ShowLoading(false);
            Debug.LogError("Please enter an API Key in AnythingSettings!");
            yield break;
        }

        // Trim our request string
        char[] charsToTrim = { '*', ' ', '\'', ',', '.' };
        searchTerm = searchTerm.Trim(charsToTrim);
        // lowercase our request string
        searchTerm = searchTerm.ToLower();
        // Make API call string
        string APICall = apiPrefix + apiKey + categPrefix + searchTerm;
        if (AnythingSettings.Instance.showDebugMessages) Debug.Log(APICall);

        // Make web request to API
        UnityWebRequest www = UnityWebRequest.Get(APICall);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            AnythingSetup.Instance.ShowLoading(false);
            Debug.LogError(www.downloadHandler.text);
            //TODO: Advanced error handling. 
            Debug.LogError("Network Error for Anything World using API Key: " + AnythingSettings.Instance.APIKey);
            yield break;
        }

        //Convert result to JSON
        string result = www.downloadHandler.text;
        result = FixJson(result);
        if (AnythingSettings.Instance.showDebugMessages) Debug.LogWarning("Result:" + result);
        string text = Regex.Replace(www.downloadHandler.text, @"[[\]]", "");
        List<SearchResult> searchResultList = new List<SearchResult>();
        if (text.Contains("no model found"))
        {
            Debug.LogWarning("No model found");
        }
        else
        {
            AWThing[] resultsArray = JSONHelper.FromJson<AWThing>(result);
            #endregion
            //Request thumbnails to textures
            searchResultList = new List<SearchResult>();
            for (int i = 0; i < resultsArray.Length; i++)
            {
                List<Texture2D> thumbnailTextureList = new List<Texture2D>();
                Texture2D tempTex = null;
                string thumbnailURL = null;
                if (resultsArray[i].source == "AW-API")
                {
                    thumbnailURL = resultsArray[i].model.other.reference;
                }
                else
                {
                    thumbnailURL = resultsArray[i].reference;
                }

                if (thumbnailURL != null)
                {
                    using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(thumbnailURL))
                    {
                        yield return uwr.SendWebRequest();

                        if (uwr.isNetworkError || uwr.isHttpError)
                        {
                            Debug.Log(uwr.error);
                            Debug.Log("URL" + thumbnailURL);
                        }
                        else
                        {
                            // Get downloaded asset bundle
                            tempTex = DownloadHandlerTexture.GetContent(uwr);
                        }
                    }
                }
                else
                {
                    //Debug.LogWarning("No thumbnail detected for " + resultsArray[i].name);
                }

                if (tempTex == null)
                {
                    tempTex = Resources.Load("Textures/Editor/globe_arms") as Texture2D;
                }




                SearchResult temp = new SearchResult();
                temp.name = resultsArray[i].name;
                temp.Thumbnail = tempTex;
                searchResultList.Add(temp);
            }
        }
        //Turn JSON into AWThing data format.
        searchDelegate(searchResultList.ToArray());
        searchDelegate -= delegateFunc;

    }
    #endregion

    [Obsolete]
    public IEnumerator CompleteSearch(string searchTerm, SearchCompleteDelegate delegateFunc)
    {
        if (searchTerm == "")
        {
            List<SearchResult> emptyList = new List<SearchResult>();
            searchDelegate += delegateFunc;
            searchDelegate(emptyList.ToArray());
            searchDelegate -= delegateFunc;
            yield break;
        }
        else
        {
            char[] charsToTrim = { '*', ' ', '\'', ',', '.' };
            searchTerm = searchTerm.Trim(charsToTrim);
        }
        searchDelegate += delegateFunc;
        string apiPrefix = "https://anything-world-api.appspot.com/anything?key=";
        string apiKey = AnythingSettings.Instance.APIKey;
        string appName = AnythingSettings.Instance.AppName;
        if (String.IsNullOrEmpty(appName))
        {
            AnythingSetup.Instance.ShowLoading(false);
            Debug.LogError("Please enter an App Name in AnythingSettings!");
            //appIDInvalid = true;
            yield break;
        }
        if (String.IsNullOrEmpty(apiKey))
        {
            AnythingSetup.Instance.ShowLoading(false);
            Debug.LogError("Please enter an API Key in AnythingSettings!");
            yield break;
        }


        // lowercase all requests
        searchTerm = searchTerm.ToLower();

        string categPrefix = "&name=";
        // Make API call string
        string APICall = apiPrefix + apiKey + categPrefix + searchTerm;
        if (AnythingSettings.Instance.showDebugMessages) Debug.Log(APICall);
        // Make web request to API
        UnityWebRequest www = UnityWebRequest.Get(APICall);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            AnythingSetup.Instance.ShowLoading(false);
            Debug.LogError(www.downloadHandler.text);
            //TODO: Advanced error handling. 
            //Debug.LogError("Network Error for Anything World using API Key: " + AnythingSettings.Instance.APIKey);
            yield break;
        }
        //Convert result to JSON
        string result = www.downloadHandler.text;
        string nameResult = FixJson(result);
        if (AnythingSettings.Instance.showDebugMessages) Debug.LogWarning("Result:" + result);
        //string text = Regex.Replace(www.downloadHandler.text, @"[[\]]", "");
        //Turn JSON into AWThing data format.
        AWThing[] nameArray = JSONHelper.FromJson<AWThing>(nameResult);
        if (nameArray.Length > 0)
        {

        }


        categPrefix = "&creature=";
        // Make API call string
        APICall = apiPrefix + apiKey + categPrefix + searchTerm;
        if (AnythingSettings.Instance.showDebugMessages) Debug.Log(APICall);
        // Make web request to API
        www = UnityWebRequest.Get(APICall);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            AnythingSetup.Instance.ShowLoading(false);
            Debug.LogError(www.downloadHandler.text);
            //TODO: Advanced error handling. 
            Debug.LogError("Network Error for Anything World using API Key: " + AnythingSettings.Instance.APIKey);
            yield break;
        }
        //Convert result to JSON
        result = www.downloadHandler.text;
        string creatureResult = FixJson(result);
        if (AnythingSettings.Instance.showDebugMessages) Debug.LogWarning("Result:" + result);
        //text = Regex.Replace(www.downloadHandler.text, @"[[\]]", "");
        //Turn JSON into AWThing data format.




        AWThing[] creatureArray = JSONHelper.FromJson<AWThing>(creatureResult);



        AWThing[] resultsArray = new AWThing[nameArray.Length + creatureArray.Length];
        nameArray.CopyTo(resultsArray, 0);
        creatureArray.CopyTo(resultsArray, nameArray.Length);

        //Request thumbnails to textures
        List<SearchResult> searchResultList = new List<SearchResult>();
        for (int i = 0; i < resultsArray.Length; i++)
        {
            List<Texture2D> thumbnailTextureList = new List<Texture2D>();
            Texture2D tempTex = null;
            string thumbnailURL = null;
            if (resultsArray[i].source == "AW-API")
            {
                thumbnailURL = resultsArray[i].model.other.reference;
            }
            else
            {
                thumbnailURL = resultsArray[i].reference;
            }

            if (thumbnailURL != null)
            {
                using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(thumbnailURL))
                {
                    yield return uwr.SendWebRequest();

                    if (uwr.isNetworkError || uwr.isHttpError)
                    {
                        Debug.Log(uwr.error);
                        Debug.Log("URL" + thumbnailURL);
                    }
                    else
                    {
                        // Get downloaded asset bundle
                        tempTex = DownloadHandlerTexture.GetContent(uwr);
                    }
                }
            }
            else
            {
                //Debug.LogWarning("No thumbnail detected for " + resultsArray[i].name);
            }

            if (tempTex == null)
            {
                tempTex = Resources.Load("Textures/Editor/globe_arms") as Texture2D;
            }




            SearchResult temp = new SearchResult();
            temp.name = resultsArray[i].name;
            temp.Thumbnail = tempTex;
            searchResultList.Add(temp);
        }

        searchDelegate(searchResultList.ToArray());
        searchDelegate -= delegateFunc;
    }

    private string FixJson(string value)
    {
        value = "{\"Items\":" + value + "}";
        return value;
    }




    #region Creation

    /// <summary>
    /// Creates <see cref="AnythingQueueObject"/> from parameters and adds it to the <c>anythingQueue</c>.
    /// </summary>
    private void AddToQueue(string objName, bool autoLayout, bool hasBehaviour, bool hasCollider, Vector3 objectPos, Quaternion objectRot, Vector3 objectScale, Transform parentTrans, GameObject objRef)
    {
        // TOOD: find out why this can be called before Init() - G
        if (anythingQueue == null)
        {
            Init();
        }

        if (AnythingSettings.Instance.showDebugMessages) Debug.Log("AnythingCreator.AddToQueue: " + objName);
        AnythingQueueObject aQObject = new AnythingQueueObject(objName, autoLayout, hasBehaviour, hasCollider, objectPos, objectRot, objectScale, parentTrans, objRef);
        anythingQueue.Enqueue(aQObject);

        if (makeObjectRoutineRunning == false)
        {
            //If MakeObjectProcess is not currently running make it
            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR

                if (makeObjectEditorRoutine != null)
                    EditorCoroutineUtility.StopCoroutine(makeObjectEditorRoutine);
                makeObjectEditorRoutine = EditorCoroutineUtility.StartCoroutine(MakeObjectProcess(), this);
#endif
            }
            else
            {
                if (makeObjectRoutine != null)
                    StopCoroutine(makeObjectRoutine);

                makeObjectRoutine = StartCoroutine(MakeObjectProcess());
            }

        }
    }

    /// <summary>
    /// Loop that iterates through object queue and performs the make process on each Queue object.
    /// </summary>
    private IEnumerator MakeObjectProcess()
    {
        makeObjectRoutineRunning = true;
        AnythingSetup.Instance.ShowLoading(true);
        while (anythingQueue.Count > 0)
        {
            AnythingQueueObject aObject = anythingQueue.Peek() as AnythingQueueObject;
            string nameLower = aObject.objName.ToLower();
            Vector3 thingPos = aObject.objectPos;
            Vector3 thingScale = aObject.objectScale;
            Quaternion thingRot = aObject.objectRot;
            Transform parentTrans = aObject.parentTransform;
            bool hasBehaviour = aObject.hasBehaviour;
            bool hasCollider = aObject.hasCollider;
            GameObject thingGO = null;
            AWObj thing = null;




            if (!madeThings.ContainsKey(aObject.objName))
            {
                madeThings[aObject.objName] = new List<GameObject>();
                // If objRef provided assign to thingGO else create new GO and AWObj ref.
                if (aObject.objRef != null)
                {
                    thingGO = aObject.objRef;
                    thing = thingGO.GetComponent<AWObj>();
                }
                else
                {
                    thingGO = new GameObject();
                    thing = thingGO.AddComponent<AWObj>();
                }
                thingGO.name = aObject.objName;
                thing.MakeAWObj(aObject.objName, hasBehaviour, hasCollider);
                while (!thing.AWObjMade)
                {
                    if (Application.isEditor && !Application.isPlaying)
                    {
#if UNITY_EDITOR
                        yield return new EditorWaitForSeconds(0.01f);
#endif
                    }
                    else
                    {
                        yield return new WaitForEndOfFrame();
                    }
                }
                _category = CategoryMap.GetCategory(nameLower);
                behaviour = BehaviourMap.GetBehaviour(nameLower);
                if (GroupMap.ThingHasAGroup(behaviour, _category))
                {
                    //flockCount = 1;
                    CreateAWGroup(thing, aObject.objName);
                }
                else
                {
                    if (aObject.autoLayout)
                    {
                        thingPos = GetGridPosition(thing, thing.ObjectScale);
                    }

                    if (parentTrans != null)
                        thingGO.transform.parent = parentTrans;

                    thingGO.transform.position = thingPos;
                    thingGO.transform.localRotation = thingRot;
                    thingGO.transform.localScale = Vector3.Scale(thingGO.transform.localScale, thingScale);
                    madeThings[aObject.objName].Add(thingGO);

                }

            }
            else
            {

                if (aObject.objRef != null)
                {

                    AWSafeDestroy.SafeDestroy(aObject.objRef);
                }

                AWObj AWObjToClone = madeThings[aObject.objName][0].GetComponent<AWObj>(); // TODO: careful, 0 index reference

                if (AWObjToClone != null)
                {
                    if (aObject.autoLayout)
                    {

                        thingPos = GetGridPosition(AWObjToClone, AWObjToClone.ObjectScale);
                    }
                }
                thingGO = Instantiate(madeThings[aObject.objName][0], thingPos, thingRot);
                thingGO.name = madeThings[aObject.objName][0].name + " " + _totalObjects;

                AWObj clonedAWObj = thingGO.GetComponent<AWObj>();
                if (clonedAWObj != null)
                {
                    thing = clonedAWObj;
                    clonedAWObj.AWObjMade = true;
                }
                if (parentTrans != null)
                    thingGO.transform.parent = parentTrans;



                thingGO.transform.localScale = Vector3.Scale(thingGO.transform.localScale, thingScale);

                FlockMember currFlockMember = thingGO.GetComponentInChildren<FlockMember>();

                if (currFlockMember != null)
                {
                    AddCloneToGroup(currFlockMember.gameObject);
                    madeThings[aObject.objName].Add(currFlockMember.gameObject);
                }
                else
                {
                    madeThings[aObject.objName].Add(thingGO);
                }

                yield return new WaitForSeconds(0.1f);
            }

            _totalObjects++;
            anythingQueue.Dequeue();

            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                yield return new EditorWaitForSeconds(0.01f);
#endif
            }
            else
            {
                yield return new WaitForEndOfFrame();
            }



            if (thingGO != null && thing != null)
            {
                ThingGroup group = SearchSceneLedger(nameLower);
                group.awInstances.Add(thing);
                group.objInstances.Add(thingGO);
            }
            else
            {
                Debug.Log("Breakpoint check");
            }

        }
        makeObjectRoutineRunning = false;
        AnythingSetup.Instance.ShowLoading(false);
    }



    private void CheckMadeObjects(string key)
    {
        if (MadeThings.ContainsKey(key))
        {
            if (MadeThings.TryGetValue(key, out List<GameObject> values))
            {
                foreach (var value in values.ToArray())
                {
                    if (value == null)
                    {
                        madeThings[key].Remove(value);
                    }
                }

                if (values.Count == 0)
                {
                    MadeThings.Remove(key);
                }
            }
        }
    }

    /// <summary>
    /// Search scene ledger for object of matching name.
    /// </summary>
    /// <param name="name">String to find matching object name to.</param>
    /// <returns>Matching <see cref="ThingGroup"/> object holding instances of the specific game object.</returns>
    private ThingGroup SearchSceneLedger(string name)
    {
        foreach (ThingGroup group in SceneLedger.ThingGroups.ToArray())
        {
            if (group.objectName == name)
            {
                return group;
            }
        }

        return SceneLedger.NewThingGroup(name);
    }

    /// <summary>
    /// Create flock of AW objects.
    /// </summary>
    private void CreateAWGroup(AWObj thing, string objName)
    {
        GameObject groupObj = GroupMap.GroupPrefab(behaviour);
        groupControllerObject = Instantiate(groupObj) as GameObject;

        groupController = groupControllerObject.GetComponent<Flock>();

        // TODO: polymorphic, proper
        FlockMember currFlockMember = thing.GetComponentInChildren<FlockMember>();
        groupController.flockPrefabs = new GameObject[1];
        groupController.flockPrefabs[0] = currFlockMember.gameObject; // TODO: allow for more fish  
        AddCloneToGroup(currFlockMember.gameObject);

        // we have already added our original object to group, so can safely destroy AWObj gameobject now
        // AWSafeDestroy.SafeDestroy(thing.gameObject);

        madeThings[objName].Add(currFlockMember.gameObject);

    }

    private void AddCloneToGroup(GameObject groupClone)
    {

        FlockMember fishChild = groupClone.GetComponentInChildren<FlockMember>();

        // TODO: careful, this will pick up any Flock
        if (groupController == null)
            groupController = GameObject.FindObjectOfType<Flock>();

        groupController.AddMember(fishChild);
    }

    public GameObject QueryMadeObjects(string query)
    {
        string lowerQuery = query.ToLower();
        if (madeThings == null)
        {
            if (AnythingSettings.Instance.showDebugMessages) Debug.Log("No made things");
            return null;
        }

        if (madeThings.ContainsKey(lowerQuery))
        {
            if (AnythingSettings.Instance.showDebugMessages) Debug.Log("Checking made items for ");
            List<GameObject> matchingObjects = madeThings[lowerQuery];
            if (matchingObjects.Count > 0)
            {
                return matchingObjects[0];
            }
        }
        return null;
    }

    #endregion

    #region Grid Layout
    protected Vector3 GetGridPosition(AWObj gridObj, float gridScale)
    {
        float yOffset = gridObj.BoundsYOffset;
        Vector3 gridPos = LayoutGrid.GetNextAvailablePos(gridScale);

        if (LayoutGrid.NoSpaceLeft)
        {
            Debug.LogWarning("No space left on the grid, so expanding it!");
            LayoutGrid.ExpandGrid(10, 0);
            gridPos = LayoutGrid.GetNextAvailablePos(gridScale);
        }
        gridPos.y = yOffset;

        return gridPos;
    }
    public void ResetAutoLayout(bool resetPositions = true)
    {
        layoutGrid = null;
        //It through each group in the scene ledger and place each item in group on the grid.
        foreach (ThingGroup group in SceneLedger.ThingGroups.ToArray())
        {
            foreach (var awCreature in group.awInstances)
            {
                if (awCreature != null)
                {
                    if (resetPositions)
                    {
                        awCreature.awThing.transform.position = GetGridPosition(awCreature, awCreature.ObjectScale);
                    }
                    else
                    {
                        GetGridPosition(awCreature, awCreature.ObjectScale);
                    }

                }
            }
        }
    }
    #endregion
    public string GetDisplayName(string _name)
    {
        if (_name != null)
        {
            string disp = Regex.Replace(_name, @"\d", "");
            char[] dispArr = disp.ToCharArray();
            dispArr[0] = char.ToUpper(dispArr[0]);
            return new string(dispArr);
        }
        else
        {
            return null;
        }
    }
}

/// <summary>
/// Queue object that allows object data from API to be passed into the anythingQueue in <c>MakeObjectProcess</c>.
/// </summary>
public class AnythingQueueObject
{
    #region Fields
    public string objName;
    public bool autoLayout = true;
    public bool hasBehaviour = true;
    public bool hasCollider = true;
    public Vector3 objectPos = Vector3.zero;
    public Quaternion objectRot = Quaternion.identity;
    public Vector3 objectScale = Vector3.one;
    public Transform parentTransform;
    public GameObject objRef = null;
    #endregion

    #region Public Methods
    public AnythingQueueObject(string oName, bool aLayout, bool hBehaviour, bool hCollider, Vector3 oPos, Quaternion oRot, Vector3 oScale, Transform pTrans)
    {
        objName = oName;
        autoLayout = aLayout;
        hasBehaviour = hBehaviour;
        hasCollider = hCollider;
        objectPos = oPos;
        objectRot = oRot;
        objectScale = oScale;
        parentTransform = pTrans;
    }

    public AnythingQueueObject(string oName, bool aLayout, bool hBehaviour, bool hCollider, Vector3 oPos, Quaternion oRot, Vector3 oScale, Transform pTrans, GameObject _objRef)
    {
        objName = oName;
        autoLayout = aLayout;
        hasBehaviour = hBehaviour;
        hasCollider = hCollider;
        objectPos = oPos;
        objectRot = oRot;
        objectScale = oScale;
        parentTransform = pTrans;
        objRef = _objRef;
    }
    #endregion
}

/// <summary>
/// Class holding information about search results for the Object Creator panel.
/// Holds object names and thumbnails, and handles serialization of of thumbnail images across runtime/editor.
/// </summary>
[Serializable]
public class SearchResult
{
    public string DisplayName
    {
        get
        {
            return GetDisplayName();
        }
    }

    [SerializeField]
    public string name;
    [SerializeField]
    public Texture2D thumbnail;
    public Texture2D Thumbnail
    {
        get
        {
            if (thumbnail != null)
            {
                //Debug.Log("Returning thumbnail");
                return thumbnail;
            }
            else
            {


                //Debug.Log("Deserializing thumbnail");
                bool chain = false;
                if (mipMapCount > 1) chain = true;

                Texture2D texCopy = new Texture2D(texWidth, texHeight, format, chain);
                texCopy.LoadRawTextureData(texStream);
                texCopy.Apply();

                if (texCopy != null)
                {
                    thumbnail = texCopy;
                    return thumbnail;
                }
                else
                {
                    return null;
                }
            }
        }

        set
        {
            //Debug.Log("Serializing texture from value");
            #region Serialize Thumbnail
            texStream = value.GetRawTextureData();
            texWidth = value.width;
            texHeight = value.height;
            format = value.format;
            mipMapCount = value.mipmapCount;
            thumbnail = value;
            #endregion

        }
    }
    [SerializeField]
    public byte[] texStream;
    int texHeight;
    int texWidth;
    TextureFormat format;
    int mipMapCount;
    private string GetDisplayName()
    {
        if (name != null)
        {
            string disp = Regex.Replace(name, @"\d", "");
            char[] dispArr = disp.ToCharArray();
            dispArr[0] = char.ToUpper(dispArr[0]);
            return new string(dispArr);
        }
        else
        {
            return null;
        }

    }

}

[Serializable]
public class ThingGroup
{
    [SerializeField]
    public string objectName;
    [SerializeField]
    public List<AWObj> awInstances;
    [SerializeField]
    public List<GameObject> objInstances;

    [SerializeField]
    private int goalInstances=-1;

#if UNITY_EDITOR
    private EditorCoroutine sliderWaitEditorRoutine;
#else
    private Coroutine sliderWaitRoutine;
#endif

    public int GoalInstances
    {
        get
        {
            if (goalInstances <0)
            {
                return objInstances.Count;
            }
            else
            {
                return goalInstances;
            }
        }
        set
        {
            if (value != goalInstances)
            {
                if (value >= 0)
                {
                    goalInstances = value;
#if UNITY_EDITOR
                    if (sliderWaitEditorRoutine != null)
                        EditorCoroutineUtility.StopCoroutine(sliderWaitEditorRoutine);
                    sliderWaitEditorRoutine = EditorCoroutineUtility.StartCoroutineOwnerless(SliderAdjust(goalInstances));
#else
                    if (sliderWaitRoutine != null)
                        StopCoroutine(sliderWaitRoutine);
                    sliderWaitRoutine = AnythingCreator.Instance.StartCoroutine(SliderAdjust(goalInstances));
#endif
                }
                else
                {
                    goalInstances = 0;
                }
            }
           

        }
    }
    [SerializeField]
    private int heldGoalInstances = 1;
    public int HeldGoalInstances
    {
        get
        {
            return heldGoalInstances;
        }
        set
        {
            heldGoalInstances = value;
        }
    }


    public ThingGroup()
    {
        awInstances = new List<AWObj>();
        objInstances = new List<GameObject>();
    }
    public ThingGroup(string groupName)
    {
        objectName = groupName;
        awInstances = new List<AWObj>();
        objInstances = new List<GameObject>();

    }

    public void ClearObjects()
    {
        foreach (var creature in objInstances)
        {
            AWSafeDestroy.SafeDestroy(creature);
        }
    }

    private IEnumerator SliderAdjust(int newCount)
    {
        while (AnythingCreator.Instance.MakeObjectRoutineRunning)
        {
#if UNITY_EDITOR
            yield return new EditorWaitForSeconds(0.05f);
#else
            yield return new WaitForEndOfFrame();
#endif
        }

        Adjust(newCount);
    }

    public void Adjust(int newCount)
    {
        if (AnythingSettings.Instance.showDebugMessages) Debug.Log("Adjust " + objectName + " to : " + newCount);

        int difference = newCount - objInstances.Count;
        if (difference == 0)
        {
            return;
        }
        else if (difference < 0)
        {
            int positiveDiff = Mathf.Abs(difference);

            for (int i = 0; i < positiveDiff; i++)
            {
                int ind = objInstances.Count - 1;
                AWSafeDestroy.SafeDestroy(objInstances[ind]);
                objInstances.RemoveAt(ind);
            }
        }
        else
        {
            for (int i = 0; i < difference; i++)
            {
                AnythingCreator.Instance.MakeObject(objectName);
                AnythingCreator.Instance.ResetAutoLayout(true);
            }
        }
    }

}

[Serializable]
public class SceneLedger
{
    [SerializeField]
    private List<ThingGroup> thingGroups = null;
    public List<ThingGroup> ThingGroups
    {
        get
        {
            if (thingGroups == null)
            {
                thingGroups = new List<ThingGroup>();
            }
            return thingGroups;
        }
    }

    public SceneLedger()
    {
        thingGroups = new List<ThingGroup>();
    }
    public ThingGroup NewThingGroup(string name)
    {
        ThingGroup newGroup = new ThingGroup(name);
        thingGroups.Add(newGroup);
        return newGroup;
    }

    public void RemoveGroup(ThingGroup group)
    {
        group.ClearObjects();
        thingGroups.Remove(group);
    }
}