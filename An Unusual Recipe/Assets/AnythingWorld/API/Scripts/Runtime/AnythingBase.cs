﻿using UnityEngine;



public class AnythingBase : MonoBehaviour
{
    #region Fields
    protected string _category;
    protected int _totalObjects;

    public static AnythingSettings Settings
    {
        get
        {
            if (AnythingSettings.Instance == null)
            {
                AnythingSettings.CreateInstance<AnythingSettings>();
            }

            return AnythingSettings.Instance;
        }
    }
    #endregion

    #region Protected Methods

    protected void ResetEverything()
    {
        AWObj[] sceneAWObjs = FindObjectsOfType<AWObj>();
        foreach (AWObj awObj in sceneAWObjs)
        {
            AWSafeDestroy.SafeDestroy(awObj.gameObject);
        }
        AWHabitat[] sceneAWHabitats = FindObjectsOfType<AWHabitat>();
        foreach (AWHabitat awHab in sceneAWHabitats)
        {
            AWSafeDestroy.SafeDestroy(awHab.gameObject);
        }
        // remove groups too
        Flock[] sceneFlocks = FindObjectsOfType<Flock>();
        foreach (Flock flockObj in sceneFlocks)
        {
            AWSafeDestroy.SafeDestroy(flockObj.gameObject);
        }
    }
    #endregion


    public delegate void OpenAPIGenWindowDelegate();
    public static OpenAPIGenWindowDelegate openAPIWindowDelegate;

    public static void CheckAWSetup()
    {
        if (!FindObjectOfType<AnythingSetup>())
        {
            GameObject setup = Instantiate(Resources.Load("AnythingSetup")) as GameObject;
            setup.name = "AnythingSetup";
        }
    }
}
