﻿using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor.Experimental.SceneManagement;
#endif

[ExecuteAlways]
[Serializable]
public class AnythingObject : MonoBehaviour
{
    #region Fields
    public string ObjectName;
    public bool CreateInEditor = true;
    public bool HasBehaviour = false;
    public bool HasCollider = false;
    public bool RandomYRotation = false;
    [SerializeField]
    public bool ObjectCreated = false;
    private AnythingCreator anythingCreator;
    private string lastObjectName;
    #endregion

    #region Unity Callbacks
    void Awake()
    {
        anythingCreator = AnythingCreator.Instance;

#if UNITY_EDITOR
        if (PrefabStageUtility.GetCurrentPrefabStage() != null)
            ObjectCreated = false;
#endif
    }

    void Start()
    {
        if (ObjectCreated)
            return;

        if (RandomYRotation)
        {
            transform.eulerAngles = new Vector3(transform.rotation.x, UnityEngine.Random.Range(0, 360), transform.rotation.z);
        }
        if (ObjectName != null)
        {
            if ((CreateInEditor && Application.isEditor && !Application.isPlaying) || (Application.isPlaying))
            {
                CreateObject();
            }
        }
    }
    #endregion

    #region Private Methods
    private void CleanUp()
    {
        AWObj[] awObjs = GetComponentsInChildren<AWObj>();
        foreach (AWObj awObj in awObjs)
        {

            AWSafeDestroy.SafeDestroy(awObj.gameObject);

        }
    }

    public void CreateObject()
    {
        CleanUp();
        anythingCreator.MakeObject(ObjectName, transform.position, transform.rotation, transform.localScale, transform, HasCollider, HasBehaviour);
        ObjectCreated = true;
        lastObjectName = ObjectName;
    }

    #endregion

}
