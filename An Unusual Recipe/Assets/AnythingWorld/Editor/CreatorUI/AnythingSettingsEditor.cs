﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;
using Unity.EditorCoroutines.Editor;
using System.Collections;

[CustomEditor(typeof(AnythingSettings))]
public class AnythingSettingsEditor : Editor
{
    public static string VERSION_NUMBER = "1.2.0.1";
    public static string LATEST_VERSION_URL = "http://anything-world-api.appspot.com/version";

    [SerializeField]
    protected Texture2D logo;
    public Color GREEN_COLOR = new Color(0.53f, 1f, 0f);

    private VersionCheck versionCheck;

    public void Awake()
    {
        logo = Resources.Load("Textures/Editor/logo_legless_white", typeof(Texture2D)) as Texture2D;
        EditorCoroutineUtility.StartCoroutine(CheckVersion(), this);
    }

    public override void OnInspectorGUI()
    {
        Color guiColor = GUI.color; // Save the current GUI color
        GUI.color = Color.clear; // This does the magic
        float width = 60;
        float height = 60;
        EditorGUI.DrawTextureTransparent(new Rect(6, 2, width, height), logo);
        GUI.color = guiColor; // Get back to previous GUI color

        EditorGUILayout.Space(60);
        DrawUILine(GREEN_COLOR);

        base.OnInspectorGUI();
        EditorGUILayout.LabelField("Version", VERSION_NUMBER);


        if (versionCheck != null)
        {
            if (versionCheck.version != VERSION_NUMBER)
            {
                if (GUILayout.Button("Click Here To Upgrade To " + versionCheck.version, GUILayout.Height(20)))
                {
                    Application.OpenURL(versionCheck.downloadLink);
                }
            }
        }

        EditorGUILayout.Space(10);
        DrawUILine(GREEN_COLOR);


        if (GUILayout.Button("Visit Our Site!", GUILayout.Height(20)))
        {
            Application.OpenURL("https://anything.world/");
        }

    }

    protected void DrawUILine(Color color, int thickness = 1, int padding = 20)
    {
        Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
        r.height = thickness;
        r.y += padding / 2;
        r.x -= 2;
        r.width += 6;
        EditorGUI.DrawRect(r, color);
    }

    private IEnumerator CheckVersion()
    {
        UnityWebRequest www = UnityWebRequest.Get(LATEST_VERSION_URL);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.LogError("Network Error for Anything World version check");
            yield break;
        }

        string result = www.downloadHandler.text;

        if (AnythingSettings.Instance.showDebugMessages) Debug.Log(result);

        versionCheck = JsonUtility.FromJson<VersionCheck>(result);
    }
}