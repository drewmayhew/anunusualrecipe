﻿using System.Collections;
using UnityEngine;

namespace MeshStitcher
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class ModelStitcher : MonoBehaviour
    {
        #region Fields
        public Shader overrideShader = null;
        public Material overrideMaterial = null;
        private AWObj _controllingAWObj;
        #endregion

        #region Unity Callbacks
        private void Start()
        {
            // TODO: careful! reliant on parent object script
            _controllingAWObj = transform.parent.GetComponent<AWObj>();
            if (_controllingAWObj == null)
            {
                // Debug.LogWarning($"No AWObj found for {gameObject.name}");
                // TODO: fix, this is in here for flock members, which have no controlling AWObj currently - GM
                Initialize();
            }
            else
            {
                //Must wait for AWOBj to be made or the mesh stitcher will not initialize correctly in runtime. 
                StartCoroutine(WaitForAWObjCompletion());
            }
        }

        private void OnValidate()
        {
        }

        private void Reset()
        {

        }
        private void OnEnable()
        {

            if (overrideShader == null && overrideMaterial == null)
            {
                // Debug.Log("Applying default shader");
                overrideShader = Shader.Find("Anything World/Simple Lit Stitched");
            }
        }
        #endregion

        #region Public Methods

        #endregion

        #region Private Methods
        private void Initialize()
        {
            var joints = GetComponentsInChildren<Joint>(true);


            //Adds mesh stitcher to main body game object and stitches it to itself, no change in position.
            GameObject bodyGO = gameObject.transform.GetChild(0).gameObject;
            // if no meshfilter on first child, search for body instead
            // TODO: standardise this, seems a little risky to me - GM
            if (bodyGO.GetComponentInChildren<MeshFilter>() == null)
            {
                bodyGO = gameObject.transform.Find("body").gameObject;
            }
            InitializeMeshStitcher(bodyGO, bodyGO.GetComponentInChildren<MeshFilter>(), bodyGO.GetComponentInChildren<MeshFilter>(), overrideShader, overrideMaterial, false);

            //Iterates through meshes with joints and adds a mesh stitcher to each.
            foreach (var joint in joints)
            {
                if (joint.gameObject.tag != "AWStitcherDisable")
                {
                    var connectedTo = joint.connectedBody;
                    if (connectedTo != null)
                    {
                        MeshFilter stitchToThisMeshFilter = connectedTo.GetComponentInChildren<MeshFilter>(true);
                        MeshFilter addMeshStitcherTo = joint.gameObject.GetComponentInChildren<MeshFilter>(true);

                        if (stitchToThisMeshFilter != null && addMeshStitcherTo != null)
                        {
                            InitializeMeshStitcher(addMeshStitcherTo.gameObject, addMeshStitcherTo, stitchToThisMeshFilter, overrideShader, overrideMaterial);
                        }
                        else
                        {
                            //Debug.LogWarning("Missing mesh filter in child components");
                        }
                    }
                    else
                    {
                        Debug.LogWarning("Joint is not connected to anything");
                    }
                }
                else
                {
                    Debug.LogWarning("Stitcher should not be applied to this joint.");
                }

            }
        }
        private static void InitializeMeshStitcher(GameObject mesh, MeshFilter source, MeshFilter target, Shader overrideShader = null, Material overrideMaterial = null, bool stitchComponent = true)
        {

            MeshStitcher newMeshStitcher = mesh.AddComponent<MeshStitcher>();
            if (overrideShader)
            {
                newMeshStitcher.overrideShader = overrideShader;
            }
            if (overrideMaterial)
            {
                newMeshStitcher.overrideMaterial = overrideMaterial;
            }
            newMeshStitcher.Initialize(source, target, stitchComponent);
        }
        private IEnumerator WaitForAWObjCompletion()
        {
            while (!_controllingAWObj.AWObjMade)
                yield return new WaitForEndOfFrame();

            Initialize();
        }
        #endregion

    }
}
