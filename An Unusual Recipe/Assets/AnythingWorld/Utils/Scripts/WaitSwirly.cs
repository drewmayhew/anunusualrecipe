﻿using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
using Unity.EditorCoroutines.Editor;
#endif
using System.Collections;

[ExecuteAlways]
public class WaitSwirly : MonoBehaviour
{

    protected float _maskAmount = 0;
    protected Image _swirlyImage;
    public bool StartOnEnable = false;
    private float _waitTime = 2f;
    private float _maskIncr;
    private Coroutine _swirlRoutine;
#if UNITY_EDITOR
    private EditorCoroutine _editorSwirlRoutine;
#endif
    private bool _isShowing;
    public float WaitTime
    {
        get
        {
            return _waitTime;
        }
    }
    private bool _shouldSwirl;

    private Image[] _postWaitImages;

    void Awake()
    {
        _swirlyImage = GetComponent<Image>();
        _maskAmount = 0;
        _isShowing = false;
    }

    public void StartSwirly(bool shouldStart)
    {
        _shouldSwirl = shouldStart;
        _maskIncr = 0.01f;
        if (shouldStart)
        {
            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                _editorSwirlRoutine = EditorCoroutineUtility.StartCoroutineOwnerless(UpdateSwirly());
#endif
            }
            else
            {
                _swirlRoutine = StartCoroutine(UpdateSwirly());
            }
        }
        _isShowing = shouldStart;
    }

    void OnDisable()
    {
        SetSwirlyMask();
    }


    private IEnumerator UpdateSwirly()
    {
        while (_isShowing)
        {
            if (_maskAmount >= 1f)
            {
                _maskIncr = -0.01f;
            }
            else if (_maskAmount <= 0f)
            {
                _maskIncr = 0.01f;
            }
            _maskAmount += _maskIncr;
            SetSwirlyMask();

            if (Application.isEditor && !Application.isPlaying)
            {
#if UNITY_EDITOR
                yield return new EditorWaitForSeconds(0.05f);
#endif
            }
            else
            {
                yield return new WaitForSeconds(0.05f);
            }
        }

    }

    protected void SetSwirlyMask()
    {
        _swirlyImage.fillAmount = _maskAmount;
        transform.Rotate(new Vector3(0, 0, 2f));
    }
}
