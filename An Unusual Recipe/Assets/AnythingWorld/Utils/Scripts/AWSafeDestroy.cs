﻿using System.Collections.Generic;
using UnityEngine;


public class AWSafeDestroy : MonoBehaviour
{

    public static void SafeDestroy(UnityEngine.Object _object, bool _nullify = true)
    {
        if (Application.isEditor && !Application.isPlaying)
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.delayCall += () =>
            {
                DestroyImmediate(_object);
                if (_nullify) _object = null;
            };
#endif
        }
        else
        {
            Destroy(_object);
            if (_nullify) _object = null;
        }
    }

    public static void ClearList<T>(List<T> list)
    {
        if (Application.isEditor && !Application.isPlaying)
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.delayCall += () =>
            {
                list.Clear();
            };
#endif
        }
        else
        {
            list.Clear();
        }
    }


    public static void SafeDestroy(UnityEngine.Object _object, UnityEngine.Object _object2)
    {
        if (Application.isEditor && !Application.isPlaying)
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.delayCall += () =>
            {
                DestroyImmediate(_object);
                DestroyImmediate(_object2);
                _object = null;
                _object2 = null;
            };
#endif
        }
        else
        {
            Destroy(_object);
            Destroy(_object2);
            _object = null;
            _object2 = null;
        }
    }

    public static void SafeDestroy(UnityEngine.Object _object, UnityEngine.Object _object2, UnityEngine.Object _object3)
    {
        if (Application.isEditor && !Application.isPlaying)
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.delayCall += () =>
            {
                DestroyImmediate(_object);
                DestroyImmediate(_object2);
                DestroyImmediate(_object3);
                _object = null;
                _object2 = null;
                _object3 = null;
            };
#endif
        }
        else
        {
            Destroy(_object);
            Destroy(_object2);
            Destroy(_object3);
            _object = null;
            _object2 = null;
            _object3 = null;
        }
    }
}


