﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Threading.Tasks;
#if UNITY_EDITOR
using UnityEditor;
using Unity.EditorCoroutines.Editor;
#endif
/// <summary>
/// Encapsulates functions for using DictationRecognizer.
/// </summary>
[ExecuteAlways]
public class DictationEngine
{
    #region Instancing
    private static DictationEngine instance;
    /*
    public static DictationEngine Instance
    {
        get
        {
            if (instance == null)
            {
                if (AnythingCreator.Instance.gameObject.GetComponent<DictationEngine>())
                {
                    AWSafeDestroy.Destroy(AnythingCreator.Instance.gameObject.GetComponent<DictationEngine>());
                    instance = AnythingCreator.Instance.gameObject.AddComponent<DictationEngine>();
                }
            }
            return instance;
        }
    }
    */

    /*
    public static DictationEngine Instance
    {
        get
        {
            if (AnythingCreator.Instance.gameObject.GetComponent<DictationEngine>())
            {
                AWSafeDestroy.SafeDestroy(AnythingCreator.Instance.gameObject.GetComponent<DictationEngine>());
            }
            instance = AnythingCreator.Instance.gameObject.AddComponent<DictationEngine>();
            return instance;
        }
    }
    */

    #endregion
    public string ResultText;
    public DictationRecognizer dictationRecognizer;
    [SerializeField]
    public class UnityEventString : UnityEngine.Events.UnityEvent<string> { };
    public UnityEventString OnPhraseRecognized;

    public UnityEngine.Events.UnityEvent OnUserStartedSpeaking;

    private bool isUserSpeaking;
    private void Start()
    {
        //StartDictationEngine();
    }

    public SpeechSystemStatus Status
    {
        get
        {
            if (dictationRecognizer != null)
            {
                return dictationRecognizer.Status;
            }
            else
            {
                dictationRecognizer = new DictationRecognizer();
                return dictationRecognizer.Status;
            }
        }
    }


    private void DictationRecognizer_OnDictationHypothesis(string text)
    {
        Debug.LogFormat("Dictation hypothesis: {0}", text);

        if (isUserSpeaking == false)
        {
            isUserSpeaking = true;
            OnUserStartedSpeaking?.Invoke();
        }
    }
    /// <summary>
    /// thrown when engine has some messages, that are not specifically errors
    /// </summary>
    /// <param name="completionCause"></param>
    private void DictationRecognizer_OnDictationComplete(DictationCompletionCause completionCause)
    {
        if (completionCause != DictationCompletionCause.Complete)
        {
            Debug.LogWarningFormat("Dictation completed unsuccessfully: {0}.", completionCause);


            switch (completionCause)
            {
                case DictationCompletionCause.TimeoutExceeded:
                    CloseDictationEngine();
                    break;
                case DictationCompletionCause.PauseLimitExceeded:
                    //we need a restart
                    CloseDictationEngine();
                    StartDictationEngine();
                    break;

                case DictationCompletionCause.UnknownError:
                case DictationCompletionCause.AudioQualityFailure:
                case DictationCompletionCause.MicrophoneUnavailable:
                case DictationCompletionCause.NetworkFailure:
                    //error without a way to recover
                    CloseDictationEngine();
                    break;

                case DictationCompletionCause.Canceled:
                    dictationRecognizer.Stop();
                    CloseDictationEngine();
                    break;
                //happens when focus moved to another application 

                case DictationCompletionCause.Complete:
                    //dictationRecognizer.Start();
                    //CloseDictationEngine();
                    //StartDictationEngine();
                    CloseDictationEngine();
                    break;
            }
        }
        else
        {
            CloseDictationEngine();
        }
    }

    /// <summary>
    /// Resulted complete phrase will be determined once the person stops speaking. the best guess from the PC will go on the result.
    /// </summary>
    /// <param name="text"></param>
    /// <param name="confidence"></param>
    private void DictationRecognizer_OnDictationResult(string text, ConfidenceLevel confidence)
    {
        Debug.LogFormat("Dictation result: {0}", text);
        ResultText += text + "\n";

        if (isUserSpeaking == true)
        {
            isUserSpeaking = false;
            OnPhraseRecognized?.Invoke(text);
        }
    }
    private void DictationRecognizer_OnDictationError(string error, int hresult)
    {
        Debug.LogErrorFormat("Dictation error: {0}; HResult = {1}.", error, hresult);
    }
    public void StartDictationEngine()
    {
        CloseDictationEngine();

        isUserSpeaking = false;
        dictationRecognizer = new DictationRecognizer();

        dictationRecognizer.DictationHypothesis += DictationRecognizer_OnDictationHypothesis;
        dictationRecognizer.DictationResult += DictationRecognizer_OnDictationResult;
        dictationRecognizer.DictationComplete += DictationRecognizer_OnDictationComplete;
        dictationRecognizer.DictationError += DictationRecognizer_OnDictationError;


        Debug.Log("Attempt to start dictationRecognizer");
        dictationRecognizer.Start();
    }

    public void CloseDictationEngine()
    {

        if (dictationRecognizer != null)
        {
            Debug.Log("Stopping Dictation Engine");
            dictationRecognizer.DictationHypothesis -= DictationRecognizer_OnDictationHypothesis;
            dictationRecognizer.DictationComplete -= DictationRecognizer_OnDictationComplete;
            dictationRecognizer.DictationResult -= DictationRecognizer_OnDictationResult;
            dictationRecognizer.DictationError -= DictationRecognizer_OnDictationError;

            if (dictationRecognizer.Status == SpeechSystemStatus.Running)
                dictationRecognizer.Stop();

            dictationRecognizer.Dispose();
            dictationRecognizer = null;
        }
    }

    #region Custom Listening Code

    #endregion


    /// <summary>
    /// Calls Start and Stop dictation and monitors status/timeout.
    /// </summary>
    /// <returns></returns>
    public IEnumerator ListenerUpdate()
    {
        StartDictationEngine();
        double startTime = EditorApplication.timeSinceStartup;
        double manualTimeout = 5;

        while (EditorApplication.timeSinceStartup - startTime <= manualTimeout)
        {
            // FB: Make sure to collapse the console in Unity or this will spam you.
            Debug.Log(dictationRecognizer.Status.ToString());
            yield return new EditorWaitForSeconds(0.1f);
        }

        CloseDictationEngine();
    }

}
