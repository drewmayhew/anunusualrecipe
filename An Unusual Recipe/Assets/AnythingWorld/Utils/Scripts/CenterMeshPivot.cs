﻿using UnityEngine;


public class CenterMeshPivot : MonoBehaviour
{
    private const bool SHOW_DEBUG_CENTER_SPHERE = false;
    private GameObject _pivotSphere;
    private float _wheelRadius;

    private Vector3 CenterPivotOnMesh()
    {
        // TODO: be careful! specific to one GameObject structure for wheels [ this -> parentholder -> rotator ]
        MeshFilter wheelMeshFilter = transform.GetComponentInChildren<MeshFilter>();
        if (wheelMeshFilter != null)
        {
            Mesh wheelMesh = wheelMeshFilter.mesh;

            Transform parentHolder = transform.GetChild(0);
            Transform rotator = parentHolder.GetChild(0);

            Vector3 center = wheelMesh.bounds.center;     //  mesh bounding box center point in local space 


            _pivotSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            _pivotSphere.name = "wheelcenter";
            Destroy(_pivotSphere.GetComponent<Collider>());
            _pivotSphere.transform.parent = parentHolder;
            _pivotSphere.transform.localPosition = Vector3.zero;

            // debug indicator of center
            if (!SHOW_DEBUG_CENTER_SPHERE)
            {
                Destroy(_pivotSphere.GetComponent<Renderer>());
            }


            _wheelRadius = wheelMesh.bounds.extents.y * parentHolder.localScale.y;

            rotator.localPosition = center * -1;

            Vector3 objCenterWorldSpace = transform.TransformPoint(center);   //  world space position of mesh bounding box center
            Vector3 relativePos = transform.InverseTransformPoint(center);

            // obj is scaled, so adjust here accordingly
            float referenceScale = parentHolder.localScale.x;

            // Debug.Log($"{gameObject.name} referenceScale = {referenceScale}");

            transform.localPosition += objCenterWorldSpace * referenceScale;
            transform.localPosition -= transform.parent.position * referenceScale;

            return objCenterWorldSpace;

        }

        return Vector3.zero;

    }


    public Vector3 PivotCenter
    {
        get
        {
            if (_pivotSphere == null)
            {
                Transform pivotTransform = transform.FindDeepChild("wheelcenter");
                if (pivotTransform)
                {
                    //  Debug.Log("existing pivotsphere on : " + gameObject.name);
                    _pivotSphere = pivotTransform.gameObject;
                }
                else
                {
                    // Debug.Log("no pivotsphere on : " + gameObject.name);
                    CenterPivotOnMesh();
                }
            }

            return _pivotSphere.transform.position;
        }
    }

    public float Radius
    {
        get
        {
            if (_pivotSphere == null)
            {
                Transform pivotTransform = transform.FindDeepChild("wheelcenter");
                if (pivotTransform)
                {
                    Debug.Log("existing pivotsphere on : " + gameObject.name);
                    _pivotSphere = pivotTransform.gameObject;
                }
                else
                {
                    Debug.Log("no pivotsphere on : " + gameObject.name);
                    CenterPivotOnMesh();
                }
            }

            return _wheelRadius;
        }
    }
}
