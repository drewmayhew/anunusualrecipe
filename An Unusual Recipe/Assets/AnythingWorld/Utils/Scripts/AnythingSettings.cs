﻿using UnityEngine;

[CreateAssetMenu(fileName = "AnythingSettings", menuName = "AnythingWorld/AnythingSettings", order = 1)]
public class AnythingSettings : SingletonScriptableObject<AnythingSettings>
{
    [Tooltip("Your Anything World API key goes here.")]
    public string APIKey="";
    [Tooltip("Name of application. Please ensure this value is different for each app.")]
    public string AppName = "My App";
    [Tooltip("Your address for electronic mail communications.")]
    public string Email;
    [Tooltip("Dialogflow project id. Please ensure this value corresponds to the Google project associated with the agent.")]
    public string AgentId = "anything-world-api";
    [Tooltip("Target frame rate - default is 30")]
    public int FrameRate = 30;
    [Tooltip("Lower value typically results in smoother and less jittery animations. Higher gives more accurate (but expensive) results.")]
    public int PhysicsSolverIterations = 2;
    [Tooltip("Joint based animations rely on this - default is 0.1")]
    public float MaximumAllowedTimeStep = 0.1f;
    public GameObject anythingSetup;
    [Tooltip("Show AW Debug Messages")]
    public bool showDebugMessages = false;
}