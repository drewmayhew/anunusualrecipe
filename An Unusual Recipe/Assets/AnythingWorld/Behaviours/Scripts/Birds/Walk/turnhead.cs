﻿using UnityEngine;

public class turnhead : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 0.5f;
    public float maxRotation = 5f;
    void Start()
    {

    }

    // Update is called once per frame


    void Update()
    {
        transform.localRotation = Quaternion.Euler(maxRotation * Mathf.Sin(Time.time * speed), 0f, 0f);

        //transform.Rotate(0, 0, 10);
    }
}

