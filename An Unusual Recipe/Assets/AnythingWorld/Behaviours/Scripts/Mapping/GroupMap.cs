﻿/*
*
*
* TODO: make 
*
*
*/



using UnityEngine;

public static class GroupMap
{

    public static string GROUP_PREFAB_PATH = "Prefabs/Groups/";

    private static bool _hasGroup;
    public static bool HasGroup
    {
        get
        {
            return _hasGroup;
        }
        set
        {
            _hasGroup = value;
        }
    }

    public static bool ThingHasAGroup(string behaviourName, string categoryName)
    {
        bool thingHasGroup = false;

        if (behaviourName != null)
        {
            // TODO: develop system for groups
            if (behaviourName.IndexOf("swim") != -1 || (behaviourName == "fly" && categoryName.IndexOf("vehicle") == -1))
            {
                thingHasGroup = true;
            }
        }

        return thingHasGroup;
    }

    public static GameObject GroupPrefab(string behaviourName)
    {
        string groupPrefabName = "";

        if (behaviourName == "fly")
        {
            groupPrefabName = "BirdFlock";
        }
        else if (behaviourName.IndexOf("swim") != -1)
        {
            groupPrefabName = "FishFlock";
        }

        string prefabLoc = GROUP_PREFAB_PATH + groupPrefabName;
        GameObject groupPref = Resources.Load(prefabLoc) as GameObject;
        groupPref.name = groupPrefabName + " Group";
        return groupPref;
    }
}
