﻿using UnityEngine;


/// <summary>
/// Base class from which AWBehaviours are derived.
/// </summary>
public abstract class AWBehaviour : MonoBehaviour
{
    #region Fields
    protected abstract string[] targetAnimationType { get; set; }

    public string parentPrefabType { get; set; }
    protected PrefabAnimationsDictionary settings;
    [SerializeField, HideInInspector]
    protected Component animatorComponent;
    [SerializeField, HideInInspector]
    protected AWAnimationController animator;
    [SerializeField, HideInInspector]
    public bool baseInitialised = false;
    [SerializeField, HideInInspector]
    public bool ReadyToGo { get; set; } = false;
    public GameObject AWThing
    {
        get
        {
            if (ParentAWObj == null)
            {
                if (ParentAWObj == null) ParentAWObj = GetComponentInParent<AWObj>();

                return ParentAWObj.awThing;
            }
            else
            {
                return ParentAWObj.awThing;
            }
        }
    }

    [SerializeField]
    public AWObj ParentAWObj { get; set; } = null;


    public Transform AWThingTransform
    {
        get
        {
            if (AWThing == null)
                return null;
            else
                return AWThing.transform;
        }
    }
    // inherit behaviours from this class to make sure they do not run prematurely when created at runtime!
    #endregion

    #region Unity Callbacks
    public virtual void Reset()
    {
        if (!baseInitialised)
        {
            InitializeBehaviour();
        }
    }
    public virtual void OnEnable()
    {

        //Debug.Log("OnEnabled for AWBehaviour called");
        //if (!ReadyToGo) this.enabled = false;
        if (!baseInitialised)
        {
            InitializeBehaviour();
        }
    }
    public virtual void Awake()
    {
        if (!baseInitialised)
        {
            InitializeBehaviour();
        }
    }

    private void Start()
    {

    }

    #endregion

    #region Virtual Functions
    public virtual void SetDefaultParametersValues()
    {

    }
    public virtual void InitializeBehaviour()
    {
        //Debug.Log("AWBehaviour.InitializeBehaviour");
        if (ReadyToGo && !baseInitialised)
        {
            if (ParentAWObj == null) ParentAWObj = GetComponentInParent<AWObj>();
            //Debug.Log("Ready to go: Initalizing behaviour for "+ParentAWObj._objName);
            AddAWAnimator();
            SetDefaultParametersValues();

            if (ParentAWObj != null)
            {
                baseInitialised = true;
            }
        }

    }

    /// <summary>
    /// Get the awThing object that behaviours act on.
    /// </summary>
    private void GetAWThing()
    {
        ParentAWObj = gameObject.GetComponentInParent<AWObj>();
        if (ParentAWObj == null)
        {
            Debug.Log("ParentAWObj not found in GetAWThing");
            return;
        }
    }
    //FTODO: Convert getanimationscript name to Type based function instead :)
    public virtual void AddAWAnimator()
    {
        //Debug.Log("Adding animator to " + ParentAWObj._objName);
        // special cases for flock objects, where AWObj is removed on creation
        if (ParentAWObj == null)
        {
            Debug.LogError("No parentAWObj found");
            return;
        }
        parentPrefabType = ParentAWObj.GetObjCatBehaviour();

        //Query required animator for our prefab type
        if (parentPrefabType != null)
        {
            if (targetAnimationType != null)
            {
                settings = ScriptableObject.CreateInstance<PrefabAnimationsDictionary>();
                if (settings != null)
                {
                    string animatorScriptstring = null;
                    System.Type animatorScript = null;


                    foreach (string animationType in targetAnimationType)
                    {

                        animatorScriptstring = settings.GetAnimationScriptName(parentPrefabType, animationType);
                        if (animatorScriptstring != null)
                        {
                            animatorScript = System.Type.GetType(animatorScriptstring);
                            // Debug.Log("Animator script found:" + animatorScript);
                            break;
                        }
                    }

                    //Check if animator type exists
                    if (animatorScript != null)
                    {
                        animatorComponent = AWThing.AddComponent(animatorScript);
                        animator = AWThing.GetComponent<AWAnimationController>();

                        // Debug.Log("Animator " + animator + " added to " + ParentAWObj._objName);
                    }
                }
                else
                {
                    Debug.Log("Animation map settings could not be created.");
                    return;
                }
            }
            else
            {
                Debug.Log("No animation type provided");
                return;
            }
        }
        else
        {
            Debug.Log("No parent prefab part found");
            return;
        }
    }
    public virtual void RemoveAWAnimator()
    {
        if (animator)
        {
            animator.DestroySubscripts();
            AWSafeDestroy.SafeDestroy(animator);
        }
        else
        {
            Debug.Log("Error: No animator found");
        }
    }
    public void OnDestroy()
    {
        RemoveAWAnimator();
    }

    #endregion
}
