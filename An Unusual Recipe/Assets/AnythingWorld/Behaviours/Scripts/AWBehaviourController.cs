﻿
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(AWObj))]
[System.Serializable]
///<summary>
///Handles the adding and removing of AWBehaviours an AWObj.
///</summary>
public class AWBehaviourController : MonoBehaviour
{
    #region Fields
    //Container in which object is spawned
    public AWObj parentAWObj;
    //Parent object for all limbs
    public GameObject awThing;
    private AWBehaviour[] activeBehaviours;
    private string[] availableBehaviourStrings;
    private string[] activeBehaviourStrings;
    private string[] availableDescriptors;
    private string[] activeDescriptors;
    private List<string[]> activeBehaviourNameDesc;
    private string prefabType = "";
    #endregion

    #region Unity Callbacks

    private void OnEnable()
    {
        //Set parent AWObj if not set
        if (!parentAWObj)
        {
            parentAWObj = gameObject.GetComponent<AWObj>();
            //Debug.Log("Parent AWObj assigned in BC: " + parentAWObj.name);
        }
    }
    void Update()
    {
        //Set awthing transform if not set.
        if (!awThing)
        {
            GetObjectInContainer(gameObject.transform, "AWThing");
        }

    }

    private void Start()
    {
    }
    #endregion

    #region Public Methods

    /// <summary>
    /// Add behaviour script to child awThing object.
    /// </summary>
    /// <remarks>
    /// Behaviour script will only be added if base type is AWBehaviour.
    /// </remarks>
    /// <param name="_scriptName">Name of script to add to </param>
    public void AddAWBehaviour(string _scriptName)
    {
        System.Type behaviour = System.Type.GetType(_scriptName);
        if (behaviour != null && behaviour.BaseType == typeof(AWBehaviour))
        {
            parentAWObj.AddBehaviour(behaviour);
        }


        /*
         * System.Type behaviour = System.Type.GetType(_scriptName);
        if (behaviour!=null && behaviour.BaseType == typeof(AWBehaviour))
        {
            if (!awThing)
            {
                GetObjectInContainer(gameObject.transform, "AWThing");
            }
            else
            {
                if (awThing.GetComponent(_scriptName) == null)
                {
                    var behaviourScript = (AWBehaviour)awThing.AddComponent(System.Type.GetType(_scriptName));
                    //behaviourScript.SetDefaultParametersValues();
                    //behaviourScript.InitializeBehaviour();
                    RefreshBehaviours();
                    return behaviourScript;
                }
                else
                {
                    Debug.Log("Added script that is already present");
                }
            }


        }
        else
        {
            Debug.LogError("Non AWBehaviour script tried to be added.");
        }

        RefreshBehaviours();
        return null;
         */

    }

    public AWBehaviour AddAWBehaviour<T>() where T : AWBehaviour
    {
        var behaviour = parentAWObj.behaviourObject.AddComponent<T>();
        behaviour.enabled = false;
        return behaviour;
    }
    /// <summary>
    /// Removes behaviour matching script name from awThing model.
    /// </summary>
    /// <param name="_scriptName">Name of script of thing to remove.</param>
    public void RemoveBehaviour(string _scriptName)
    {
        System.Type behaviourType = System.Type.GetType(_scriptName);
        if (parentAWObj.behaviourObject.TryGetComponent(behaviourType, out Component outputScript))
        {
            // Debug.Log("check");

            AWBehaviour tmp = (AWBehaviour)outputScript;
            tmp.RemoveAWAnimator();
            AWSafeDestroy.SafeDestroy(tmp);
        }
        else
        {
            Debug.LogError("No script of type " + _scriptName + " found to remove on " + parentAWObj.gameObject.name);
        }
    }



    /// <summary>
    /// Finds all active behaviours on model.
    /// <summary>
    /// <remarks>
    /// Finds all scripts of type AWbehaviour in the child objects and adds to list.
    /// Updates string array of active scripts to pass to editor script.
    /// </remarks>
    public void FindActiveBehaviours()
    {
        activeBehaviours = GetComponentsInChildren<AWBehaviour>();
        List<string> scriptNames = new List<string>();
        foreach (var script in activeBehaviours)
        {
            scriptNames.Add(script.GetType().ToString());
        }
        activeBehaviourStrings = scriptNames.ToArray();
    }

    public void RemoveActiveBehaviours()
    {
        FindActiveBehaviours();
        foreach (var script in activeBehaviours)
        {
            RemoveBehaviour(script.name);
        }
        RefreshBehaviours();
    }




    /// <summary>
    /// Get array of names of available behaviour scripts.
    /// </summary>
    /// <returns>Array of name string of available scripts for this model type.</returns>
    public string[] GetAvailableBehaviourStrings()
    {
        return availableBehaviourStrings;
    }

    /// <summary>
    /// Get array of names of active behaviour scripts on this object/
    /// </summary>
    /// <returns>Array of name strings of active scripts for this model.</returns>
    public string[] GetActiveBehaviourStrings()
    {
        return activeBehaviourStrings;
    }


    public void RefreshBehaviours()
    {
        FindActiveBehaviours();
        FindAvailableBehaviours();
    }

    /// <summary>
    /// Get name of the prefab.
    /// </summary>
    /// <returns></returns>
    public string GetPrefabType()
    {
        return gameObject.GetComponent<AWObj>().GetObjCategory();
    }


    public void EnableGravity()
    {
        if (awThing != null)
        {
            if (awThing.GetComponent<Rigidbody>() != null)
            {
                if (!awThing.GetComponent<Rigidbody>().useGravity)
                {
                    //Add main collider script 
                    if (awThing.gameObject.GetComponent<AddCollider>() == null)
                    {
                        awThing.gameObject.AddComponent(typeof(AddCollider));
                    }
                    //Enable gravity
                    awThing.GetComponent<Rigidbody>().useGravity = true;
                }
            }
        }
    }

    public void DisableGravity()
    {
        if (transform.childCount > 0 && awThing != null)
        {
            if (awThing.GetComponent<Rigidbody>() != null)
            {
                if (awThing.GetComponent<Rigidbody>().useGravity)
                {
                    // Disable gravity
                    awThing.GetComponent<Rigidbody>().useGravity = false;

                    //Remove main collider script
                    if (awThing.gameObject.GetComponent<AddCollider>())
                    {
                        AWSafeDestroy.SafeDestroy(awThing.gameObject.GetComponent<AddCollider>());
                    }

                    //Remove colliders
                    Collider[] prefabColliders = awThing.gameObject.GetComponents<Collider>();
                    foreach (var collider in prefabColliders)
                    {
                        AWSafeDestroy.SafeDestroy(collider);
                    }
                }
            }
        }
    }
    #endregion

    #region Private Methods

    /// <summary>
    /// Finds parent transform matching the tag and assigns to awThing variable.
    /// </summary>
    /// <param name="parent">Parent transform to search beneath.</param>
    /// <param name="tag">Tag to find matches to.</param>
    private void GetObjectInContainer(Transform parent, string tag)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            Transform child = parent.GetChild(i);
            if (child.tag == tag)
            {
                awThing = child.gameObject;
            }
            if (child.childCount > 0)
            {
                GetObjectInContainer(child, tag);
            }
        }
    }


    /// <summary>
    /// Loads all active AWBehaviours in hierarchy.
    /// </summary>
    /// <remarks>
    /// Finds all scripts in the Assets/Resources/Anythingworld/Behaviours/Scripts/*prefabcategory* folder
    /// Loads a string of the names of these scripts to the activeBehaviourStrings array.
    /// </remarks>
    private void FindAvailableBehaviours()
    {
        prefabType = parentAWObj.GetObjCatBehaviour();
        if (prefabType != "")
        {
            //Load directory to find scripts for this type of prefab
            string dir = "Behaviours/Scripts/" + prefabType;
            //Load all scripts in directory into a list
            List<string> names = new List<string>();
            foreach (var assemb in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var script in assemb.GetTypes())
                {
                    if (script.BaseType == typeof(AWBehaviour))
                    {
                        names.Add(script.Name);
                    }
                }
            }
            availableBehaviourStrings = names.ToArray();
        }
        else
        {
            Debug.Log("No prefab type found on " + gameObject.name);
        }

    }
    #endregion
}
