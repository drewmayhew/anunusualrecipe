﻿using UnityEngine;

public class FollowTransform : MonoBehaviour
{

    public Transform followTransform;

    public bool LockX;
    public bool LockY;
    public bool LockZ;


    void LateUpdate()
    {
        Vector3 followPos = followTransform.position;
        Vector3 updatePos = transform.position;
        if (!LockX)
            updatePos.x = followPos.x;
        if (!LockY)
            updatePos.y = followPos.y;
        if (!LockZ)
            updatePos.z = followPos.z;
        transform.position = updatePos;
    }
}
