﻿using UnityEngine;

public class GroundDetect : MonoBehaviour
{

    private bool grounded = true;
    public bool Grounded
    {
        get
        {
            return grounded;
        }
    }

    /// <summary>
    /// Detect collision with floor
    /// </summary>
    /// <param name="hit"></param>
    private void OnCollisionEnter(Collision hit)
    {
        Debug.Log("collision enter");
        if (hit.gameObject.tag == "ground")
        {
            grounded = true;
        }
    }

    /// <summary>
    /// Detect collision exit with floor
    /// </summary>
    /// <param name="hit"></param>
    private void OnCollisionExit(Collision hit)
    {
        if (hit.gameObject.tag == "ground")
        {
            grounded = false;
        }
    }
}
