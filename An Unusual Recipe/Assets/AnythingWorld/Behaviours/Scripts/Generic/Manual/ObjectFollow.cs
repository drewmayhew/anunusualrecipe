﻿using UnityEngine;

public class ObjectFollow : MonoBehaviour
{

    private Rigidbody _rBody;

    // Start is called before the first frame update
    void Start()
    {
        _rBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _rBody.MovePosition(transform.position + (transform.forward * 100f) * Time.fixedDeltaTime);
    }
}
