﻿using UnityEngine;

public class UniformBobController : MonoBehaviour
{

    // Target position the vehicle is trying to reach
    Vector3 _targetPosition;
    // The speed of the animal's movement
    public float moveSpeed = 15f;
    // The speed of the animal's rotation
    public float turnSpeed = 1f;
    // Start is called before the first frame update
    void Start()
    {
        GetRandomTargetPos();
    }

    // Update is called once per frame
    void Update()
    {
        // Update position to target
        Vector3 m_DirToTarget = _targetPosition - transform.position;        // Check if the target hasn't been reached yet
        if (Mathf.Abs(m_DirToTarget.x) < 20 && Mathf.Abs(m_DirToTarget.y) < 20 && Mathf.Abs(m_DirToTarget.z) < 20)
        {
            // Pick new target position
            GetRandomTargetPos();
            m_DirToTarget = _targetPosition - transform.position;
        }        // Turn towards the target adding banking and shaking
        Vector3 normalizedLookDirection = m_DirToTarget.normalized;
        Quaternion m_LookRotation = Quaternion.LookRotation(normalizedLookDirection);
        transform.rotation = Quaternion.Slerp(transform.rotation, m_LookRotation, Time.deltaTime * turnSpeed);        // Move vehicle towards the target
        transform.GetComponent<Rigidbody>().MovePosition(Vector3.Lerp(transform.position, transform.position + transform.forward, moveSpeed * Time.deltaTime));
    }

    /// <summary>
    /// Method used for specifying the new target position.
    /// </summary>
    private void GetRandomTargetPos()
    {
        _targetPosition = new Vector3(Random.Range(-50f, 50f), transform.position.y, Random.Range(-50f, 50f));
    }

}
