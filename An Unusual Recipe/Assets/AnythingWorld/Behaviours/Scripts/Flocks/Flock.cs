﻿using System.Collections.Generic;
using UnityEngine;

public class Flock : MonoBehaviour
{
    private readonly bool SHOW_DEBUG_GOALS = false;
    public GameObject[] flockPrefabs;
    public int flockSize = 50;
    public int numMembers = 1;
    public List<GameObject> allMembers;
    public Vector3 goalPos = Vector3.zero;
    private GameObject debugObj;
    private bool _setup = false;

    private void Start()
    {
        if (SHOW_DEBUG_GOALS)
            debugObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
    }

    public List<GameObject> GetAllMembers()
    {
        return allMembers;
    }

    public void MakeMember()
    {
        for (int i = 0; i < numMembers; i++)
        {
            Vector3 pos = new Vector3(
                Random.Range(-flockSize, flockSize),
                Random.Range(-flockSize, flockSize),
                Random.Range(-flockSize, flockSize)
            );
            GameObject member = (GameObject)Instantiate(
                flockPrefabs[Random.Range(0, flockPrefabs.Length)], pos, Quaternion.identity);
            member.transform.parent = transform;
            allMembers[i] = member;
        }
    }

    public void AddMember(FlockMember newMember)
    {
        if (allMembers == null)
        {
            allMembers = new List<GameObject>();

        }

        if (!_setup)
        {
            // create reference box collider
            BoxCollider refArea = gameObject.GetComponent<BoxCollider>();
            if (refArea == null)
            {
                refArea = gameObject.AddComponent<BoxCollider>();
                refArea.isTrigger = true;
            }
            float realflockSize = flockSize * 2;
            refArea.size = new Vector3(realflockSize, realflockSize, realflockSize);
            refArea.center = new Vector3(0, 0, 0);
            _setup = true;
        }


        newMember.transform.position = GetGoalPos();
        newMember.transform.parent = transform;
        allMembers.Add(newMember.gameObject);
        numMembers++;
    }

    void Update()
    {
        HandleGoalPos();
    }

    void HandleGoalPos()
    {
        if (Random.Range(1, 1000) < 10)
        {
            goalPos = GetGoalPos();
            if (SHOW_DEBUG_GOALS)
            {
                debugObj.transform.position = goalPos;
            }
        }
    }

    private Vector3 GetGoalPos()
    {
        Vector3 newPos = new Vector3(
                Random.Range(-flockSize, flockSize),
                Random.Range(-flockSize, flockSize),
                Random.Range(-flockSize, flockSize)
            );
        newPos += transform.position;
        return newPos;
    }
}