﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleWheelController : MonoBehaviour
{
    private List<Transform> wheels;
    private List<Transform> wheelsVisual;
    private List<WheelCollider> _wheelColliders;
    private List<WheelCollider> _steerWheelColliders;
    private List<WheelCollider> _driveWheelColliders;
    public float enginePower = 150.0f;
    public float rotationSpeed = 5f;

    public float power = 0.0f;
    public float brake = 0.0f;
    public float steer = 0.0f;
    public float maxSteer = 25.0f;
    public Vector3 VehicleCenterOfMass = new Vector3(0f, -0.5f, 0f);

    private Rigidbody _rBody;
    private Vector3 _target;
    private AWObj _controllingAWObj;
    private bool _readyToGo = false;


    void Awake()
    {
        _readyToGo = false;
    }

    void Start()
    {
        _rBody = GetComponent<Rigidbody>();
        _rBody.centerOfMass = VehicleCenterOfMass;

        UpdateTarget();

        // TODO: careful! reliant on parent object script
        _controllingAWObj = transform.parent.GetComponent<AWObj>();
        if (_controllingAWObj == null)
        {
            Debug.LogError($"No AWObj found for {gameObject.name}");
            return;
        }

        StartCoroutine(WaitForAWObjCompletion());
    }

    private IEnumerator WaitForAWObjCompletion()
    {
        while (!_controllingAWObj.AWObjMade)
            yield return new WaitForEndOfFrame();

        StartCoroutine(CollectWheels());
    }

    private IEnumerator CollectWheels()
    {

        wheels = new List<Transform>();
        wheelsVisual = new List<Transform>();

        _wheelColliders = new List<WheelCollider>();
        _steerWheelColliders = new List<WheelCollider>();
        _driveWheelColliders = new List<WheelCollider>();

        foreach (Transform t in transform.GetComponentsInChildren<Transform>())
        {
            string tName = t.name.ToLower();
            if (t != transform)
            {
                if (tName.IndexOf("wheel_collider") != -1)
                {
                    WheelCollider wCollider = t.GetComponent<WheelCollider>();
                    if (t.GetComponent<WheelCollider>() == null)
                    {
                        wCollider = t.gameObject.AddComponent<WheelCollider>();

                    }
                    wheels.Add(t);
                    _wheelColliders.Add(wCollider);
                }
                else if (tName.IndexOf("wheel") != -1)
                {

                    if (t.childCount > 0)
                    {

                        Transform visualWheel = t.GetChild(0);

                        wheelsVisual.Add(visualWheel);
                    }
                }

                if (tName.IndexOf("steer") != -1)
                {
                    _steerWheelColliders.Add(t.GetComponent<WheelCollider>());
                }
                if (tName.IndexOf("drive") != -1)
                {
                    _driveWheelColliders.Add(t.GetComponent<WheelCollider>());
                }
            }
        }


        for (int i = 0; i < wheels.Count; i++)
        {
            wheels[i].position = wheelsVisual[i].parent.GetComponent<CenterMeshPivot>().PivotCenter;

            wheels[i].GetComponent<WheelCollider>().radius = wheelsVisual[i].parent.GetComponent<CenterMeshPivot>().Radius;
        }

        yield return new WaitForEndOfFrame();
        _readyToGo = true;

    }

    private void LateUpdate()
    {
        if (!_readyToGo)
            return;

        for (int i = 0; i < wheels.Count; i++)
        {
            Transform wheel = wheels[i];
            ApplyLocalPositionToVisuals(wheel.GetComponent<WheelCollider>(), i);
        }
    }

    void Update()
    {
        if (!_readyToGo)
            return;

        Vector3 relativePos = transform.position - _target;
        Quaternion targetRotation = Quaternion.LookRotation(relativePos);

        float transY = transform.eulerAngles.y;
        float DeltaAngle = Mathf.DeltaAngle(transY, targetRotation.eulerAngles.y) * -1;

        float accel = 0.6f;


        Vector3 toTarget = relativePos.normalized;

        // reverse if target behind     
        if (Vector3.Dot(toTarget, transform.forward) < 0)
        {
            accel *= -1;
        }

        power = (accel * enginePower * Time.deltaTime * 250.0f) * -1;

        float targSteer = Mathf.Clamp(DeltaAngle, -maxSteer, maxSteer);

        if (steer != targSteer)
        {
            steer = Mathf.Lerp(steer, targSteer, 0.01f);
        }

        if (brake > 0f)
        {
            ChangeWheels(steer, brake, 0f);
        }
        else if (accel == 0f)
        {
            ChangeWheels(steer, 1f, 0f);
        }
        else
        {
            ChangeWheels(steer, 0f, power);
        }

        float targSqrMag = Vector3.SqrMagnitude(transform.position - _target);

        if (targSqrMag < 100f)
        {
            UpdateTarget();
        }
    }

    private void ChangeWheels(float steerAmount, float brakeAmount, float motorPower)
    {
        // steer
        foreach (WheelCollider sCollider in _steerWheelColliders)
        {
            sCollider.steerAngle = steerAmount;
        }
        // brake
        foreach (WheelCollider wCollider in _wheelColliders)
        {
            wCollider.brakeTorque = brakeAmount;
        }
        // motor torque
        foreach (WheelCollider dCollider in _driveWheelColliders)
        {
            dCollider.motorTorque = motorPower;
        }
    }

    public void ApplyLocalPositionToVisuals(WheelCollider collider, int wheelIndex)
    {

        Transform visualWheel = wheelsVisual[wheelIndex];

        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);

        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }



    private void UpdateTarget()
    {
        _target = new Vector3(Random.Range(-50f, 50f), transform.position.y, Random.Range(-50f, 50f));
    }

}
