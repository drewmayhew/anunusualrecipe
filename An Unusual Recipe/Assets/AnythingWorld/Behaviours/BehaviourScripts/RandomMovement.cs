﻿using UnityEngine;

/// <summary>
/// Generates random points on the X/Z plane and moves agent towards it.
/// </summary>
public class RandomMovement : AWBehaviour
{
    #region Fields
    protected override string[] targetAnimationType { get; set; } = { "walk", "drive", "default" };
    public float turnSpeed;
    public float moveSpeed;
    // Target position the animal is trying to reach.
    public Vector3 TargetPosition { get; set; } = new Vector3(0f, 0f, 0f);
    // Radius for target position.
    public float TargetRadius { get; set; } = 20f;
    public float SpeedMultiplier { get; set; } = 1f;

    /*
     * USE AWThing to access the creature game object, and AWThingTransform to access the transform for the creature object. 
     */
    #endregion

    #region Unity Callbacks
    void Start()
    {
        if (ParentAWObj == null)
        {
            ParentAWObj = gameObject.GetComponentInParent<AWObj>();
            GetRandomTargetPos();
        }
        else
        {
            GetRandomTargetPos();
        }

    }
    void Update()
    {
        SolveMovement();
    }
    #endregion

    #region Private Methods
    private void SolveMovement()
    {
        if (ParentAWObj == null)
        {
            ParentAWObj = gameObject.GetComponentInParent<AWObj>();
        }
        else
        {
            Vector3 m_DirToTarget = TargetPosition - AWThingTransform.position;

            DrawArrow.ForDebug(AWThingTransform.transform.position, m_DirToTarget, Color.green, 2f);
            // Check if the target hasn't been reached yet
            if (Mathf.Abs(m_DirToTarget.x) < TargetRadius && Mathf.Abs(m_DirToTarget.y) < TargetRadius && Mathf.Abs(m_DirToTarget.z) < TargetRadius)
            {
                // Pick new target position
                GetRandomTargetPos();
                m_DirToTarget = TargetPosition - AWThingTransform.position;
            }
            // Turn towards the target
            Vector3 normalizedLookDirection = m_DirToTarget.normalized;
            Quaternion m_LookRotation = Quaternion.LookRotation(normalizedLookDirection);
            AWThingTransform.rotation = Quaternion.Slerp(AWThingTransform.rotation, m_LookRotation, Time.deltaTime * turnSpeed);
            // Move animal towards the target
            AWThingTransform.position = Vector3.Lerp(AWThingTransform.position, AWThingTransform.position + AWThingTransform.forward, SpeedMultiplier * moveSpeed * Time.deltaTime);
        }
    }

    /// <summary>
    /// Method used for specifying the new target position.
    /// </summary>
    private void GetRandomTargetPos()
    {
        TargetPosition = new Vector3(Random.Range(-50f, 50f), AWThingTransform.position.y, Random.Range(-50f, 50f));
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Finds parentPrefab type and gets animator script from animatorMapSettings, adds to object.
    /// </summary>
    public override void SetDefaultParametersValues()
    {
        string prefabType = AWThing.GetComponentInParent<AWObj>().GetObjCatBehaviour();
        string behaviour = "RandomMovement";
        PrefabAnimationsDictionary settings = ScriptableObject.CreateInstance<PrefabAnimationsDictionary>();
        var tSpeed = settings.GetDefaultParameterValue(prefabType, behaviour, "turnSpeed");

        if (turnSpeed == 0)
        {
            turnSpeed = tSpeed;
        }
        var mSpeed = settings.GetDefaultParameterValue(prefabType, behaviour, "moveSpeed");

        if (moveSpeed == 0)
        {
            moveSpeed = mSpeed;
        }
    }
    #endregion
}
