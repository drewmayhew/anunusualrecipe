﻿using System.Collections;
using UnityEngine;

public class HoppingMovement : AWBehaviour
{
    #region Fields
    private new string parentPrefabType;
    public float MovementSpeed = 15f;
    public float TurnSpeed = 5f;
    public float JumpAmount = 10f;
    public float JumpTime = 1f;
    private Rigidbody rBody;
    private float jumpFallMultiplier = 22;
    private float jumpUpMultiplier = -1;
    private float delaMultiplier = 1000f;
    private float startY;
    private Quaternion targetRotation;
    private Vector3 jumpForce;

    [SerializeField]
    [HideInInspector]
    private bool awObjReady = false;
    [SerializeField]
    [HideInInspector]
    private bool inputsMultiplied = false;
    public bool AWObjReady
    {
        set
        {
            awObjReady = value;
        }
    }
    private Vector3 targetPosition;
    private AWObj controllingAWObj;
    private float nextJumpTime;
    private GroundDetect groundDetect;

    protected override string[] targetAnimationType { get; set; } = { "hop", "default" };
    //private bool _isJumping = true;
    #endregion

    #region Unity Callbacks
    private void Start()
    {



        // TODO: careful! reliant on parent object script
        controllingAWObj = transform.parent.GetComponent<AWObj>();
        if (controllingAWObj == null)
        {
            Debug.LogError($"No AWObj found for {gameObject.name}");
            return;
        }

        if (!inputsMultiplied)
        {
            MultiplyInputs();
        }

        StartCoroutine(WaitForAWObjCompletion());
    }
    public override void Awake()
    {
        base.Awake();
        jumpForce = Vector3.zero;
    }
    void Update()
    {
        if (!awObjReady)
        {
            return;
        }
        else if (ParentAWObj == null)
        {
            SetupReferences();
            return;
        }

        Vector3 hopperForce = Vector3.zero;
        // dampen jumps
        if (rBody.velocity.y < 0)
        {
            hopperForce = (Vector3.down * Physics.gravity.y * (jumpFallMultiplier - 1)) * Time.deltaTime;
        }
        else if (rBody.velocity.y > 0)
        {
            hopperForce = (Vector3.up * Physics.gravity.y * (jumpUpMultiplier - 1)) * Time.deltaTime;
        }

        // if grounded add forward speed and jump to forces
        if (groundDetect.Grounded)
        {
            float hopperSpeed = rBody.velocity.sqrMagnitude;
            float speedThresh = MovementSpeed / (delaMultiplier / 10f);
            if (hopperSpeed <= speedThresh)
            {
                hopperForce += AWThingTransform.forward * (MovementSpeed * Time.deltaTime);
            }
            // clamp speed if necessary
            else
            {
                rBody.velocity = rBody.velocity.normalized * (speedThresh / speedThresh);
                // Debug.Log("hopper speed too much = " + hopperSpeed + " vs " + speedThresh);
            }

            GetJumpForce();

            if (jumpForce != Vector3.zero)
            {
                hopperForce += jumpForce;
                jumpForce = Vector3.zero;
            }
        }

        rBody.AddForce(hopperForce, ForceMode.Force);

        Vector3 lookRotation = targetPosition;
        lookRotation.y = AWThingTransform.position.y;

        Quaternion targRot = Quaternion.Lerp(AWThingTransform.rotation, Quaternion.LookRotation(lookRotation - AWThingTransform.position), TurnSpeed * Time.deltaTime);

        targetRotation = targRot;

        AWThingTransform.rotation = targetRotation;

        float targSqrMag = Vector3.SqrMagnitude(AWThingTransform.position - targetPosition);

        if (targSqrMag < 50f)
        {
            UpdateTarget();
        }

        Transform BodyTransform = AWThingTransform.Find("body");
        if (BodyTransform)
        {
            BodyTransform.position = AWThingTransform.position;
            BodyTransform.rotation = AWThingTransform.rotation;
        }

    }
    #endregion

    #region Private Methods
    private IEnumerator WaitForAWObjCompletion()
    {
        //yield return new WaitForEndOfFrame();

        while (!controllingAWObj.AWObjMade)
        {
            yield return new WaitForEndOfFrame();
            Debug.Log(transform.name + " hopper awobj wait..");
        }

        awObjReady = true;
    }


    private void MultiplyInputs()
    {
        MovementSpeed *= delaMultiplier;
        JumpAmount *= delaMultiplier / 10;
        inputsMultiplied = true;
    }
    private void GetJumpForce()
    {
        if (Time.time > nextJumpTime)
        {
            jumpForce = Vector3.up * JumpAmount;
            nextJumpTime = Time.time + JumpTime;
        }
    }
    private void UpdateTarget()
    {
        targetPosition = new Vector3(Random.Range(-50f, 50f), AWThingTransform.position.y, Random.Range(-50f, 50f));
    }

    private void SetupReferences()
    {
        ParentAWObj = gameObject.GetComponentInParent<AWObj>();
        rBody = AWThingTransform.GetComponent<Rigidbody>();
        startY = AWThingTransform.localPosition.y;
        groundDetect = AWThingTransform.gameObject.AddComponent<GroundDetect>();
        UpdateTarget();
    }

    #endregion
    #region Public Methods
    public override void SetDefaultParametersValues()
    {
        string prefabType = gameObject.GetComponentInParent<AWObj>().GetObjCatBehaviour();
        string behaviour = "HoppingMovement";
        PrefabAnimationsDictionary settings = ScriptableObject.CreateInstance<PrefabAnimationsDictionary>();
        MovementSpeed = settings.GetDefaultParameterValue(prefabType, behaviour, "MovementSpeed");
        TurnSpeed = settings.GetDefaultParameterValue(prefabType, behaviour, "TurnSpeed");
        JumpAmount = settings.GetDefaultParameterValue(prefabType, behaviour, "JumpAmount");
        JumpTime = settings.GetDefaultParameterValue(prefabType, behaviour, "JumpTime");
    }
    #endregion










}
