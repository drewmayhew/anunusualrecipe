﻿using System.Collections.Generic;
using UnityEngine;

public class VehicleFourWheelAnimation : AWAnimationController
{

    // Animation settings
    public float enginePower = 500;
    public float rotationSpeed = 10;
    public float power = -1500;
    public float brake = 0;
    public float steer = 0;
    public float maxSteer = 60;
    public Vector3 centerOfMass = new Vector3(0, -0.5f, 0);

    //Autos
    public string Descriptor { get; set; } = "Drive";

    // ParameterController object used for monitoring parameters' changes
    ParameterController paramControl;

    // Start is called before the first frame update
    void Start()
    {
        Initialization("vehicle_four_wheel", "drive");

        // Initialize list of parameters used in this animation script
        parametersList = new List<Parameter>()
        {
            new Parameter("parent", "enginePower","AWVehicleWheelController", "EnginePower",enginePower),
            new Parameter("parent", "rotationSpeed","AWVehicleWheelController","RotationSpeed",rotationSpeed),
            new Parameter("parent", "power","AWVehicleWheelController", "Power",power),
            new Parameter("parent", "brake","AWVehicleWheelController","Brake",brake),
            new Parameter("parent", "steer","AWVehicleWheelController","Steer",steer),
            new Parameter("parent", "maxSteer","AWVehicleWheelController","MaxSteer",maxSteer),
            new Parameter("parent", "centerOfMass","AWVehicleWheelController","VehicleCenterOfMass",0,centerOfMass)
        };

        // Initialize paramControl and _prefabToScript variables
        paramControl = new ParameterController(parametersList);



        // TODO: careful! reliant on parent object script
        controllingAWObj = transform.parent.GetComponent<AWObj>();
        if (controllingAWObj == null)
        {
            controllingAWObj = transform.parent.parent.GetComponent<AWObj>();
            if (controllingAWObj == null)
            {
                Debug.LogError($"No AWObj found for {gameObject.name}");
                return;
            }
        }

        StartCoroutine(WaitForAWObjCompletion());

    }



    /// <summary>
    /// Method used for updating all parameters for the animation
    /// </summary>
    protected override void UpdateParameters()
    {
        // Check which parameters were modified
        List<Parameter> modifiedParameters = paramControl.CheckParameters(new List<(string, float, Vector3)>() { ("enginePower",enginePower,new Vector3()),
            ("rotationSpeed",rotationSpeed,new Vector3()),("power",power,new Vector3()),("brake",brake,new Vector3()),("steer",steer,new Vector3()),("maxSteer",maxSteer,new Vector3()),("centerOfMass",0,centerOfMass)});

        // Update parmeters value in the proper script
        foreach (var param in modifiedParameters)
        {
            prefabToScript[(param.PrefabPart, param.ScriptName)].ModifyParameter(param);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (scriptsSet)
            UpdateParameters();
    }

    //------------------------------------------------------------------------Behaviour specific methods---------------------------------------------------------------



    public override void ActivateShader()
    { }

    public override void DeactivateShader()
    { }

    public override void UpdateMovementSpeed(float speed)
    {
        throw new System.NotImplementedException();
    }
    public override void UpdateMovementSizeScale(float scale)
    {
        throw new System.NotImplementedException();
    }
}
