﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Animation controller for the WingedFlyer walk animation.
/// </summary>
public class WingedFlyerWalkAnimation : WalkAnimation
{
    #region Fields
    // Animation settings
    public float bodyWobbleMovementSpeed = 4;
    public float bodyWobbleMaxAngle = 4;
    public float rightLegMovementSpeed = 3;
    public float leftLegMovementSpeed = 2.4f;
    public float rightWingMovementSpeed = 5;
    public float leftWingMovementSpeed = 4;
    public Vector3 rightLegMovementRotateTo = new Vector3(10, -20, 0);
    public Vector3 leftLegMovementRotateTo = new Vector3(10, 20, 0);
    public Vector3 rightWingMovementRotateTo = new Vector3(0, 0, -40);
    public Vector3 leftWingMovementRotateTo = new Vector3(0, 0, 40);
    public float headTurnSpeed = 3;
    public float headTurnMaxAngle = 30;

    // Autos
    public string Descriptor { get; set; } = "Walk";

    /// ParameterController object used for monitoring parameters' changes
    ParameterController paramControl;
    #endregion

    #region Unity Callbacks
    void Start()
    {
        Initialization("winged_flyer__waddle", "walk");

        // Initialize list of parameters used in this animation script
        parametersList = new List<Parameter>()
        {
            new Parameter("body", "bodyWobbleMovementSpeed","AWWobble", "MoveSpeed",bodyWobbleMovementSpeed),
            new Parameter("body", "bodyWobbleMaxAngle","AWWobble","Frequency",bodyWobbleMaxAngle),
            new Parameter("wing_right", "rightWingMovementSpeed","AWWingFlap", "RotationSpeed",rightWingMovementSpeed),
            new Parameter("wing_right", "rightWingMovementRotateTo","AWWingFlap","RotateTo",0,rightWingMovementRotateTo),
            new Parameter("wing_left", "leftWingMovementSpeed","AWWingFlap","RotationSpeed",leftWingMovementSpeed),
            new Parameter("wing_left", "leftWingMovementRotateTo","AWWingFlap","RotateTo",0,leftWingMovementRotateTo),
            new Parameter("leg_right", "rightLegMovementSpeed","AWWingFlap","RotationSpeed",rightLegMovementSpeed),
            new Parameter("leg_right", "rightLegMovementRotateTo","AWWingFlap","RotateTo",0,rightLegMovementRotateTo),
            new Parameter("leg_left", "leftLegMovementSpeed","AWWingFlap","RotationSpeed",leftLegMovementSpeed),
            new Parameter("leg_left", "leftLegMovementRotateTo","AWWingFlap","RotateTo",0,leftLegMovementRotateTo),
            new Parameter("head", "headTurnSpeed","AWTurnHeadHorizontal","TurnSpeed",headTurnSpeed),
            new Parameter("head", "headTurnMaxAngle","AWTurnHeadHorizontal","MaxRotation",headTurnMaxAngle)
        };

        // Initialize paramControl and _prefabToScript variables
        paramControl = new ParameterController(parametersList);



        // TODO: careful! reliant on parent object script
        controllingAWObj = transform.parent.GetComponent<AWObj>();
        if (controllingAWObj == null)
        {
            controllingAWObj = transform.parent.parent.GetComponent<AWObj>();
            if (controllingAWObj == null)
            {
                Debug.LogError($"No AWObj found for {gameObject.name}");
                return;
            }
        }

        StartCoroutine(WaitForAWObjCompletion());

    }
    void Update()
    {
        if (scriptsSet)
            UpdateParameters();
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Method used for updating the speed of feet movement in the animation
    /// </summary>
    /// <param name="speed"></param>
    public override void UpdateMovementSpeed(float speed)
    {
        rightLegMovementSpeed = speed;
        leftLegMovementSpeed = speed - (0.2f * speed);
    }

    public override void UpdateMovementSizeScale(float scale)
    {
        // throw new System.NotImplementedException();
    }
    public override void ActivateShader()
    {
    }
    public override void DeactivateShader()
    {
    }
    #endregion

    #region Private Methods

    #endregion

    #region Protected Methods
    /// <summary>
    /// Method used for updating all parameters for the animation
    /// </summary>
    protected override void UpdateParameters()
    {
        // Check which parameters were modified
        List<Parameter> modifiedParameters = paramControl.CheckParameters(new List<(string, float, Vector3)>() { ("bodyWobbleMovementSpeed",bodyWobbleMovementSpeed,new Vector3()),
            ("bodyWobbleMaxAngle",bodyWobbleMaxAngle,new Vector3()),("rightWingMovementSpeed",rightWingMovementSpeed,new Vector3()),
            ("rightWingMovementRotateTo",0,rightWingMovementRotateTo),("leftWingMovementSpeed",leftWingMovementSpeed,new Vector3()),
            ("leftWingMovementRotateTo",0,leftWingMovementRotateTo),("rightLegMovementSpeed",rightLegMovementSpeed,new Vector3()),
            ("rightLegMovementRotateTo",0,rightLegMovementRotateTo), ("leftLegMovementSpeed",leftLegMovementSpeed,new Vector3()),
            ("leftLegMovementRotateTo",0,leftLegMovementRotateTo),("headTurnSpeed",headTurnSpeed,new Vector3()),
            ("headTurnMaxAngle",headTurnMaxAngle,new Vector3())});

        // Update parmeters value in the proper script
        foreach (var param in modifiedParameters)
        {
            prefabToScript[(param.PrefabPart, param.ScriptName)].ModifyParameter(param);
        }
    }
    #endregion

}
