﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Animation controller for the walk animation for BipedWalk2 prefab.
/// </summary>
public class BipedWalk2WalkAnimation : WalkAnimation
{
    #region Fields
    // Behaviour settings
    public float bodyShakeMovementSpeed = 0.7f;
    public float bodyShakeMovementFrequency = 0.5f;
    public float rightFrontLegMovementSpeed = 1;
    public float leftFrontLegMovementSpeed = 1;
    public float rightHindLegMovementSpeed = 1;
    public float leftHindLegMovementSpeed = 1;
    public float rightFrontLegMovementRadius = 0.5f;
    public float leftFrontLegMovementRadius = 0.5f;
    public float rightHindLegMovementRadius = 0.5f;
    public float leftHindLegMovementRadius = 0.5f;
    public float rightFrontLegStartDegree = 90;
    public float leftFrontLegStartDegree = 270;
    public float rightHindLegStartDegree = 180;
    public float leftHindLegStartDegree = 0;

    // ParameterController object used for monitoring parameters' changes
    ParameterController paramControl;
    #endregion

    #region Unity Callbacks
    void Start()
    {

        // Obtain settings for this particular animation
        Initialization("biped__walk2", "walk");

        // Initialize list of parameters used in this behaviour script
        parametersList = new List<Parameter>()
        {
            new Parameter("body", "bodyShakeMovementSpeed","AWBodyShakeMovement", "MoveSpeed",bodyShakeMovementSpeed),
            new Parameter("body", "bodyShakeMovementFrequency","AWBodyShakeMovement","Frequency",bodyShakeMovementFrequency),
            new Parameter("hand_right", "rightFrontLegMovementSpeed","AWFeetMovement", "StepSpeed",rightFrontLegMovementSpeed),
            new Parameter("hand_right", "rightFrontLegStartDegree","AWFeetMovement","StepDegree",rightFrontLegStartDegree),
            new Parameter("hand_right", "rightFrontLegMovementRadius","AWFeetMovement","StepRadius",rightFrontLegMovementRadius),
            new Parameter("hand_left", "leftFrontLegMovementSpeed","AWFeetMovement","StepSpeed",leftFrontLegMovementSpeed),
            new Parameter("hand_left", "leftFrontLegStartDegree","AWFeetMovement","StepDegree",leftFrontLegStartDegree),
            new Parameter("hand_left", "leftFrontLegMovementRadius","AWFeetMovement","StepRadius",leftFrontLegMovementRadius),
            new Parameter("foot_right", "rightHindLegMovementSpeed","AWFeetMovement","StepSpeed",rightHindLegMovementSpeed),
            new Parameter("foot_right", "rightHindLegStartDegree","AWFeetMovement","StepDegree",rightHindLegStartDegree),
            new Parameter("foot_right", "rightHindLegMovementRadius","AWFeetMovement","StepRadius",rightHindLegMovementRadius),
            new Parameter("foot_left", "leftHindLegMovementSpeed","AWFeetMovement","StepSpeed",leftHindLegMovementSpeed),
            new Parameter("foot_left", "leftHindLegStartDegree","AWFeetMovement","StepDegree",leftHindLegStartDegree),
            new Parameter("foot_left", "leftHindLegMovementRadius","AWFeetMovement","StepRadius",leftHindLegMovementRadius)
        };


        // Initialize paramControl and _prefabToScript variables
        paramControl = new ParameterController(parametersList);



        // TODO: careful! reliant on parent object script
        controllingAWObj = transform.parent.GetComponent<AWObj>();
        if (controllingAWObj == null)
        {
            controllingAWObj = transform.parent.parent.GetComponent<AWObj>();
            if (controllingAWObj == null)
            {
                Debug.LogError($"No AWObj found for {gameObject.name}");
                return;
            }
        }

        StartCoroutine(WaitForAWObjCompletion());

    }
    void Update()
    {
        if (scriptsSet)
            UpdateParameters();
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Method used for updating the speed of feet movement in the animation
    /// </summary>
    /// <param name="speed"></param>
    public override void UpdateMovementSpeed(float speed)
    {
        rightFrontLegMovementSpeed = speed;
        leftFrontLegMovementSpeed = speed;
        rightHindLegMovementSpeed = speed;
        leftHindLegMovementSpeed = speed;

    }

    /// <summary>
    /// Method used for updating the size of feet movement in the animation
    /// </summary>
    /// <param name="scale"></param>
    public override void UpdateMovementSizeScale(float scale)
    {
        rightFrontLegMovementRadius *= scale;
        leftFrontLegMovementRadius *= scale;
        rightHindLegMovementRadius *= scale;
        leftHindLegMovementRadius *= scale;
    }

    public override void ActivateShader()
    {
        //throw new System.NotImplementedException();
    }

    public override void DeactivateShader()
    {
        //throw new System.NotImplementedException();
    }
    #endregion

    #region Private Methods

    #endregion

    #region Protected Methods
    /// <summary>
    /// Method used for updating all parameters for the behaviour
    /// </summary>
    protected override void UpdateParameters()
    {
        // Check which parameters were modified
        List<Parameter> modifiedParameters = paramControl.CheckParameters(new List<(string, float)>() { ("bodyShakeMovementSpeed",bodyShakeMovementSpeed),
            ("bodyShakeMovementFrequency",bodyShakeMovementFrequency),("rightFrontLegMovementSpeed",rightFrontLegMovementSpeed),
            ("rightFrontLegStartDegree",rightFrontLegStartDegree),("rightFrontLegMovementRadius",rightFrontLegMovementRadius),
            ("leftFrontLegMovementSpeed",leftFrontLegMovementSpeed),("leftFrontLegStartDegree",leftFrontLegStartDegree),
            ("leftFrontLegMovementRadius",leftFrontLegMovementRadius), ("rightHindLegMovementSpeed",rightHindLegMovementSpeed),
            ("rightHindLegStartDegree",rightHindLegStartDegree),("rightHindLegMovementRadius",rightHindLegMovementRadius),
            ("leftHindLegMovementSpeed",leftHindLegMovementSpeed), ("leftHindLegStartDegree",leftHindLegStartDegree),
            ("leftHindLegMovementRadius",leftHindLegMovementRadius)});

        // Update parmeters value in the proper script
        foreach (var param in modifiedParameters)
        {
            prefabToScript[(param.PrefabPart, param.ScriptName)].ModifyParameter(param);
        }
    }
    #endregion
}
