using System.Collections;
using UnityEngine;

// [ExecuteAlways]
public class FlockMemberWingFlap : MonoBehaviour
{
    public float RotateSpeed = 5f;
    public Vector3 RotateTo = new Vector3(0, 0, 20f);
    private Vector3 _rotateFrom;

    private bool _shouldFlap;
    private Vector3 _currentAngle;
    //private float _reachedThreshold = 0.1f;
    private bool _directionTo;
    private Transform _flapTransform;

    void Start()
    {
        RotateTo = transform.localEulerAngles + RotateTo;
        _shouldFlap = false;
        float delayStart = Random.Range(0f, 0.4f);
        StartCoroutine("StartFlap", delayStart);

    }

    private IEnumerator StartFlap(float delayStart)
    {
        yield return new WaitForSeconds(delayStart);

        _flapTransform = transform.Find("WavefrontObject");

        while (_flapTransform == null)
        {
            _flapTransform = transform.Find("WavefrontObject");
            yield return new WaitForEndOfFrame();
        }

        _directionTo = true;
        _rotateFrom = _flapTransform.localEulerAngles;
        _currentAngle = _flapTransform.localEulerAngles;
        _shouldFlap = true;
    }
    private void Update()
    {
        if (_shouldFlap)
        {

            // _currentAngle = _flapTransform.localEulerAngles;
            // RotateTo = transform.localEulerAngles + RotateTo;

            float speedStep = Time.deltaTime * RotateSpeed;

            float angle;
            bool sameRotation;

            if (_directionTo)
            {
                _currentAngle = new Vector3(
                Mathf.LerpAngle(_currentAngle.x, RotateTo.x, speedStep),
                Mathf.LerpAngle(_currentAngle.y, RotateTo.y, speedStep),
                Mathf.LerpAngle(_currentAngle.z, RotateTo.z, speedStep));


                angle = Quaternion.Angle(Quaternion.Euler(_currentAngle), Quaternion.Euler(RotateTo));
                sameRotation = Mathf.Abs(angle) < 1f;

                if (sameRotation)
                {
                    _directionTo = false;
                }
            }
            else
            {
                _currentAngle = new Vector3(
                Mathf.LerpAngle(_currentAngle.x, _rotateFrom.x, speedStep),
                Mathf.LerpAngle(_currentAngle.y, _rotateFrom.y, speedStep),
                Mathf.LerpAngle(_currentAngle.z, _rotateFrom.z, speedStep));

                angle = Quaternion.Angle(Quaternion.Euler(_currentAngle), Quaternion.Euler(_rotateFrom));
                sameRotation = Mathf.Abs(angle) < 1f;

                if (sameRotation)
                {
                    _directionTo = true;
                }
            }

            _flapTransform.localEulerAngles = _currentAngle;
        }
    }
}
