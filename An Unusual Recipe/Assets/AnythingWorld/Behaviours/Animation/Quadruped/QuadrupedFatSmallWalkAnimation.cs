﻿using System.Collections.Generic;
using UnityEngine;

public class QuadrupedFatSmallWalkAnimation : WalkAnimation
{

    // Behaviour settings
    public float bodyShakeMovementSpeed = 1;
    public float bodyShakeMovementFrequency = 0.4f;
    public float rightFrontLegMovementSpeed = 2;
    public float leftFrontLegMovementSpeed = 2;
    public float rightHindLegMovementSpeed = 2;
    public float leftHindLegMovementSpeed = 2;
    public float rightFrontLegMovementRadius = 0.4f;
    public float leftFrontLegMovementRadius = 0.4f;
    public float rightHindLegMovementRadius = 0.1f;
    public float leftHindLegMovementRadius = 0.1f;
    public float rightFrontLegStartDegree = 90;
    public float leftFrontLegStartDegree = 270;
    public float rightHindLegStartDegree = 180;
    public float leftHindLegStartDegree = 0;


    // ParameterController object used for monitoring parameters' changes
    ParameterController paramControl;

    // Start is called before the first frame update
    void Start()
    {
        // Obtain settings for this particular animation
        Initialization("quadruped_fat_small_generic__walk", "walk");

        // Initialize list of parameters used in this behaviour script
        parametersList = new List<Parameter>()
        {
            new Parameter("body", "bodyShakeMovementSpeed","AWBodyShakeMovement", "MoveSpeed",bodyShakeMovementSpeed),
            new Parameter("body", "bodyShakeMovementFrequency","AWBodyShakeMovement","Frequency",bodyShakeMovementFrequency),
            new Parameter("feet_front_right", "rightFrontLegMovementSpeed","AWFeetMovement", "StepSpeed",rightFrontLegMovementSpeed),
            new Parameter("feet_front_right", "rightFrontLegStartDegree","AWFeetMovement","StepDegree",rightFrontLegStartDegree),
            new Parameter("feet_front_right", "rightFrontLegMovementRadius","AWFeetMovement","StepRadius",rightFrontLegMovementRadius),
            new Parameter("feet_front_left", "leftFrontLegMovementSpeed","AWFeetMovement","StepSpeed",leftFrontLegMovementSpeed),
            new Parameter("feet_front_left", "leftFrontLegStartDegree","AWFeetMovement","StepDegree",leftFrontLegStartDegree),
            new Parameter("feet_front_left", "leftFrontLegMovementRadius","AWFeetMovement","StepRadius",leftFrontLegMovementRadius),
            new Parameter("feet_hind_right", "rightHindLegMovementSpeed","AWFeetMovement","StepSpeed",rightHindLegMovementSpeed),
            new Parameter("feet_hind_right", "rightHindLegStartDegree","AWFeetMovement","StepDegree",rightHindLegStartDegree),
            new Parameter("feet_hind_right", "rightHindLegMovementRadius","AWFeetMovement","StepRadius",rightHindLegMovementRadius),
            new Parameter("feet_hind_left", "leftHindLegMovementSpeed","AWFeetMovement","StepSpeed",leftHindLegMovementSpeed),
            new Parameter("feet_hind_left", "leftHindLegStartDegree","AWFeetMovement","StepDegree",leftHindLegStartDegree),
            new Parameter("feet_hind_left", "leftHindLegMovementRadius","AWFeetMovement","StepRadius",leftHindLegMovementRadius)
        };


        // Initialize paramControl and _prefabToScript variables
        paramControl = new ParameterController(parametersList);



        // TODO: careful! reliant on parent object script
        controllingAWObj = transform.parent.GetComponent<AWObj>();
        if (controllingAWObj == null)
        {
            controllingAWObj = transform.parent.parent.GetComponent<AWObj>();
            if (controllingAWObj == null)
            {
                Debug.LogError($"No AWObj found for {gameObject.name}");
                return;
            }
        }

        StartCoroutine(WaitForAWObjCompletion());

    }


    /// <summary>
    /// Method used for updating all parameters for the behaviour
    /// </summary>
    protected override void UpdateParameters()
    {
        // Check which parameters were modified
        List<Parameter> modifiedParameters = paramControl.CheckParameters(new List<(string, float)>() { ("bodyShakeMovementSpeed",bodyShakeMovementSpeed),
            ("bodyShakeMovementFrequency",bodyShakeMovementFrequency),("rightFrontLegMovementSpeed",rightFrontLegMovementSpeed),
            ("rightFrontLegStartDegree",rightFrontLegStartDegree),("rightFrontLegMovementRadius",rightFrontLegMovementRadius),
            ("leftFrontLegMovementSpeed",leftFrontLegMovementSpeed),("leftFrontLegStartDegree",leftFrontLegStartDegree),
            ("leftFrontLegMovementRadius",leftFrontLegMovementRadius), ("rightHindLegMovementSpeed",rightHindLegMovementSpeed),
            ("rightHindLegStartDegree",rightHindLegStartDegree),("rightHindLegMovementRadius",rightHindLegMovementRadius),
            ("leftHindLegMovementSpeed",leftHindLegMovementSpeed), ("leftHindLegStartDegree",leftHindLegStartDegree),
            ("leftHindLegMovementRadius",leftHindLegMovementRadius)});

        // Update parmeters value in the proper script
        foreach (var param in modifiedParameters)
        {
            prefabToScript[(param.PrefabPart, param.ScriptName)].ModifyParameter(param);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (scriptsSet)
            UpdateParameters();
    }

    //------------------------------------------------------------------------Behaviour specific methods---------------------------------------------------------------

    /// <summary>
    /// Method used for updating the speed of feet movement in the animation
    /// </summary>
    /// <param name="speed"></param>
    public override void UpdateMovementSpeed(float speed)
    {
        rightFrontLegMovementSpeed = speed;
        leftFrontLegMovementSpeed = speed;
        rightHindLegMovementSpeed = speed;
        leftHindLegMovementSpeed = speed;

    }


    /// <summary>
    /// Method used for updating the size of feet movement in the animation
    /// </summary>
    /// <param name="scale"></param>
    public override void UpdateMovementSizeScale(float scale)
    {
        rightFrontLegMovementRadius *= scale;
        leftFrontLegMovementRadius *= scale;
        rightHindLegMovementRadius *= scale;
        leftHindLegMovementRadius *= scale;
    }

    public override void ActivateShader()
    {
        throw new System.NotImplementedException();
    }

    public override void DeactivateShader()
    {
        throw new System.NotImplementedException();
    }
}