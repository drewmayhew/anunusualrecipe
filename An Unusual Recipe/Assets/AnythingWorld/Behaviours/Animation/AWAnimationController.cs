﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base animation controller class from which other animation controllers inherit.
/// </summary>
public abstract class AWAnimationController : MonoBehaviour
{
    #region Fields
    protected AWObj controllingAWObj;

    // List of all the parameters used in this animation
    protected List<Parameter> parametersList;

    protected List<PrefabPartToAnimationSubscript> prefabToScriptType;
    protected Dictionary<(string, string), AWAnimationSubscripts> prefabToScript;

    // Variable used to signal finishing of the scripts setup
    protected bool scriptsSet = false;
    #endregion

    #region Unity Callbacks

    #endregion

    #region Public Methods
    public void RemoveAnimationScripts()
    {
        // Iterate over prefab's parts
        if (prefabToScriptType != null)
        {
            foreach (var prefab in prefabToScriptType)
            {
                // Find gameobject related the the prefab's part
                Transform prefabObject = transform;
                if (!prefab.Key.Equals("parent"))
                {
                    prefabObject = transform.Find(prefab.Key);
                }

                // Iterate over the scripts added to the prefab's part
                foreach (var script in prefabToScriptType.Find(x => x.Key == prefab.Key).Value)
                {
                    // Add script to the prefab part and store it in the _prefabToScript dictionary
                    if (((AWAnimationSubscripts)prefabObject.gameObject.GetComponent(System.Type.GetType(script))))
                    {
                        ((AWAnimationSubscripts)prefabObject.gameObject.GetComponent(System.Type.GetType(script))).ResetState();
                        AWSafeDestroy.SafeDestroy(prefabObject.gameObject.GetComponent(System.Type.GetType(script)));
                        //DestroyImmediate(prefabObject.gameObject.GetComponent(System.Type.GetType(script)));
                    }
                }
            }

            if (prefabToScript != null)
                prefabToScript.Clear();

            scriptsSet = false;

        }

    }
    public void RemoveAnimationScriptsEditor()
    {
        // Iterate over prefab's parts
        if (prefabToScriptType != null)
        {
            foreach (var prefab in prefabToScriptType)
            {
                // Find gameobject related the the prefab's part
                Transform prefabObject = transform;
                if (!prefab.Key.Equals("parent"))
                {
                    prefabObject = transform.Find(prefab.Key);
                }

                // Iterate over the scripts added to the prefab's part
                foreach (var script in prefabToScriptType.Find(x => x.Key == prefab.Key).Value)
                {
                    // Add script to the prefab part and store it in the _prefabToScript dictionary
                    ((AWAnimationSubscripts)prefabObject.gameObject.GetComponent(System.Type.GetType(script))).ResetState();


                    // Remove must be called after destroy immediate so no benefit to safedestroying here.
                    if (!Application.isPlaying && Application.isEditor)
                    {
#if UNITY_EDITOR
                        UnityEditor.EditorApplication.delayCall += () =>
                        {
                            DestroyImmediate(prefabObject.gameObject.GetComponent(System.Type.GetType(script)));
                            prefabToScript.Remove((prefab.Key, script));
                        };
#endif

                    }
                    else
                    {
                        Destroy(prefabObject.gameObject.GetComponent(System.Type.GetType(script)));
                        prefabToScript.Remove((prefab.Key, script));
                    }

                }
            }

            scriptsSet = false;
        }
    }

    public abstract void ActivateShader();
    public abstract void DeactivateShader();

    public void DestroySubscripts()
    {
        RemoveAnimationScripts();
        DeactivateShader();
    }

    /// <summary>
    /// Method used for updating the speed of animation according to the speed of the body movement
    /// </summary>
    /// <param name="speed">New movement speed</param>
    public abstract void UpdateMovementSpeed(float speed);

    /// <summary>
    /// Method used for updating the size of the feet movement in the animation according to the model scale
    /// </summary>
    /// <param name="size">New movement step size</param>
    public abstract void UpdateMovementSizeScale(float scale);
    #endregion

    #region Private Methods
    private void OnDestroy()
    {
        RemoveAnimationScripts();
        DeactivateShader();
    }
    #endregion

    #region Protected Methods
    protected abstract void UpdateParameters();



    protected void Initialization(string prefab, string behaviour)
    {
        // Obtain settings for this particular behaviour
        AnimationSettings _settings = ScriptableObject.CreateInstance<AnimationSettings>();
        prefabToScriptType = _settings.GetScriptsForPrefabAnimation(prefab, behaviour);

        prefabToScript = new Dictionary<(string, string), AWAnimationSubscripts>();
    }

    protected void AddAnimationScripts()
    {
        // Iterate over prefab's parts
        foreach (var prefab in prefabToScriptType)
        {
            // Find gameobject related the the prefab's part
            Transform prefabObject = transform;
            if (!prefab.Key.Equals("parent"))
            {
                prefabObject = transform.Find(prefab.Key);
            }

            // Iterate over the scripts added to the prefab's part
            foreach (var script in prefabToScriptType.Find(x => x.Key == prefab.Key).Value)
            {
                // Add script to the prefab part and store it in the _prefabToScript dictionary
                prefabToScript.Add((prefab.Key, script), (AWAnimationSubscripts)prefabObject.gameObject.AddComponent(System.Type.GetType(script)));
            }
        }

        // Update all parameters for the behaviour in order to give them initial value
        foreach (var param in parametersList)
        {
            prefabToScript[(param.PrefabPart, param.ScriptName)].ModifyParameter(param);
        }

        UpdateMovementSizeScale(controllingAWObj.ObjectScale);

        scriptsSet = true;
    }
    protected IEnumerator WaitForAWObjCompletion()
    {
        while (!controllingAWObj.AWObjMade)
            yield return new WaitForEndOfFrame();

        AddAnimationScripts();
        ActivateShader();
    }
    #endregion











}
