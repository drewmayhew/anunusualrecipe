﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Animation script defining the wriggle animation for the "Uniform" prefab type.
/// </summary>
public class UniformWriggleAnimation : AWAnimationController
{
    #region Fields
    /// Shader default parameters
    private float WobbleDistance = 4;
    private float WobbleSpeed = 3;

    /// ParameterController object used for monitoring parameters' changes
    ParameterController paramControl;

    #endregion

    #region Unity Callbacks
    void Start()
    {
        Initialization("uniform__wriggle", "Move");

        // Initialize list of parameters used in this behaviour script
        parametersList = new List<Parameter>();

        // Initialize paramControl variable
        paramControl = new ParameterController(parametersList);



        // TODO: careful! reliant on parent object script
        controllingAWObj = transform.parent.GetComponent<AWObj>();
        if (controllingAWObj == null)
        {
            controllingAWObj = transform.parent.parent.GetComponent<AWObj>();
            if (controllingAWObj == null)
            {
                Debug.LogError($"No AWObj found for {gameObject.name}");
                return;
            }
        }

        StartCoroutine(WaitForAWObjCompletion());

    }
    void Update()
    {
        if (scriptsSet)
            UpdateParameters();
    }

    #endregion

    #region Protected Methods
    /// <summary>
    /// Method used for updating all parameters for the behaviour
    /// </summary>
    protected override void UpdateParameters()
    {
        // Check which parameters were modified
        List<Parameter> modifiedParameters = paramControl.CheckParameters(new List<(string, float, Vector3)>());

        // Update parmeters value in the proper script
        foreach (var param in modifiedParameters)
        {
            prefabToScript[(param.PrefabPart, param.ScriptName)].ModifyParameter(param);
        }
    }
    #endregion

    #region Public Methods
    public override void ActivateShader()
    {
        var shaders = gameObject.GetComponentsInChildren<Renderer>();
        foreach (var shader in shaders)
        {
            shader.sharedMaterial.SetFloat("WobbleDistance", WobbleDistance);
            shader.sharedMaterial.SetFloat("WobbleSpeed", WobbleSpeed);
        }
    }

    public override void DeactivateShader()
    {
        var shaders = gameObject.GetComponentsInChildren<Renderer>();
        foreach (var shader in shaders)
        {
            shader.sharedMaterial.SetFloat("WobbleDistance", 0);
            shader.sharedMaterial.SetFloat("WobbleSpeed", 0);
        }
    }

    public override void UpdateMovementSpeed(float speed)
    {
        throw new System.NotImplementedException();
    }

    public override void UpdateMovementSizeScale(float scale)
    {
        throw new System.NotImplementedException();
    }
    #endregion
}
