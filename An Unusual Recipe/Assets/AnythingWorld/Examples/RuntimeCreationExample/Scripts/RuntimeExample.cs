﻿using System.Collections.Generic;
using UnityEngine;

public class RuntimeExample : MonoBehaviour
{

    [Range(1, 100)]
    public int _objectTotal = 3;

    private AnythingCreator _anythingCreator;
    private AnythingHabitat _anythingHabitat;

    void Start()
    {
        _anythingHabitat = AnythingHabitat.Instance;
        _anythingCreator = AnythingCreator.Instance;

        MakeGameHabitat();
        MakeGameActors();
    }


    private void MakeGameHabitat()
    {
        _anythingHabitat.MakeHabitat("mountain", false);
    }

    private void MakeGameActors()
    {
        List<string> things = new List<string>();
        things.Add("unicycle");
        things.Add("gazelle");
        things.Add("delorian");
        things.Add("car");
        things.Add("armadillo");
        things.Add("plane");

        string randomThing;
        int randomIndex;
        for (int i = 0; i < _objectTotal; i++)
        {
            randomIndex = Random.Range(0, things.Count);
            randomThing = things[randomIndex];
            _anythingCreator.MakeObject(randomThing);
        }
    }
}
