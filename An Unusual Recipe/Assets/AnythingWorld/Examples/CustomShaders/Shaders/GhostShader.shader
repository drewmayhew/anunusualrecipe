﻿Shader "Anything World/Ghost Shader"
{
	Properties
	{
		[MainTexture] _BaseColor("Base Color", Color) = (1, 1, 1, 1)
		[MainColor] _BaseMap("Base Map (RGB) Smoothness / Alpha (A)", 2D) = "white" {}

		[HDR]
		_AmbientColor("Ambient Color", Color) = (0.5, 0.5, 0.5, 1)


		[HDR]
		_SpecularColor("Specular Color", Color) = (0.9, 0.9, 0.9, 1)
		_Glossiness("Glossiness", Float) = 32

		[HDR]
		_RimColor("Rim Color", Color) = (1, 1, 1, 1)
		_RimAmount("Rim Amount", Range(0, 1)) = 0.716
		_RimThreshold("Rim Threshold", Range(0,1)) = 0.1
		[HiddenInInspector] _Color("Color", Color) = (0.5, 0.65, 1, 1)
		[HiddenInInspector] _MainTex("Main Texture", 2D) = "white" {}
		// Blending state

		[HideInInspector] _Surface("__surface", Float) = 0.0
		[HideInInspector] _Blend("__blend", Float) = 0.0
		[HideInInspector] _AlphaClip("__clip", Float) = 0.0
		[HideInInspector] _SrcBlend("__src", Float) = 1.0
		[HideInInspector] _DstBlend("__dst", Float) = 0.0
		[HideInInspector] _ZWrite("__zw", Float) = 1.0
		[HideInInspector] _Cull("__cull", Float) = 0.0//2.0
	}

		SubShader
		{
			Tags{"RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
			LOD 300
			Pass
			{
				//First line requests some lighting data be passed into shader
				//Second line requests to restrict this data to only main directional light.

				Tags{
				"LightMode" = "UniversalForward"
				"PassFlags" = "OnlyDirectional"}

				ZWrite On
				ZTest LEqual
				Cull[_Cull]

				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile_fwdbase
				#include "UnityCG.cginc"
				#include "Lighting.cginc"
				#include "AutoLight.cginc"

			// -------------------------------------
			// Material Keywords
			#pragma shader_feature _ALPHATEST_ON
			#pragma shader_feature _ALPHAPREMULTIPLY_ON
			#pragma shader_feature _ _SPECGLOSSMAP _SPECULAR_COLOR
			#pragma shader_feature _GLOSSINESS_FROM_BASE_ALPHA
			#pragma shader_feature _NORMALMAP
			#pragma shader_feature _EMISSION
			#pragma shader_feature _RECEIVE_SHADOWS_OFF

			// -------------------------------------
			// Universal Pipeline keywords
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			// -------------------------------------
			// Unity defined keywords
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			struct appdata
			{
				float4 vertex : POSITION;
				float4 uv : TEXCOORD0;
				float3 normal : NORMAL;
				float stitched : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 worldNormal : NORMAL;
				float3 viewDir : TEXCOORD1;
				SHADOW_COORDS(2)
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			uniform float4x4 TargetObjectTransform;


			sampler2D _BaseMap;
			float4 _BaseMap_ST;

			v2f vert(appdata input)
			{
				v2f output;
				//UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);
				output.pos = UnityObjectToClipPos(input.vertex);
				//output.uv = TRANSFORM_TEX(input.uv, _MainTex);
				output.uv = TRANSFORM_TEX(input.uv, _BaseMap);
				output.viewDir = WorldSpaceViewDir(input.vertex);
				output.worldNormal = UnityObjectToWorldNormal(input.normal);
				TRANSFER_SHADOW(output)
				return output;
			}

			/*
			v2f vert(appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.viewDir = WorldSpaceViewDir(v.vertex);
				o.worldNormal = UnityObjectToWorldNormal(v.normal);
				TRANSFER_SHADOW(o)
				return o;
			}
			*/

			//Light Color variables
			float4 _BaseColor;
			float4 _Color;
			float4 _AmbientColor;
			//Specular variables
			float4 _SpecularColor;
			float _Glossiness;
			//Rim Color Variables
			float4 _RimColor;
			float _RimAmount;
			float _RimThreshold;

			float4 frag(v2f i) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);


				float3 normal = normalize(i.worldNormal);

				//Get normalised viewing direction
				float3 viewDir = normalize(i.viewDir);
				//Get vector between light  dir and view dir
				float3 halfVector = normalize(_WorldSpaceLightPos0 + viewDir);
				//???? confusing
				float NdotH = dot(normal, halfVector);

				//Sample texture color
				//float4 sample = tex2D(_MainTex, i.uv);
				float4 sample = tex2D(_BaseMap, i.uv);
				//Dot product between surface normal and light angle
				float NdotL = dot(_WorldSpaceLightPos0, normal);

				//// SHADOWS
				float shadow = SHADOW_ATTENUATION(i);


				//// LIGHTING
				float lightIntensity = smoothstep(0, 0.01, NdotL * shadow);
				float4 light = lightIntensity * _LightColor0;
				//// SPECULAR
				float specularIntensity = pow(NdotH * lightIntensity, _Glossiness * _Glossiness);
				//smoothstep specular intensity
				float specularIntensitySmooth = smoothstep(0.005, 0.01, specularIntensity);
				float4 specular = specularIntensitySmooth * _SpecularColor;

				//// RIM LIGHTING
				float4 rimDot = 1 - dot(viewDir, normal);
				float rimIntensity = rimDot * pow(NdotL, _RimThreshold);
				rimIntensity = smoothstep(_RimAmount - 0.01, _RimAmount + 0.01, rimIntensity);
				float4 rim = rimIntensity + rimDot;

				return _BaseColor * sample * (_AmbientColor + light + specular + rim);
			}
			ENDCG
		}

			//Shadow handling
			UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
		}
}