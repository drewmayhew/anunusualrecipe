﻿using UnityEditor;
using UnityEngine;
[CustomEditor(typeof(ForestSpawner))]
public class ForestSpawnerEditor : Editor
{
    private ForestSpawner controller;
    private void OnEnable()
    {
        controller = (ForestSpawner)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Load Trees"))
        {
            controller.LoadFromFolder();
        }
        if (GUILayout.Button("Clear Tree Library"))
        {
            controller.ClearTreeLibrary();
        }
        GUILayout.EndHorizontal();
        if (GUILayout.Button("Spawn Forest"))
        {
            controller.SpawnTrees();
        }
        if (GUILayout.Button("Clear Forest"))
        {
            controller.ClearExistingTrees();
        }
    }
}
