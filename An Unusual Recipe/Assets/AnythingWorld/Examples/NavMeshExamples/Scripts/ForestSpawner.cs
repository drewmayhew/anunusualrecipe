﻿using System.Collections.Generic;
using UnityEngine;

public class ForestSpawner : MonoBehaviour
{
    public Terrain terrainObject = null;
    public float treeHeightLimit = 0.5f;
    public GameObject treeObject = null;
    public float radius = 10;
    public Vector2 regionSize = Vector2.one;
    public int rejectionSamples = 30;
    public float displayRadius = 1;
    public float treeHeightOffset = 1;
    public bool treeRandomRotationX = false;
    public bool treeRandomRotationY = false;
    public bool treeRandomRotationZ = false;
    public bool treeRandomScale = false;

    public float minScale = 1f;
    public float maxScale = 2f;
    public bool ShowDebugGizmos = false;

    public GameObject assetFolder;
    //public GameObject rockFolder;
    public List<GameObject> treeLibrary;
    List<Vector2> points;
    [SerializeField]
    public List<GameObject> treeList;
    

    private void Start()
    {
        //LoadFromFolder();   
    }
    public void LoadFromFolder()
    {
        foreach (Transform child in assetFolder.transform)
        {
            treeLibrary.Add(child.gameObject);
        }
    }
    public void ClearTreeLibrary()
    {
        treeLibrary = new List<GameObject>();
    }
    public void SpawnTrees()
    {
        if (treeLibrary.Count == 0 && assetFolder != null) LoadFromFolder();
        if (treeLibrary.Count == 0) return;
        points = PoissonDiscSampling.GeneratePoints(radius, regionSize, rejectionSamples);
        ClearExistingTrees();
        foreach (Vector2 point in points)
        {
            Vector3 point3D = new Vector3(point.x, 0, point.y);
            point3D = point3D + transform.position;
            float scaledTreeHeightOffset = treeHeightOffset;
            if (terrainObject != null)
            {
                float terrainHeight = terrainObject.SampleHeight(point3D);
                if (terrainHeight < treeHeightLimit)
                {
                    if (treeLibrary.Count == 0) return;
                    GameObject environmentObj = treeLibrary[Random.Range(0, treeLibrary.Count)];

                    GameObject newTree = Object.Instantiate(environmentObj, transform);
                    treeList.Add(newTree);

                    if (treeRandomRotationX == true)
                    {
                        float angle = Random.value * Mathf.PI * 2;

                        newTree.transform.Rotate(new Vector3(1, 0, 0), (angle * Mathf.Rad2Deg));

                    }
                    if (treeRandomRotationY == true)
                    {
                        float angle = Random.value * Mathf.PI * 2;

                        newTree.transform.Rotate(new Vector3(0, 1, 0), (angle * Mathf.Rad2Deg));

                    }
                    if (treeRandomRotationZ == true)
                    {
                        float angle = Random.value * Mathf.PI * 2;

                        newTree.transform.Rotate(new Vector3(0, 0, 1), (angle * Mathf.Rad2Deg));

                    }
                    if (treeRandomScale == true)
                    {
                        float newScale = Random.Range(minScale, maxScale);
                        newTree.transform.localScale = newTree.transform.localScale * newScale;
                        scaledTreeHeightOffset *= newScale;
                    }


                    newTree.transform.position = new Vector3(point3D.x, terrainHeight + scaledTreeHeightOffset, point3D.z);
                }
            }
        }
    }
    public void ClearExistingTrees()
    {
        if (treeList != null && treeList.Count > 0)
        {
            foreach (GameObject tree in treeList)
            {
                AWSafeDestroy.SafeDestroy(tree);
            }
        }
        treeList = new List<GameObject>();
    }
    void OnDrawGizmos()
    {
        if (ShowDebugGizmos == false) return;
        Gizmos.DrawWireCube(regionSize / 2, regionSize);
        if (points != null)
        {
            foreach (Vector2 point in points)
            {


                Vector3 point3D = new Vector3(point.x, 0, point.y);
                point3D = point3D + transform.position;
                Gizmos.DrawSphere(point3D, displayRadius);
            }
        }
    }
}
