﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class InventoryController : MonoBehaviour
{
    public GameObject inventoryGUI;
    [SerializeField]
    public List<GameObject> inventorySlots;
    [SerializeField]
    public List<GameObject> pages;

    private List<InventoryItem> itemsToDisplay;

    // Start is called before the first frame update
    void Start()
    {
        inventoryGUI.SetActive(false);
        itemsToDisplay = new List<InventoryItem>();
        //inventorySlots = new List<GameObject>();
        //pages = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (inventoryGUI.activeSelf == true)
            {
                inventoryGUI.SetActive(false);
            }
            else
            {
                GenerateInventory();
                inventoryGUI.SetActive(true);
            }
        }
    }
    
    private void GenerateInventory()
    {

        for(int i=0; i<itemsToDisplay.Count; i++)
        {
            if (i < 6)
            {
                inventorySlots[i].GetComponent<RawImage>().texture = itemsToDisplay[i].icon;
                inventorySlots[i].GetComponent<RawImage>().color = Color.white;
            }
        }
    }

    public void AddItem(Texture2D icon, string name)
    {
        itemsToDisplay.Add(new InventoryItem(icon, name));
    }

    private struct InventoryItem
    {
        public Texture2D icon;
        public string name;

        public InventoryItem(Texture2D icon, string name)
        {
            this.icon = icon;
            this.name = name;
        }
    }
}

