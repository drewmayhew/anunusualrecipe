﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlayerController : MonoBehaviour
{
    private int itemsHeld = 0;

    public delegate void OnItemGet();
    public static OnItemGet onItemGetDelegate;
   // public TMPro.TextMeshProUGUI text;
    private AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        onItemGetDelegate += IncrementItems;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void IncrementItems()
    {
        itemsHeld++;
       // text.text = itemsHeld.ToString();
        audioSource.Play();
    }

}
