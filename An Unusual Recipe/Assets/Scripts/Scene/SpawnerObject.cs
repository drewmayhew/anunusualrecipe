﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerObject : MonoBehaviour
{
    public float speed = 5f;
    public float height = 0.5f;
    private Vector3 startPos;
    public PlayerController player;
    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        //audioSource = GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update()
    {
        //calculate what the new Y position will be
        float newY = Mathf.Sin(Time.time * speed);
        //set the object's Y to the new calculated Y
        transform.position = new Vector3(startPos.x, startPos.y+ (newY * height), startPos.z);
    }
    private void FixedUpdate()
    {
      
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            PlayerController.onItemGetDelegate?.Invoke();
            //Destroy(this.gameObject);
        }
    }
}
