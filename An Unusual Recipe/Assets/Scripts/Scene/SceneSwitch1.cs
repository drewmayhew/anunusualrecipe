﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitch1 : MonoBehaviour
{
    public bool PortalTriggered = false;
    public GameObject Portal2;

    private void Update()
    {
        if (PortalTriggered == true)
        {
            Portal2.SetActive(true);
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (PortalTriggered == true)
        {
            SceneManager.LoadScene(2);
        }
    }
}

