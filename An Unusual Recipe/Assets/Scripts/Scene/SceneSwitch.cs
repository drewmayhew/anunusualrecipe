﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitch : MonoBehaviour
{
    public bool PortalTriggered = false;
    public GameObject Portal1;

    private void Update()
    {
        if(PortalTriggered == true)
        {
            Portal1.SetActive(true);
        }
    }

    void OnTriggerEnter(Collider collider)
    {     
        if(PortalTriggered == true)
        {
            SceneManager.LoadScene(1);
        }
    }
}