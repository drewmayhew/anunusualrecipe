﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class ObjectSpawner : MonoBehaviour
{

    [Range(0f,20f)] public float radius = 20f;
    [SerializeField]
    private List<GameObject> objectsToSpawn;
    [SerializeField]
    private List<Texture2D> objectIcons;
    [SerializeField]
    public List<string> iconNames;
    public InventoryController iconController;

    private int currentObjectIndex = 0;

    public Transform player;
    public List<Texture2D> ObjectIcons { get => objectIcons; set => objectIcons = value; }
    public List<GameObject> ObjectsToSpawn { get => objectsToSpawn; set => objectsToSpawn = value; }

    // Start is called before the first frame update
    void Start()
    {

        PlayerController.onItemGetDelegate += SpawnNextItem;

        //Parent to current object, set object location to zero and set inactive
        foreach(GameObject obj in ObjectsToSpawn)
        {
            CheckSpawnerObjects(obj);
            DeactivateObject(obj);
        }
        if (ObjectsToSpawn.Count > 0)
        {
            SpawnNextItem();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void CheckSpawnerObjects(GameObject obj)
    {
        if (!obj.GetComponent<SpawnerObject>())
        {
            obj.AddComponent<SpawnerObject>();
        }

        if (!obj.GetComponent<Collider>())
        {
            obj.AddComponent<BoxCollider>().isTrigger = true;
        }
        else
        {
            obj.GetComponent<Collider>().isTrigger = true;
        }
    }
    private void DeactivateObject(GameObject obj)
    {
        obj.SetActive(false);
        obj.transform.localPosition = Vector3.zero;
        obj.transform.SetParent(this.transform);

    }

    private void SpawnNextItem()
    {
        Vector3 spawnLocation = new Vector3();
        if(RandomPoint(player.transform.position, out spawnLocation))
        {
            if (currentObjectIndex < objectsToSpawn.Count)
            {
                GameObject currentObj = objectsToSpawn[currentObjectIndex];
                currentObj.SetActive(true);
                currentObj.transform.position = spawnLocation;


                if (currentObjectIndex != 0)
                {
                    int prevIndex = currentObjectIndex - 1;

                    iconController.AddItem(ObjectIcons[prevIndex], iconNames[prevIndex]);
                    DeactivateObject(ObjectsToSpawn[prevIndex]);
                }
                currentObjectIndex++;

            }
            else
            {
                int prevIndex = currentObjectIndex - 1;
                iconController.AddItem(ObjectIcons[prevIndex], iconNames[prevIndex]);
                DeactivateObject(objectsToSpawn[prevIndex]);
            }
        }
    }

    bool SampleRandomNavmeshPoint(Vector3 center, float range, out Vector3 result)
    {
        for (int i = 0; i < 30; i++)
        {
            Vector3 randomPoint = center + Random.insideUnitSphere * range;
            UnityEngine.AI.NavMeshHit hit;
            if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, UnityEngine.AI.NavMesh.AllAreas))
            {
                result = hit.position;
                return true;
            }
        }
        result = Vector3.zero;
        return false;
    }

    private bool RandomPoint(Vector3 centre, out Vector3 result)
    {
        Vector3 randomDirection = Random.insideUnitSphere * radius;
        randomDirection += centre;
        NavMeshHit hit;
        result = Vector3.zero;
        if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
        {
            result = hit.position;
            return true;
        }
        return false;
    }
}
