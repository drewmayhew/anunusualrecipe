﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitch2 : MonoBehaviour
{
    public bool PortalTriggered = false;
    public GameObject Portal3;

    private void Update()
    {
        if (PortalTriggered == true)
        {
            Portal3.SetActive(true);
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (PortalTriggered == true)
        {
            SceneManager.LoadScene(2);
        }
    }
}

